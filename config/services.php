<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'secret' => '',
	],

	'facebook' => [
		'client_id' => '466257920209082',
		'client_secret' => 'f3b775a2cb183ffb55fc853720ed929c',
		'redirect' => 'http://activitly.tagyf.com/social_auth/facebook',
	],

	'twitter' => [
		'client_id' => 'CAkDq1JGsFWz5hOwgphs4AEQe',
		'client_secret' => 'XlL3jyEcTKHS3lVjV7rAh2mNwX0QmRLjKEWHMqKKenR8DjItln',
		'redirect' => 'http://www.activitly.com/social_auth/twitter',
	],

	'google' => [
		'client_id' => '813010634516-ninl9e4i3bfqq7vcapvhg489t0gmhjdh.apps.googleusercontent.com',
		'client_secret' => 'CBqR6iz7i0R-0O0H4JACqMQS',
		'redirect' => 'http://www.activitly.com/social_auth/google'
	],

];
