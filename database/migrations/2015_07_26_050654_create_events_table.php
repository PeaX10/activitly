<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id')->index();
			$table->string('title');
			$table->integer('owner');
			$table->string('cover');
			$table->string('lat');
			$table->string('lng');
            $table->string('location');
            $table->string('adress');
            $table->integer('activity_id');
			$table->string('description');
			$table->date('date');
            $table->integer('place');
            $table->string('start_time');
            $table->string('end_time');
			$table->string('options');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
