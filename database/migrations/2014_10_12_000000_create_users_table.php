<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username')->unique();
            $table->string('firstname');
            $table->string('lastname');
			$table->integer('gender');
			$table->string('email')->unique();
			$table->string('password', 60);
            $table->string('location');
            $table->string('activities');
            $table->string('avatar');
			$table->string('banner');
			$table->string('facebook');
            $table->string('twitter');
            $table->string('google');
            $table->date('birthday');
            $table->integer('validate');
            $table->integer('step');
            $table->integer('ban');
            $table->text('token');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
