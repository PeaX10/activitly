<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use Mail;
use App\AuthSocial;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
     * @param  boolean $home
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data, $home=null)
	{
        if(!empty($home)){
            return Validator::make($data, [
                'username' => 'required|alpha_dash|min:3|max:32|unique:users',
                'email' => 'required|email|unique:users'
            ]);
        }else{
            return Validator::make($data, [
                'username' => 'required|alpha_dash|min:3|max:32|unique:users',
                'firstname' => 'required|max:50',
                'lastname' => 'required|max:50',
                'gender' => 'required|numeric',
                'birthday' => 'required|date_format:"d/m/Y|before:"now -16 years"',
                'email' => 'required|email|unique:users',
                'password' => 'min:6|required',
                'password_confirmation' => 'min:6|same:password|required'
            ]);
        }
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
        // Tant que que le token unique n'est pas unique, on va en chercher un nouveau
        $unique = false;
        while($unique === false){
            $data['token'] = str_random('48');
            if(User::where('token', '=', $data['token'])->count() == 0) $unique = true;
        }
        /*

        Mail::send('mail.confirm_account', $data, function($message) use($data)
        {
            $message->to($data['email'], ucfirst(strtolower($data['lastname'])).' '.ucfirst(strtolower($data['firstname'])))->subject('Bienvenue sur Activitly !');
        });
        */
        if(count(Mail::failures()) == 0 ) {
            $user = User::create([
                'firstname' => ucfirst(strtolower($data['firstname'])),
                'lastname' => ucfirst(strtolower($data['lastname'])),
                'gender' => $data['gender'],
                'birthday' => $data['birthday'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'username' => $data['username'],
                'token' => $data['token']
            ]);
            if($user){
                // On regarde si on a une connexion social et on lui rajoute l'id
                if(!empty($data['key_social'])){
                    $social = explode('-', $data['key_social']);
                    $auth = AuthSocial::where('network', $social[0])->where('user', 0)->where('key', $social[1])->first();
                    if(count($auth) == 1){
                        $auth->user = $user->id;
                        $auth->save();
                    }
                }
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }


	}

}
