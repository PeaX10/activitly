<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPost extends Model {

    protected $table = 'event_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user', 'event', 'type', 'content', 'created_at', 'updated_at'];

}
