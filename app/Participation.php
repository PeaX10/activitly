<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Participation extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'participations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user', 'event', 'created_at'];


}
