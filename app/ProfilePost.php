<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilePost extends Model {

    protected $table = 'profile_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user', 'type', 'content', 'created_at', 'updated_at'];

}
