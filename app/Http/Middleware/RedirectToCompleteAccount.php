<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use App\User;
use Carbon\Carbon;

class RedirectToCompleteAccount {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->auth->check())
		{
			if($this->auth->user()->validate == 0){
				return redirect('/auth/logout')->with('validate', true);
			}else{
				$step = $this->auth->user()->step;
				if($step === 0) {
					return new RedirectResponse(url('/account/complete'));
				}else{
					// On dit que l'utilisateur été bien connecté à ce moment là
					User::where('id', $this->auth->user()->id)->update(['last_activity' => Carbon::now()]);
					return $next($request);
				}
			}
		}else{
            return redirect()->guest('auth/login');
        }
	}

}
