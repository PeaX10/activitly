<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use App\Invite;
use Jenssegers\Date\Date;

class VarToView {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        # à appliquer à tout les pays plus tard
        setlocale(LC_ALL, 'fr_FR');
        Date::setLocale('fr');

		if ($this->auth->check()) {
            $menu = [   'activities_next' => DB::table('participations')
                                                ->join('events', 'participations.event', '=', 'events.id')
                                                ->where('participations.status', 1)
                                                ->where('participations.user', $this->auth->user()->id)
                                                ->where('events.date', '>=', date('Y-m-d'))
                                                ->count(),
                        'activities_invites' => Invite::where('recipient', Auth::user()->id)->where('visible', 1)->count(),
                        'messages' => 2,
                        'notifications' => 2,
                        'profile_friends' => 0
            ];
            view()->share('user', $this->auth->user());
            view()->share('menu', $menu);
		}

        return $next($request);
	}
}
