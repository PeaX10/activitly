<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\NewsletterUser;
use Mail;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        if(Auth::check()){
            $user = Auth::user();
            if($user->validate == 0)
                return redirect('auth/logout')->with('validate', true);
            if($user->step == 0)
                return redirect('account/complete');
            return redirect('/board');
        }

        return view('home/index');
	}

    public function googleVerif(){
        return 'google-site-verification: google98fc6fcaba38269b.html';
    }

	public function newsletter(Request $request){
		$rules = array('email' => 'required|email|unique:newsletter_users|unique:users');

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }else{
            NewsletterUser::create(array('email' => Input::get('email')));
            return redirect('/')->with('success_newsletter', true);
        }
	}

    public function terms(){
        return view('pages/terms');
    }

    public function privacy(){
        return view('pages/privacy');
    }

    public function contact(){
        return view('pages/contact');
    }

    public function postContact(Request $request){
        $rules = array('message' => 'required', 'subject' => 'required|numeric|between:1,6');
        if(!Auth::check()){
            $rules = array_merge($rules, array('name' => 'required', 'email' => 'required|email'));
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('contact')
                ->withErrors($validator)
                ->withInput();
        }else{
            if(Auth::check()){
                $user = Auth::user();
                $data['email']  = $user->email;
                $data['name']  = $user->lastname.' '.$user->firstname;
            }else{
                $data['email'] = Input::get('email');
                $data['name'] = Input::get('name');
            }
            $data['msg'] = Input::get('message');
            // Sujet
            $data['subject'] = Input::get('subject');
            $sujet = array('1' => 'Proposition d\'une nouvelle activité',
                '2' => 'Problème technique',
                '3' => 'Réclamation',
                '4' => 'Partenariat',
                '5' => 'Publicité',
                '6' => 'Autres');
            foreach($sujet as $key => $value){
                if($key == $data['subject']) $data['subject'] = $value;
            }
            /*
            Mail::send('mail.contact', $data, function($message) use($data)
            {
                $message->to('contact@activitly.com', $data['name'])->subject('Activitly Contact - '.$data['subject']);
            });
            */

            if(count(Mail::failures()) == 0 ) {
                return redirect('contact')->with('success_contact', true);
            }else{
                return redirect('contact')->withInput()->with('contact_failed', true);
            }
        }
    }


}
