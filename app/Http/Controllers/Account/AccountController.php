<?php
namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\User;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Intervention\Image\Facades\Image;
use App\AuthSocial;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Support\Facades\File;

class AccountController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Panel Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    public function index(){
        $user = Auth::user();
        $date = \DateTime::createFromFormat('Y-m-d', $user->birthday);
        $user->birthday = $date->format('d/m/Y');
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $user->created_at);
        $user->created_date = $date->format('d/m/Y');
        return view('account/index');
    }

    public function editInfos(Request $request)
    {
        // Ajout d'une règle de validation du password
        Validator::extend('passcheck', function ($attribute, $value, $parameters)
        {
            return Hash::check($value, Auth::user()->getAuthPassword());
        });

        $user = Auth::user();
        $rules = array('username' => 'required|alpha_dash|min:3|max:32|unique:users,username,'.$user->id,
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'gender' => 'required|numeric',
            'birthday' => 'required|before:"now -16 years"|date_format:"d/m/Y"',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'city' => 'required',
            'password_infos' => 'min:6|required|passcheck',
            'new_password' => 'min:6',
            'new_password_confirm' => 'required_with:new_password|same:new_password');

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect('account')
                ->withErrors($validator)
                ->withInput();
        }else{
            // On fait les validations que le validator ne peux pas faire
            $address = Input::get('city').', France';
            $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            if($json['status'] == 'ZERO_RESULTS' || $json['status'] == 'INVALID_REQUEST'){
                $validator->getMessageBag()->add('location', 'L\'adresse est introuvable ou n\'est pas situé en France');
                return redirect('/account')->withInput()->withErrors($validator);
            }else{
                // Sinon on récupère les infos nécessaires
                $adress = $json['results'][0]['formatted_address'];
                $lat = $json['results'][0]['geometry']['location']['lat'];
                $lng = $json['results'][0]['geometry']['location']['lng'];
                $address_components = $json['results'][0]['address_components'];
                foreach($address_components as $key => $value){
                    foreach($value['types'] as $key2 => $value2){
                        if($value2 == 'locality') $city = $value['long_name'];
                        if($value2 == 'country') $country = $value['long_name'];
                    }
                }
                if(empty($city) || empty($country)){
                    $validator->getMessageBag()->add('location', 'Il y a un problème avec cette adresse');
                    return redirect('/account')->withInput()->withErrors($validator);
                } else {
                    $city = $city.', '.$country;
                }
            }

            $user->location = $city;
            $user->username = Input::get('username');
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');
            $user->gender = Input::get('gender');
            // Besoin de mettre en forme la date
            $birthday = \DateTime::createFromFormat('d/m/Y', Input::get('birthday'));
            $birthday = $birthday->format('Y-m-d');
            $user->birthday = $birthday;

            // Modification du mot de passe
            if(Input::has('new_password')){
                $user->password = Hash::make(Input::get('new_password'));
            }

            // Modification E-mail
            $update_email = false;
            if($user->email != Input::get('email')){
                // On demande une vérification de l'adresse mail
                $user->validate = 0;

                // Tant que que le token unique n'est pas unique, on va en chercher un nouveau
                $unique = false;
                while($unique === false){
                    $data['token'] = str_random('48');
                    if(User::where('token', '=', $data['token'])->count() == 0) $unique = true;
                }
                $data['email'] = Input::get('email');
                $data['lastname'] = $user->lastname;
                $data['firstname'] = $user->firstname;
                $data['username'] = $user->username;
                
                /*
                Mail::send('mail.change_email', $data, function($message) use($data)
                {
                    $message->to($data['email'], ucfirst(strtolower($data['lastname'])).' '.ucfirst($data['firstname']))->subject('Activitly - Changement de votre adresse mail');
                });
                */

                if(count(Mail::failures()) != 0 ) {
                    $error_email = true;
                }else{
                    $user->email = Input::get('email');
                    $user->token = $data['token'];
                    $update_email = true;
                }
            }

            if(!empty($error_email)){
                return redirect('/')->withInput()->with('error_email_updated', true);
            }else{
                $user->save();
                if($update_email){
                    Auth::logout();
                    return redirect('/')->withInput()->with('email_updated', true);
                }else{
                    return redirect('/account')->with('success_edit_account_infos', true);
                }
            }
        }
    }

    public function editProfile(Request $request)
    {
        // Ajout d'une règle de validation du password
        Validator::extend('passcheck', function ($attribute, $value, $parameters)
        {
            return Hash::check($value, Auth::user()->getAuthPassword());
        });

        $rules = ['activities' => 'required',
            'facebook' => 'url|regex:/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/',
            'twitter' => 'url|regex:/^(https?:\/\/)?(www\.)?twitter\.com\/(#!\/)?[a-zA-Z0-9_]+/',
            'google' => 'url|regex:/plus\.google\.com\/.?\/?.?\/?([0-9]*)/i',
            'profile_img' => 'max:2048|image',
            'ban_img' => 'max:2048|image',
            'profile_params' => 'required_with:profile_img',
            'ban_params' => 'required_with:ban_img',
            'password_profile' => 'required|passcheck'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/account')
                ->withErrors($validator)
                ->withInput();
        }else{
            // On vérifie que les paramètres des photos existes bien et sont corrects
            $error_key = false;
            // Photo de Profil
            if($request->has('profile_params')){
                $json = json_decode(Input::get('profile_params'));
                $json_profile = $json;
                if((json_last_error() != JSON_ERROR_NONE)){
                    $error_key = true;
                }else{
                    // On vérifie que toutes les clés sont présentes
                    $key_allowed = array('x', 'y', 'width', 'height', 'rotate');
                    $json = (array) $json;
                    foreach($json as $key => $value){
                        if(!in_array($key, $key_allowed) || !is_numeric($value)) $error_key = true;
                    }
                    if(count($json) != 5) $error_key = true;
                }
            }

            // Bannière
            if($request->has('ban_params')) {
                $json = json_decode(Input::get('ban_params'));
                $json_ban = $json;
                if ((json_last_error() != JSON_ERROR_NONE)) {
                    $error_key = true;
                } else {
                    // On vérifie que toutes les clés sont présentes
                    $key_allowed = array('x', 'y', 'width', 'height', 'rotate');
                    $json = (array)$json;
                    foreach ($json as $key => $value) {
                        if (!in_array($key, $key_allowed) || !is_numeric($value)) $error_key = true;
                    }
                    if(count($json) != 5) $error_key = true;
                }
            }

            if($error_key == true){
                $validator->getMessageBag()->add('profile_img', 'Une erreur lors du découpage de l\'image est survenue.');
            }

            // On vérifie que les activités favorites sont connues
            $activities = $request->only('activities')['activities'];
            $activities = explode(',', $activities);
            $error_activity = false;
            foreach($activities as $key => $value){
                if(Activity::where('name', '=', $value)->count() == 0){
                    $error_activity = true;
                }
            }
            if($error_activity){
                $validator->getMessageBag()->add('activities', 'Une ou plusieurs activités que vous avez entrer ne sont pas disponibles pour le moment, merci de ne sélectionner que les activités proposés.');
            }
        }
        if ($validator->getMessageBag()->count() > 0) {
            return redirect('/account')
                ->withErrors($validator)
                ->withInput()->with('open_profile', true);
        }else{
            $user = Auth::user();
            $auth = AuthSocial::where('user', $user->id)->first();
            if($request->hasFile('profile_img')){
                $profile_img = Image::make(Input::file('profile_img'));
                $profile_img->crop(ceil($json_profile->width), ceil($json_profile->height), ceil($json_profile->x), ceil($json_profile->y));
                $filename = strtolower($user->username).'_'.str_random(32);
                if(File::exists(public_path('img/avatar/'.$user->avatar.'.png'))) File::delete(public_path('img/avatar/'.$user->avatar.'.png'));
                $profile_img->save(public_path('img/avatar/'.$filename.'.png'), 100);
                $profile_img->destroy();
                $user->avatar = $filename;
            }
            if($request->hasFile('ban_img')){
                $ban_img = Image::make(Input::file('ban_img'));
                $ban_img->crop(ceil($json_ban->width), ceil($json_ban->height), ceil($json_ban->x), ceil($json_ban->y));
                $filename = strtolower($user->username).'_'.str_random(32);
                if(File::exists(public_path('img/banner/'.$user->banner.'.png'))) File::delete(public_path('img/banner/'.$user->banner.'.png'));
                $ban_img->save(public_path('img/banner/'.$filename.'.png'), 100);
                $ban_img->destroy();
                $user->banner = $filename;
            }
            $user->step = 1;
            $user->activities = Input::get('activities');
            $user->facebook = Input::get('facebook');
            $user->twitter = Input::get('twitter');
            $user->google = Input::get('google');
            $user->save();
            return redirect('account')->with('success_profile_updated', true)->with('open_profile', true);
        }
    }

    public function confirmAccount($token){
        $user = User::where('token', '=', $token)->first();
        if(!empty($user)){
            $user->validate = 1;
            $user->token = null;
            $user->save();
            return redirect('/')->with('create_msg', 'yes');
        }else{
            return redirect('/');
        }
    }

    public function completeAccount(){
        $user = Auth::user();
        if($user->step === 0){
            $date = \DateTime::createFromFormat('Y-m-d', $user->birthday);
            $user->birthday = $date->format('d/m/Y');
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $user->created_at);
            $user->created_date = $date->format('d/m/Y');
            // On récupère les datas supplémentaires récupèrer depuis les réseaux sociaux
            if(AuthSocial::where('user', $user->id)->count() > 0){
                $data = AuthSocial::where('user', $user->id)->first();
                $user = array_merge($user->toArray(), (array) json_decode($data->data));
            }
            return view('account.complete');
        }else{
            return redirect('account');
        }
    }

    public function verifCompleteAccount(Request $request){
        $rules = ['city' => 'required',
                'activities' => 'required',
                'facebook' => 'url|regex:/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/',
                'twitter' => 'url|regex:/^(https?:\/\/)?(www\.)?twitter\.com\/(#!\/)?[a-zA-Z0-9_]+/',
                'google' => 'url|regex:/plus\.google\.com\/.?\/?.?\/?([0-9]*)/i',
                'profile_img' => 'max:2048|image',
                'ban_img' => 'max:2048|image',
                'profile_params' => 'required_with:profile_img',
                'ban_params' => 'required_with:ban_img'
        ];

        $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect('account/complete')
                    ->withErrors($validator)
                    ->withInput();
            }else{

            // On fait les validations que le validator ne peux pas faire
            $address = Input::get('city').', France';
            $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            if($json['status'] == 'ZERO_RESULTS'){
                $validator->getMessageBag()->add('city', 'La ville que vous avez entré n\'est pas disponible en France');
            }else{
                // Sinon on récupère la ville sous un format international
                $city = $json['results'][0]['formatted_address'];
            }

            // On vérifie que les paramètres des photos existes bien et sont corrects
            $error_key = false;
            // Photo de Profil
            if($request->has('profile_params')){
                $json = json_decode(Input::get('profile_params'));
                $json_profile = $json;
                if((json_last_error() != JSON_ERROR_NONE)){
                    $error_key = true;
                }else{
                    // On vérifie que toutes les clés sont présentes
                    $key_allowed = array('x', 'y', 'width', 'height', 'rotate');
                    $json = (array) $json;
                    foreach($json as $key => $value){
                        if(!in_array($key, $key_allowed) || !is_numeric($value)) $error_key = true;
                    }
                    if(count($json) != 5) $error_key = true;
                }
            }

            // Bannière
            if($request->has('ban_params')) {
                $json = json_decode(Input::get('ban_params'));
                $json_ban = $json;
                if ((json_last_error() != JSON_ERROR_NONE)) {
                    $error_key = true;
                } else {
                    // On vérifie que toutes les clés sont présentes
                    $key_allowed = array('x', 'y', 'width', 'height', 'rotate');
                    $json = (array)$json;
                    foreach ($json as $key => $value) {
                        if (!in_array($key, $key_allowed) || !is_numeric($value)) $error_key = true;
                    }
                    if(count($json) != 5) $error_key = true;
                }
            }

            if($error_key == true){
                $validator->getMessageBag()->add('profile_img', 'Une erreur lors du découpage de l\'image est survenue.');
            }

            // On vérifie que les activités favorites sont connues
            $activities = $request->only('activities')['activities'];
            $activities = explode(',', $activities);
            $error_activity = false;
            foreach($activities as $key => $value){
                if(Activity::where('name', '=', $value)->count() == 0){
                    $error_activity = true;
                }
            }
            if($error_activity){
                $validator->getMessageBag()->add('activities', 'Une ou plusieurs activités que vous avez entrer ne sont pas disponibles pour le moment, merci de ne sélectionner que les activités proposés.');
            }
        }
        if ($validator->getMessageBag()->count() > 0) {
            return redirect('account/complete')
                ->withErrors($validator)
                ->withInput();
        }else{
            $user = Auth::user();
            $auth = AuthSocial::where('user', $user->id)->first();
            if($request->hasFile('profile_img')){
                $profile_img = Image::make(Input::file('profile_img'));
                $profile_img->crop(ceil($json_profile->width), ceil($json_profile->height), ceil($json_profile->x), ceil($json_profile->y));
                $filename = strtolower($user->username).'_'.str_random(32);
                $profile_img->save(public_path('img/avatar/'.$filename.'.png'), 100);
                $profile_img->destroy();
                $user->avatar = $filename;
            }elseif(count($auth) > 0){
                $avatar_online = (array) json_decode($auth->data);
                if(isset($avatar_online['avatar_online'])) {
                    $avatar_online = str_replace('https://', 'http://', $avatar_online['avatar_online']);
                    $profile_img = Image::make($avatar_online);
                    $filename = strtolower($user->username) . '_' . str_random(32);
                    $profile_img->save(public_path('img/avatar/' . $filename . '.png'), 100);
                    $profile_img->destroy();
                    $user->avatar = $filename;
                }
            }
            if($request->hasFile('ban_img')){
                $ban_img = Image::make(Input::file('ban_img'));
                $ban_img->crop(ceil($json_ban->width), ceil($json_ban->height), ceil($json_ban->x), ceil($json_ban->y));
                $filename = strtolower($user->username).'_'.str_random(32);
                $ban_img->save(public_path('img/banner/'.$filename.'.png'), 100);
                $ban_img->destroy();
                $user->banner = $filename;
            }elseif(count($auth) > 0){
                $banner_online = (array) json_decode($auth->data);
                if(isset($banner_online['banner_online'])) {
                    $banner_online = str_replace('https://', 'http://', $banner_online['banner_online']);
                    $ban_img = Image::make($banner_online);
                    $filename = strtolower($user->username) . '_' . str_random(32);
                    $ban_img->save(public_path('img/banner/' . $filename . '.png'), 100);
                    $ban_img->destroy();
                    $user->banner = $filename;
                }
            }
            $user->step = 1;
            $user->location = $city;
            $user->activities = Input::get('activities');
            $user->facebook = Input::get('facebook');
            $user->twitter = Input::get('twitter');
            $user->google = Input::get('google');
            $user->save();
            return redirect('board');
        }
    }


}
