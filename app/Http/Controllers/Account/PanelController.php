<?php
namespace App\Http\Controllers\Account;

use App\Participation;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Activity;
use App\Event;
use Intervention\Image\Facades\Image;
use App\Invite;
use App\Friendship;
use App\ProfilePost;
use Illuminate\Support\Facades\Response;
use App\EventPost;

class PanelController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Panel Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    public function index(){
        $user = Auth::user();
        $events = Event::where('location', $user->location)->where('date', '>=', date('Y-m-d'))->get();
        return view('board.index', compact('events'));
    }

    public function createActivity(){
        return view('board.activity.create');
    }

    public function createActivityPost(Request $request){
        $rules = [  'title' => 'required|min:4',
                    'location' => 'required',
                    'activity' => 'required|exists:activities,name',
                    'cover_img' => 'image|max:2048',
                    'date-activity' => 'required|date_format:"d/m/Y|after:"now -1 day"|before:"now +2 months +1 day"',
                    'start_time' => 'required|date_format:"H:i"',
                    'end_time' => 'required_with:endtime|date_format:"H:i"',
                    'description' => 'required|min:30|max:1500',
                    'place'       => 'required|min:0|max:100|numeric'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('activity/create')
                ->withErrors($validator)
                ->withInput();
        }else{
            // On fait les validations que le validator ne peux pas faire
            $address = Input::get('location');
            $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            if($json['status'] == 'ZERO_RESULTS' || $json['status'] == 'INVALID_REQUEST'){
                $validator->getMessageBag()->add('location', 'L\'adresse est introuvable ou n\'est pas situé en France');
            }else{
                // Sinon on récupère les infos nécessaires
                $adress = $json['results'][0]['formatted_address'];
                $lat = $json['results'][0]['geometry']['location']['lat'];
                $lng = $json['results'][0]['geometry']['location']['lng'];
                $address_components = $json['results'][0]['address_components'];
                foreach($address_components as $key => $value){
                    foreach($value['types'] as $key2 => $value2){
                        if($value2 == 'locality') $city = $value['long_name'];
                        if($value2 == 'country') $country = $value['long_name'];
                    }
                }
                if(empty($city) || empty($country)) $validator->getMessageBag()->add('location', 'Il y a un problème avec cette adresse');
            }

            if ($validator->fails()) {
                return redirect('activity/create')
                    ->withErrors($validator)
                    ->withInput();
            }else{
                // Tout est bon on upload l'image et on créer l'activité

                $user = Auth::user();

                $data = [   'title' => Input::get('title'),
                            'owner' => $user->id,
                            'lat'   => $lat,
                            'lng'   => $lng,
                            'adress' => $adress,
                            'location' => $city.', '.$country,
                            'activity_id' => Activity::where('name', Input::get('activity'))->first()->id,
                            'description' => Input::get('description'),
                            'date' => \DateTime::createFromFormat('d/m/Y', Input::get('date-activity'))->format('Y-m-d'),
                            'start_time' => Input::get('start_time'),
                            'end_time'  => Input::get('end_time'),
                            'place'     => Input::get('place')
                ];

                if($request->hasFile('cover_img')){
                    $profile_img = Image::make(Input::file('cover_img'));
                    $filename = strtolower($user->username).'_'.str_random(32);
                    $profile_img->save(public_path('img/cover/'.$filename.'.png'), 100);
                    $profile_img->destroy();
                    $data['cover'] = $filename;
                }
                if(Input::get('endtime')) $data['end_time'] = Input::get('end_time');
                Event::create($data);

                return redirect('activity')->with('success_activity_create', true);
            }
        }
    }

    public function getActivities(){
        $activities_next = Event::where('owner', Auth::user()->id)->where('date', '>=', date('Y-m-d'))->get();
        $activities_prev = Event::where('owner', Auth::user()->id)->where('date', '<', date('Y-m-d'))->get();
        return view('board.activity.index', compact('activities_next', 'activities_prev'));
    }

    public function getNextActivities(){
        $activities_next = DB::table('participations')
            ->join('events', 'participations.event', '=', 'events.id')
            ->where('participations.status', 1)
            ->where('participations.user', Auth::user()->id)
            ->where('events.date', '>=', date('Y-m-d'))
            ->get();
        return view('board.activity.next', compact('activities_next'));
    }

    public function getPrevActivities(){
        $activities_prev = DB::table('participations')
            ->join('events', 'participations.event', '=', 'events.id')
            ->where('participations.status', 1)
            ->where('participations.user', Auth::user()->id)
            ->where('events.date', '<', date('Y-m-d'))
            ->get();
        return view('board.activity.prev', compact('activities_prev'));
    }

    public function getInvites(){
        $invites = Invite::where('recipient', Auth::user()->id)->where('visible', 1)->get();
        return view('board.activity.invite', compact('invites'));
    }

    public function DelInvites(){
        $user = Auth::user();
        $id_invite = Input::get('invite');
        $invite = Invite::where('recipient', $user->id)->where('id', $id_invite)->first();
        if($invite->count() >= 1){
            $invite->visible = 0;
            $invite->save();
        }

        return redirect('activity/invites')->with('success_invite_deleted', true);
    }

    public function showProfile(Request $request, $username){
        $profile = User::where('username', $username)->first();
        $folowers = Friendship::where('user_id', $profile->id)->get();
        $subscriptions = Friendship::where('friend_id', $profile->id)->get();
        $posts = ProfilePost::where('profile_user', $profile->id)->orderBy('updated_at', 'desc')->paginate(10);
        $my_activities = Event::where('owner', Auth::user()->id)->orderBy('date', 'desc')->get();
        $activities_next = DB::table('participations')
            ->join('events', 'participations.event', '=', 'events.id')
            ->where('participations.status', 1)
            ->where('participations.user', Auth::user()->id)
            ->where('events.date', '>=', date('Y-m-d'))
            ->orderBy('events.date', 'asc')
            ->get();
        $activities_prev = DB::table('participations')
            ->join('events', 'participations.event', '=', 'events.id')
            ->where('participations.status', 1)
            ->where('participations.user', Auth::user()->id)
            ->where('events.date', '<', date('Y-m-d'))
            ->orderBy('events.date', 'desc')
            ->get();

        if ($request->ajax()) {
            return view('board.profile.posts', compact('profile', 'posts'));
        }

        return view('board.profile.view', compact('profile', 'folowers', 'subscriptions', 'my_activities', 'activities_next', 'activities_prev', 'posts'));
    }

    public function getFriends(){
        $user = Auth::user();
        $friends = Friendship::where('user_id', $user->id)->get();
        foreach($friends as $key => $value){
            $friends[$key] = User::where('id', $value->friend_id)->first();
        }
        return view('board.profile.friends', compact('friends'));
    }

    public function search(Request $request){
        $user = Auth::user();
        $rules = [  'activity' => 'exists:activities,name',
            'city' => '',
            'date' => 'date_format:"d/m/Y|after:"now -1 day"|before:"now +2 months +1 day"'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('search')
                ->withErrors($validator)
                ->withInput();
        }else{
            // Recherche
            $events = new Event;
            // Si l'activité est défini
            if(!empty(Input::get('activity'))){
                $activity_id = Activity::where('name', Input::get('activity'))->first()->id;
                $events = $events->where('activity_id', $activity_id);
            }
            // Si la ville est défini
            if(!empty(Input::get('city'))){
                // On fait les validations que le validator ne peux pas faire
                $address = Input::get('city');
                $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern
                $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
                $response = file_get_contents($url);
                $json = json_decode($response,TRUE); //generate array object from the response from the web
                if($json['status'] == 'ZERO_RESULTS' || $json['status'] == 'INVALID_REQUEST'){
                    $validator->getMessageBag()->add('location', 'L\'adresse est introuvable ou n\'est pas situé en France');
                }else{
                    // Sinon on récupère les infos nécessaires
                    $adress = $json['results'][0]['formatted_address'];
                    $lat = $json['results'][0]['geometry']['location']['lat'];
                    $lng = $json['results'][0]['geometry']['location']['lng'];
                    $address_components = $json['results'][0]['address_components'];
                    foreach($address_components as $key => $value){
                        foreach($value['types'] as $key2 => $value2){
                            if($value2 == 'locality') $city = $value['long_name'];
                            if($value2 == 'country') $country = $value['long_name'];
                        }
                    }
                    if(empty($city) || empty($country)) $validator->getMessageBag()->add('location', 'Il y a un problème avec cette adresse'); else $city = $city.', '.$country;
                }

                if ($validator->fails()) {
                    return redirect('search')
                        ->withErrors($validator)
                        ->withInput();
                }else{
                    $events = $events->where('location', $city);
                }

            }

            // Si il y a une date
            if(!empty(Input::get('date'))){
                $events = $events->where('date', \DateTime::createFromFormat('d/m/Y', Input::get('date'))->format('Y-m-d'));
            }
            $events = $events->where('date', '>=', date('Y-m-d'))->orderBy('date', 'desc');
            $events_p = $events->paginate(6);
            $events = $events->get();

            return view('board.search', compact('events', 'events_p'));
        }

    }

    public function postOnProfile(Request $request, $pseudo){
        $user = Auth::user();
        $rules = [  'content' => 'required|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('profile/'.$pseudo)
                ->withErrors($validator)
                ->withInput();
        }else{
            $post = new ProfilePost();
            $post->type = 1;
            $post->user = $user->id;
            $post->profile_user = User::where('username', $pseudo)->first()->id;
            $post->content = Input::get('content');
            $post->save();
            return redirect('profile/'.$pseudo)->with('success_post', true);
        }
    }

    public function postOnActivity(Request $request, $id){
        $user = Auth::user();
        $rules = [  'content' => 'required|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $post = new EventPost();
            $post->type = 1;
            $post->user = $user->id;
            $post->event = $id;
            $post->content = Input::get('content');
            $post->save();
            return back()->with('success_post', true);
        }
    }
    
    public function addFriend(Request $request, $pseudo){
        $user = Auth::user();
        $friend = User::where('username', $pseudo)->first();

        $friendship = new Friendship();
        $friendship->user_id = $user->id;
        $friendship->friend_id = $friend->id;
        $friendship->save();
        return redirect('profile/'.$pseudo)->with('success_friend_add', true);
    }

    public function deleteFriend(Request $request, $pseudo){
        $user = Auth::user();
        $friend = User::where('username', $pseudo)->first();

        Friendship::where('user_id', $user->id)->where('friend_id', $friend->id)->delete();
        return redirect('profile/'.$pseudo)->with('success_friend_delete', true);
    }

    public function deletePostProfile($id){
        $user = Auth::user();
        $post = ProfilePost::where('id', $id);
        // On vérifie que l'utilisateur en a bien le droit
        if($post->count() != 0 && ($post->first()->user == $user->id || $post->first()->profile_user == $user->id)){
            $post->delete();
            return back()->with('success_post_deleted', true);
        }else{
            return redirect('/');
        }
    }

    public function deletePostActivity($id){
        $user = Auth::user();
        $post = EventPost::where('id', $id);
        // On vérifie que l'utilisateur en a bien le droit
        if($post->count() > 0 && $post->first()->user == $user->id || $post->first()->event == $user->id){
            $post->delete();
            return back()->with('success_post_deleted', true);
        }else{
            return redirect('/');
        }
    }

    public function deleteActivity($id){
        $user = Auth::user();
        $event = Event::where('id', $id);
        // On vérifie que l'utilisateur en a bien le droit
        if($event->count() > 0 && $event->first()->owner == $user->id){
            $event->delete();
            return redirect('/board')->with('success_activity_deleted', true);
        }else{
            return redirect('/');
        }
    }

    public function showActivity(Request $request, $id){
        $activity = Event::where('id', $id)->first();
        $owner = User::where('id', $activity->owner)->first();
        $posts = EventPost::where('event', $id)->orderBy('updated_at', 'desc')->paginate(10);
        $participants = Participation::where('event', $activity->id)->get();
        $queue = $activity->place - count($participants);

        if ($request->ajax()) {
            return view('board.activity.posts', compact('activity', 'owner', 'posts'));
        }

        return view('board.activity.view', compact('activity', 'owner', 'posts', 'participants', 'queue'));
    }

}
