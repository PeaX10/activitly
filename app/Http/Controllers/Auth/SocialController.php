<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\AuthSocial;
use Illuminate\Support\Facades\Auth;

class SocialController extends Controller {

	public function facebook_redirect() {
		return Socialite::with('facebook')->scopes(['user_birthday', 'user_location', 'email', 'public_profile', 'user_friends'])->redirect();
	}

	public function facebook() {
        if(!empty($_GET['error'])){
            return redirect('/')->with('error_social_auth', 'facebook');
        }else{
            $user = Socialite::with('facebook')->user();
            // Dans le cas ou il n'existe meme pas en BDD
            if(AuthSocial::where('key', $user->id)->where('network', 'facebook')->count() == 0) {
                // On récupère les infos on les envoie aux formulaires de la page d'inscription
                // Mais aussi on créer un champs en hidden dans le formulaire qui garde l'ID FB
                // On implémente après l'inscription pour que si le champs en hidden existe
                // Une fois la création de l'utilisateur, on ajoute aussi le connexion dans la
                // Table SocialAuth
                $data = array();
                $data['key_social'] = 'facebook-' . $user->id;
                if ($user->nickname != null) $data['username'] = $user->nickname;
                $data['email'] = $user->email;
                $data['lastname'] = $user->user['last_name'];
                $data['firstname'] = $user->user['first_name'];
                if ($user->user['gender'] == 'male') {
                    $data['gender'] = 1;
                } else {
                    $data['gender'] = 0;
                }
                if (!empty($user->user['birthday'])){
                    $birthday = explode('/', $user->user['birthday']);
                    $data['birthday'] = $birthday[1].'/'.$birthday[0].'/'.$birthday[2];
                }

                $external_data = array(); // TOUTES LES AUTRES DONNES A SAV A COTE
                $external_data['facebook'] = str_replace('/app_scoped_user_id/', '/', $user->user['link']);
                $external_data['avatar_online'] = $user->avatar_original;
                if (!empty($user->user['location'])) $external_data['location'] = $user->user['location']['name'];
                // Création en BDD avec user null que l'on retrouvera avec la clé
                AuthSocial::create([
                    'network' => 'facebook',
                    'key' => $user->id,
                    'data' => json_encode($external_data),
                ]);
                return redirect('auth/register')->withInput($data);
            }elseif(AuthSocial::where('key', $user->id)->where('network', 'facebook')->where('user', 0)->count() > 0){
                // Déjà cliqué que le bouton mais ne c'est pas inscrit
                $data = array();
                $data['key_social'] = 'facebook-' . $user->id;
                if ($user->nickname != null) $data['username'] = $user->nickname;
                $data['email'] = $user->email;
                $data['lastname'] = $user->user['last_name'];
                $data['firstname'] = $user->user['first_name'];
                if ($user->user['gender'] == 'male') {
                    $data['gender'] = 1;
                } else {
                    $data['gender'] = 0;
                }
                if (!empty($user->user['birthday'])){
                    $birthday = explode('/', $user->user['birthday']);
                    $data['birthday'] = $birthday[1].'/'.$birthday[0].'/'.$birthday[2];
                }
                // On ne met pas en BDD parce que il y est déjà
                return redirect('auth/register')->withInput($data);
            }else{
                $auth = AuthSocial::where('key', $user->id)->where('network', 'facebook')->first();
                Auth::loginUsingId($auth->user);
                return redirect('/');
            }
        }
	}

    public function twitter_redirect() {
        return Socialite::with('twitter')->redirect();
    }

    public function twitter() {
        if(!empty($_GET['error'])){
            return redirect('/')->with('error_social_auth', 'twitter');
        }else{
            $user = Socialite::with('twitter')->user();
            // Dans le cas ou il n'existe meme pas en BDD
            if(AuthSocial::where('key', $user->id)->where('network', 'twitter')->count() == 0) {
                $data = array();
                $data['key_social'] = 'twitter-' . $user->id;
                if ($user->nickname != null) $data['username'] = $user->nickname;
                $data['email'] = $user->email;
                $data['firstname'] = $user->name;

                $external_data = array(); // TOUTES LES AUTRES DONNES A SAV A COTE
                $external_data['twitter'] = 'https://twitter.com/'.$user->nickname;
                $external_data['avatar_online'] = $user->avatar_original;
                $external_data['banner_online'] = $user->user['profile_banner_url'].'/600x200';
                // Création en BDD avec user null que l'on retrouvera avec la clé
                AuthSocial::create([
                    'network' => 'twitter',
                    'key' => $user->id,
                    'data' => json_encode($external_data),
                ]);
                return redirect('auth/register')->withInput($data);
            }elseif(AuthSocial::where('key', $user->id)->where('network', 'twitter')->where('user', 0)->count() > 0){
                // Déjà cliqué que le bouton mais ne c'est pas inscrit
                $data = array();
                $data['key_social'] = 'twitter-' . $user->id;
                if ($user->nickname != null) $data['username'] = $user->nickname;
                $data['email'] = $user->email;
                $data['firstname'] = $user->name;
                // On ne met pas en BDD parce que il y est déjà
                return redirect('auth/register')->withInput($data);
            }else{
                $auth = AuthSocial::where('key', $user->id)->where('network', 'twitter')->first();
                Auth::loginUsingId($auth->user);
                return redirect('/');
            }
        }
    }

    public function google_redirect() {
        return Socialite::with('google')->redirect();
    }

    public function google() {
        if(!empty($_GET['error'])){
            return redirect('/')->with('error_social_auth', 'google');
        }else{
            $user = Socialite::with('google')->user();
            // Dans le cas ou il n'existe meme pas en BDD
            if(AuthSocial::where('key', $user->id)->where('network', 'google')->count() == 0) {
                $data = array();
                $data['key_social'] = 'google-' . $user->id;
                if ($user->nickname != null) $data['username'] = $user->nickname;
                $data['email'] = $user->email;
                $data['firstname'] = $user->user['name']['givenName'];
                $data['lastname'] = $user->user['name']['familyName'];
                if ($user->user['gender'] == 'male') {
                    $data['gender'] = 1;
                } else {
                    $data['gender'] = 0;
                }

                $external_data = array(); // TOUTES LES AUTRES DONNES A SAV A COTE
                $external_data['google'] = $user->user['url'];
                $external_data['avatar_online'] = str_replace('sz=50', 'sz=500', $user->avatar);

                // Création en BDD avec user null que l'on retrouvera avec la clé
                AuthSocial::create([
                    'network' => 'google',
                    'key' => $user->id,
                    'data' => json_encode($external_data),
                ]);
                return redirect('auth/register')->withInput($data);
            }elseif(AuthSocial::where('key', $user->id)->where('network', 'google')->where('user', 0)->count() > 0){
                // Déjà cliqué que le bouton mais ne c'est pas inscrit
                $data = array();
                $data['key_social'] = 'google-' . $user->id;
                if ($user->nickname != null) $data['username'] = $user->nickname;
                $data['email'] = $user->email;
                $data['firstname'] = $user->user['name']['givenName'];
                $data['lastname'] = $user->user['name']['familyName'];
                if ($user->user['gender'] == 'male') {
                    $data['gender'] = 1;
                } else {
                    $data['gender'] = 0;
                }
                // On ne met pas en BDD parce que il y est déjà
                return redirect('auth/register')->withInput($data);
            }else{
                $auth = AuthSocial::where('key', $user->id)->where('network', 'google')->first();
                Auth::loginUsingId($auth->user);
                return redirect('/');
            }
        }
    }
}