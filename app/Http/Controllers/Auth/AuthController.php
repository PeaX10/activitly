<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\PasswordReset;
use Mail;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function getReset(){
		return view('auth.reset');
	}

	public function postReset(Request $request){

        $rules = [  'new_password' => 'required|min:6',
                    'new_password_confirm' => 'required|same:new_password',
        ];

        if (filter_var(Input::get('username'), FILTER_VALIDATE_EMAIL)){
            $rules['username'] = 'required|exists:users,email';
        }else{
            $rules['username'] = 'required|exists:users,username';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/password/reset')
                ->withErrors($validator)
                ->withInput();
        }else{
            if (filter_var(Input::get('username'), FILTER_VALIDATE_EMAIL)){
                $user = User::where('email', Input::get('username'))->first();
            }else{
                $user = User::where('username', Input::get('username'))->first();
            }

            // Tant que que le token unique n'est pas unique, on va en chercher un nouveau
            $unique = false;
            while($unique === false){
                $data['token'] = str_random('48');
                if(User::where('token', '=', $data['token'])->count() == 0) $unique = true;
            }

            $user->token = $data['token'];

            $data['email'] = $user->email;
            $data['lastname'] = $user->lastname;
            $data['firstname'] = $user->firstname;
            $data['username'] = $user->username;
            $data['password'] = Input::get('new_password');

            $password = Hash::make(Input::get('new_password'));
            $passwordReset = PasswordReset::where('user', $user->id);
            if($passwordReset->count() != 0) $passwordReset->first()->delete();
            PasswordReset::create(['user' => $user->id, 'password' => $password]);

            /*
            Mail::send('mail.password_reset', $data, function($message) use($data)
            {
                $message->to($data['email'], ucfirst(strtolower($data['lastname'])).' '.ucfirst(strtolower($data['firstname'])))->subject('Activitly - Mot de passe oublié');
            });
            */

            if(count(Mail::failures()) != 0 ) {
                return redirect('/password/reset')->withInput()->with('error_password_reset', true);
            }else{
                $user->save();
                return redirect('/password/reset')->with('success_password_reset', true);
            }
        }
	}

    public function confirmReset($token){
        $user = User::where('token', '=', $token)->first();
        if(!empty($user)){
            $password = PasswordReset::where('user', $user->id)->first();
            $user->password = $password->password;
            $password->delete();
            $user->token = null;
            $user->save();
            return redirect('/auth/login')->with('success_confirm_reset_password', true);
        }else{
            return redirect('/');
        }
    }

    public function getResetEmail(){
        return view('auth.email');
    }

    public function postResetEmail(Request $request){

        // Ajout d'une règle de validation du password
        Validator::extend('passcheck', function ($attribute, $value, $parameters)
        {
            $user = User::where('username', Input::get('username'));
            if($user->count() != 0){
                return Hash::check($value, $user->first()->password);
            }else{
                return false;
            }
        });

        // Ajout d'une règle de validation de l'email
        Validator::extend('emailcheck', function ($attribute, $value, $parameters)
        {
            $user = User::where('username', Input::get('username'));
            if($user->count() != 0){
                if($user->first()->email != Input::get('email')){
                    // On cherche si l'email est associé
                    $email = User::where('email', Input::get('email'));
                    if($email->count() != 0){
                        return false;
                    }else{
                        return true;
                    }
                }else{
                    return true;
                }
                return Hash::check($value, $user->password);
            }else{
                return true;
            }
        });

        $rules = [  'username' => 'required|exists:users,username',
                    'password_infos' => 'required|passcheck',
                    'email' => 'required|email|emailcheck'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/email/reset')
                ->withErrors($validator)
                ->withInput();
        }else{
            // Tant que que le token unique n'est pas unique, on va en chercher un nouveau
            $unique = false;
            while($unique === false){
                $data['token'] = str_random('48');
                if(User::where('token', '=', $data['token'])->count() == 0) $unique = true;
            }

            $user = User::where('username', Input::get('username'))->first();
            $user->email = Input::get('email');
            $user->validate = 0;
            $user->token = $data['token'];
            $data['email'] = $user->email;
            $data['lastname'] = $user->lastname;
            $data['firstname'] = $user->firstname;
            $data['username'] = $user->username;
            $data['password'] = Input::get('password_infos');

            /*
            Mail::send('mail.confirm_account', $data, function($message) use($data)
            {
                $message->to($data['email'], ucfirst(strtolower($data['lastname'])).' '.ucfirst(strtolower($data['firstname'])))->subject('Activitly - Valider mon adresse mail');
            });
            */

            if(count(Mail::failures()) != 0 ) {
                return redirect('/email/reset')->withInput()->with('error_email_reset', true);
            }else{
                $user->save();
                return redirect('/')->with('success_email_reset', true);
            }
        }
    }

}
