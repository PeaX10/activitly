<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

# ACCUEIL
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    // Form Newsletter
Route::post('/newsletter', ['as' => 'newsletter.register', 'uses' => 'HomeController@newsletter']);

# PAGES
Route::get('/terms', ['as' => 'pages', 'uses' => 'HomeController@terms']);
Route::get('/privacy', ['as' => 'privacy', 'uses' => 'HomeController@privacy']);
Route::get('/contact', ['as' => 'contact', 'uses' => 'HomeController@contact']);
Route::post('/contact', ['as' => 'contact.post', 'uses' => 'HomeController@postContact']);


# INTERFACE MEMBRES
Route::group(['middleware' => 'complete_account'], function(){
    Route::get('account', ['as' => 'account.index', 'uses' => 'Account\AccountController@index']);
    Route::post('account/edit/infos', ['as' => 'account.edit.infos', 'uses' => 'Account\AccountController@editInfos']);
    Route::post('account/edit/profile', ['as' => 'account.edit.profile', 'uses' => 'Account\AccountController@editProfile']);
    Route::get('board', ['as' => 'board.index', 'uses' => 'Account\PanelController@index']);
    Route::get('activity/create', ['as' => 'activity.create.get', 'uses' => 'Account\PanelController@createActivity']);
    Route::post('activity/create', ['as' => 'activity.create.post', 'uses' => 'Account\PanelController@createActivityPost']);
    Route::get('activity', ['as' => 'activity.get', 'uses' => 'Account\PanelController@getActivities']);
    Route::get('activity/next', ['as' => 'activity.next.get', 'uses' => 'Account\PanelController@getNextActivities']);
    Route::get('activity/prev', ['as' => 'activity.prev.get', 'uses' => 'Account\PanelController@getPrevActivities']);
    Route::get('activity/invites', ['as' => 'activity.invite.get', 'uses' => 'Account\PanelController@getInvites']);
    Route::post('activity/invites', ['as' => 'activity.invite.post', 'uses' => 'Account\PanelController@DelInvites']);
    Route::post('profile/{pseudo}', ['as' => 'profile.post', 'uses' => 'Account\PanelController@postOnProfile']);
    Route::post('activity/{id}/view', ['as' => 'activity.post', 'uses' => 'Account\PanelController@postOnActivity']);
    Route::get('activity/{id}/delete', ['as' => 'activity.delete', 'uses' => 'Account\PanelController@deleteActivity']);
    Route::get('post/profile/{id}/delete', ['as' => 'post.delete', 'uses' => 'Account\PanelController@deletePostProfile']);
    Route::get('post/activity/{id}/delete', ['as' => 'activity.delete', 'uses' => 'Account\PanelController@deletePostActivity']);
    Route::get('profile/{pseudo}/friend/add', ['as' => 'profile.friend.add', 'uses' => 'Account\PanelController@addFriend']);
    Route::get('profile/{pseudo}/friend/delete', ['as' => 'profile.friend.delete', 'uses' => 'Account\PanelController@deleteFriend']);
    Route::get('friends', ['as' => 'profile.friends', 'uses' => 'Account\PanelController@getFriends']);
    Route::get('friends', ['as' => 'profile.friends', 'uses' => 'Account\PanelController@getFriends']);
});
Route::get('activity/{id}/view', ['as' => 'activity.view', 'uses' => 'Account\PanelController@showActivity']);
Route::get('profile/{pseudo}', ['as' => 'profile.get', 'uses' => 'Account\PanelController@showProfile']);
Route::get('search', ['as' => 'profile.search', 'uses' => 'Account\PanelController@search']);
Route::get('account/complete', ['as' => 'complete_account', 'uses' => 'Account\AccountController@completeAccount', 'middleware' => 'auth']);
Route::post('account/complete', ['as' => 'post_complete_account', 'uses' => 'Account\AccountController@verifCompleteAccount', 'middleware' => 'auth']);

# CONNEXION / INSCRIPTION / MOT DE PASSE
Route::controllers([
	'auth' => 'Auth\AuthController',
]);
Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\AuthController@getReset']);
Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\AuthController@postReset']);
Route::get('password/confirm/{token}', ['as' => 'password.confirm', 'uses' => 'Auth\AuthController@confirmReset']);
Route::get('email/reset', ['as' => 'email.reset', 'uses' => 'Auth\AuthController@getResetEmail']);
Route::post('email/reset', ['as' => 'email.reset.post', 'uses' => 'Auth\AuthController@postResetEmail']);
Route::get('account/confirm/{token}', ['as' => 'confirm_register', 'uses' => 'Account\AccountController@confirmAccount']);
    // Social Login
        # Facebook
Route::get('facebook', 'Auth\SocialController@facebook_redirect');
Route::get('social_auth/facebook', 'Auth\SocialController@facebook');
        # Twitter
Route::get('twitter', 'Auth\SocialController@twitter_redirect');
Route::get('social_auth/twitter', 'Auth\SocialController@twitter');
        # Google Plus
Route::get('google', 'Auth\SocialController@google_redirect');
Route::get('social_auth/google', 'Auth\SocialController@google');

# JSON ARRAY
Route::get('json/activities', 'OtherController@getActivities');

# AUTRES
Route::get('/google98fc6fcaba38269b.html', ['as' => 'googleVerif', 'uses' => 'HomeController@googleVerif']); // Vérification Google

