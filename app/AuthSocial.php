<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthSocial extends Model {

    protected $table = 'auth_socials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user', 'network', 'key', 'data'];

}
