<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Friendship extends Model {

    protected $fillable = ['id', 'user_id', 'friend_id', 'created_at'];
}
