<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model {

    protected $table = 'invites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'sender', 'recipient', 'event', 'message', 'visible', 'created_at'];

}
