<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

    protected $table = 'events';

	protected $fillable = ['id', 'title', 'owner', 'lat', 'lng', 'location', 'adress', 'activity_id', 'place', 'date', 'description', 'options', 'cover', 'start_time', 'end_time'];

}
