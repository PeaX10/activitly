<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsletterUser extends Model {

    protected $table = 'newsletter_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'email'];

}
