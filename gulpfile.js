var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {
    // Compile CSS File
    mix.less(['gsdk.less'])
        .styles(['home.css'], 'public/css/home.css')
        .styles(['cropper.css'], 'public/css/cropper.css')
        .styles(['pe-icon-7-stroke.css'], 'public/css/pe-icons.css')
        .styles(['bootstrap.css'], 'public/css/bootstrap.css')
        .styles(['global.css'], 'public/css/global.css')
        .styles(['board.css'], 'public/css/board.css')
        .sass('style.scss');
    // Compile JS script
    mix.scripts(['jquery-1.10.2.js', 'jquery-ui-1.10.4.custom.min.js', 'jquery.tagsinput.js',
        'jquery.flexisel.js', 'jquery.geocomplete.js'], 'public/js/jquery.js')
        .scripts(['gsdk-checkbox.js', 'gsdk-morphing.js', 'gsdk-radio.js',
            'gsdk-bootstrapswitch.js', 'get-shit-done.js'], 'public/js/gsdk.js')
        .scripts(['bootstrap.js', 'bootstrap-datepicker.js', 'locales/bootstrap-datepicker.fr.min.js', 'bootstrap-select.js', 'bootstrap-typeahead.js', 'bootstrap-tagsinput.min.js', 'bootstrap-clockpicker.js'], 'public/js/bootstrap.js')
        .scripts(['chartist.min.js'], 'public/js/chartist.min.js')
        .scripts(['home.js'], 'public/js/home.js')
        .scripts(['account.js', 'cropper.min.js'], 'public/js/account.js')
        .scripts(['complete_profile.js', 'cropper.min.js'], 'public/js/complete_profile.js')
        .scripts(['add_activity.js'], 'public/js/add_activity.js')
        .scripts(['search.js'], 'public/js/search.js')
        .scripts(['baron.min.js', 'before.load.js', 'dropzone.min.js', 'icheck.min.js', 'ie-scripts.js', 'imagesloaded.pkgd.min.js', 'infobox.js', 'jquery.hotkeys.js', 'jquery.imageloader.js', 'jquery.lazyload.min.js', 'jquery.mCustomScrollbar.concat.min.js', 'jquery.nouislider.all.min.js', 'jquery.raty.min.js', 'jquery.smoothwheel.js', 'jquery.ui.map.full.min.js', 'jquery.ui.timepicker.js', 'jquery.validate.min.js', 'leaflet-providers.js', 'leaflet.js', 'leaflet.markercluster.js', 'load.scripts.js', 'maps.backup.js', 'maps.js', 'maps.old.js', 'markerclusterer.js', 'moment-with-locales.min.js', 'oms.min.js', 'owl.carousel.min.js', 'richmarker-compiled.js', 'richmarker.js', 'smoothscroll.js'], 'public/js/board.js');
});
