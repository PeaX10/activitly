var big_image;
$().ready(function(){
    responsive = $(window).width();

    $(window).on('scroll', gsdk.checkScrollForTransparentNavbar);

    if (responsive >= 768){
        big_image = $('.parallax-image').find('img');

        $(window).on('scroll',function(){
            parallax();
        });
    }

});

var parallax = function() {
    var current_scroll = $(this).scrollTop();

    oVal = ($(window).scrollTop() / 3);
    big_image.css('top',oVal);
};

// Get all Activities
$.getJSON( "http://activitly.tagyf.com/json/activities", function( data ) {
    $('#activities').typeahead({
        source: data,
    });
});

$("#cities").geocomplete({
    types: ['(cities)'],
    componentRestrictions: { country: 'fr' }
});