<footer class="footer footer-transparent" style="background: #333">

    <div class="container">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="{{ url('/') }}">
                        Accueil
                    </a>
                </li>
                <li>
                    <a href="{{ url('/contact') }}">
                        Contact
                    </a>
                </li>
                <li>
                    <a href="{{ url('/terms') }}">
                        Conditions Générales d'utilisation
                    </a>
                </li>
                <li>
                    <a href="{{ url('/privacy') }}">
                        Mentions légales
                    </a>
                </li>
            </ul>
        </nav>
        <div class="social-area pull-right">
            <a href="http://facebook.com/Activitly" target="_blank">
                <i class="fa fa-facebook-square"></i>
            </a>
            <a href="http://twitter.com/Activitly" target="_blank">
                <i class="fa fa-twitter"></i>
            </a>
            <a href="http://google.com/+Activitly_com" target="_blank">
                <i class="fa fa-pinterest"></i>
            </a>
        </div>
        <div class="copyright">
            <a target="_blank" href="http://activitly.copyright01.com/">© 2015 Activitly - Touts droits réservés</a>
        </div>
    </div>
</footer>