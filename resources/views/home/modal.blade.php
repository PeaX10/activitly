<div class="modal modal-small fade" id="loginBox" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Connexion avec</h4>
            </div>
            <div class="modal-body">
                <div class="social-area">
                    <a href="{!!URL::to('twitter')!!}" class="btn btn-round btn-fill btn-social btn-twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="{!!URL::to('facebook')!!}" class="btn btn-round btn-fill btn-social btn-facebook">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                    <a href="{!!URL::to('google')!!}" class="btn btn-round btn-fill btn-social btn-google">
                        <i class="fa fa-google-plus-square"></i>
                    </a>
                </div>
                <span class="divider"> ou </span>

                {!! BootForm::open()->post()->action(url('auth/login')) !!}
                <input type="hidden" name="home" value="{{ csrf_token() }}" />
                {!! BootForm::text(null, 'username')->placeholder('Pseudo ou  E-mail') !!}
                {!! BootForm::password(null, 'password')->placeholder('Mot de passe') !!}
                {!! BootForm::checkbox('Se souvenir de moi', 'remember')->data_toggle('checkbox') !!}
                {!! BootForm::submit('Connexion')->addClass('btn-info btn-fill btn-block') !!}
                {!! BootForm::close() !!}
            </div>
            <div class="modal-footer">
                <span class="text-muted">Pas encore de compte ? <a data-dismiss="modal" data-toggle="modal" data-target="#registerBox" href="#">Créer un compte</a> !</span>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-small fade" id="registerBox" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">S'inscrire avec</h4>
            </div>
                <div class="modal-body">
                    <div class="social-area">
                        <a href="{!!URL::to('twitter')!!}" class="btn btn-round btn-fill btn-social btn-twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="{!!URL::to('facebook')!!}" class="btn btn-round btn-fill btn-social btn-facebook">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <a href="{!!URL::to('google')!!}" class="btn btn-round btn-fill btn-social btn-google">
                            <i class="fa fa-google-plus-square"></i>
                        </a>
                    </div>
                <span class="divider"> ou </span>
                {!! BootForm::open()->post()->action(url('auth/register')) !!}
                    <input type="hidden" name="home" value="{{ csrf_token() }}" />
                    {!! BootForm::text(null, 'username')->placeholder('Pseudo') !!}
                    {!! BootForm::text(null, 'email')->placeholder('E-mail') !!}
                    {!! BootForm::submit('S\'inscrire')->addClass('btn-info btn-fill btn-block') !!}
                {!! BootForm::close() !!}
            </div>

            <div class="modal-footer">
                <span class="text-muted">Vous avez déjà un compte ? <a data-dismiss="modal" data-toggle="modal" data-target="#loginBox" href="#">connectez-vous</a> !</span>
            </div>
        </div>
    </div>
</div>