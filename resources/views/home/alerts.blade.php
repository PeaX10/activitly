@if(!empty(Session::get('message')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation</b> Votre compte a bien été créer, vous allez recevoir un e-mail sous peu afin d'activer votre compte.
        </div>
    </div>
@elseif(!empty(Session::get('validate')))
    <div class="alert alert-warning">
        <div class="container">
            <b>Attention</b> Merci de valider votre compte pour vous connecter. <a href="{{ url('email/reset') }}" style="color:#000" class="pull-right">Je n'ai pas reçu de mail de validation</a>
        </div>
    </div>
@elseif(!empty(Session::get('success_newsletter')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation</b> Vous êtes maintenant inscrit à notre newsletter !
        </div>
    </div>
@elseif(!empty(Session::get('create_msg')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation</b> Votre compte est maintenant activé, vous pouvez dès à présent vous connecter et profiter d'Activitly !
        </div>
    </div>
@elseif(!empty(Session::get('error')))
    <div class="alert alert-danger">
        <div class="container">
            <b>Erreur :</b> Une erreur est survenue, merci de bien vouloir recommencer.
        </div>
    </div>
@elseif(!empty(Session::get('error_social_auth')))
    <div class="alert alert-danger">
        <div class="container">
            <b>Erreur :</b> Impossible de vous connecter avec {{  Session::get('error_social_auth') }} !
        </div>
    </div>
@elseif(!empty(Session::get('success_contact')))
    <div class="alert alert-success">
        <div class="container">
            <b>Merci :</b> Votre demande de contact a bien été envoyé !
        </div>
    </div>
@elseif(!empty(Session::get('contact_failed')))
    <div class="alert alert-danger">
        <div class="container">
            <b>Erreur :</b> Une erreur lors de l'envoi du message est survenue, merci de recommencer !
        </div>
    </div>
@elseif(!empty(Session::get('success_edit_account_infos')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre compte a été mis à jour avec succès !
        </div>
    </div>
@elseif(!empty(Session::get('success_profile_updated')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre profil a été mis à jour avec succès !
        </div>
    </div>
@elseif(!empty(Session::get('error_email_updated')))
    <div class="alert alert-danger">
        <div class="container">
            <b>Erreur :</b> Une erreur lors de la modification de votre adresse mail est survenue, merci de recommencer !
        </div>
    </div>
@elseif(!empty(Session::get('email_updated')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre adresse mail a bien été modifiée, merci de vérifier vos e-mails afin de la confirmer et pouvoir vous reconnecter !
        </div>
    </div>
@elseif(!empty(Session::get('error_password_reset')))
    <div class="alert alert-success">
        <div class="container">
            <b>Erreur :</b> Une erreur est survenue, merci de recommencer !
        </div>
    </div>
@elseif(!empty(Session::get('success_password_reset')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre demande a bien été pris en compte, merci de vérifier vos e-mails afin d'activer votre nouveau mot de passe !
        </div>
    </div>
@elseif(!empty(Session::get('error_email_reset')))
    <div class="alert alert-success">
        <div class="container">
            <b>Erreur :</b> Une erreur est survenue, merci de recommencer !
        </div>
    </div>
@elseif(!empty(Session::get('success_email_reset')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre demande a bien été pris en compte, merci de vérifier vos e-mails afin d'activer votre compte !
        </div>
    </div>
@elseif(!empty(Session::get('success_confirm_reset_password')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre nouveau mot de passe est maintenant activé !
        </div>
    </div>
@elseif(!empty(Session::get('success_activity_create')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre activité a bien été créer !
        </div>
    </div>
@elseif(!empty(Session::get('success_invite_deleted')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> L'invitation a bien été refusé !
        </div>
    </div>success_friend_add
@elseif(!empty(Session::get('success_post')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre publication est maintenant est ligne !
        </div>
    </div>
@elseif(!empty(Session::get('success_friend_add')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Vous suivez maintenant &#64;{{ $profile->username }} !
        </div>
    </div>
@elseif(!empty(Session::get('success_friend_delete')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Vous ne suivez maintenant plus &#64;{{ $profile->username }} !
        </div>
    </div>
@elseif(!empty(Session::get('success_post_deleted')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre publication a bien été supprimé !
        </div>
    </div>
@elseif(!empty(Session::get('success_activity_deleted')))
    <div class="alert alert-success">
        <div class="container">
            <b>Félicitation :</b> Votre activité a bien été supprimé !
        </div>
    </div>
@elseif($errors->has())
    <div class="alert alert-warning">
        <div class="container">
            <b>Attention :</b> Il y a une ou plusieurs erreurs, merci de bien vouloir recommencer.
        </div>
    </div>
@else
    <div class="alert alert-info">
        <div class="container">
            <b>Activitly</b> est actuellement en construction, vous pouvez néanmoins vous préinscrire. Vous serez averti par e-mail lors de son ouverture.
        </div>
    </div>
@endif