@extends('default')

@section('title')
    Connectez-vous à la vie réelle
@endsection

@section('content')
    <div class="wrapper">
        <div class="parallax">
            <div class="parallax-image">
                <img src="{{ asset('img/header.jpg') }}">
            </div>
            <div class="small-info">
                <h1>Rejoignez ou hébergez des activités.</h1>
                <h3>Faites-vous de nouveaux amis tout en vous amusant.</h3>
            </div>
            <div class="search-marker">
                <i class="search-marker"></i>
            </div>
            <div class="search-activity">
                <div class="container">
                    <h3>Rechercher une activité</h3>
                    <div class="row">
                        {!! BootForm::open()->get()->action(url('/search')) !!}
                        <div class="col-md-4 col-sm-12">
                            <div class="input-group">
                                <input id="activities" name="activity" type="text" class="col-md-12 form-control" placeholder="Quelle activité ?" />
                                <span class="input-group-addon"><i class="fa fa-child"></i></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="input-group">
                                <input id="cities" name="city" class="form-control" type="text" placeholder="Quelle ville ?" />
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="input-group">
                                <input class="form-control" name="date" type="text" id="date" placeholder="Quel jour ?" />
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-0"></div>
                        <div class="col-md-4 col-sm-12" align="center">
                            <input type="submit" class="btn btn-info btn-round btn-fill btn-block" value="Rechercher">
                        </div>
                        <div class="col-md-4 col-sm-0"></div>
                        {!! BootForm::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-testimonials section-gray">
            <div class="container">
                <h2 class="tim-title text-center">Activités populaires...</h2>
                <div id="best-activities-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card card-background">
                                        <div class="image" style="background-image: url({{ asset('img/header.jpg') }}); background-size: cover; background-position: 50% 50%;">
                                            <img src="{{ asset('img/header.jpg') }}" alt="..." style="display: none;">
                                            <div class="filter"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="category">Course à pied
                                                <a href="#" class="pull-right">
                                                    <i class="fa fa-map-marker"></i> Dijon
                                                </a>
                                            </h5>
                                            <a href="#">
                                                <h4 class="title">Courir autour du Lac</h4>
                                            </a>
                                        </div>
                                        <div class="footer">
                                            <div class="author">
                                                <a href="#">
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Alexandre Petit </span>
                                                </a>
                                            </div>
                                            <div class="stats pull-right">
                                                <i class="fa fa-clock-o"></i> 12h - 13h30
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-background">
                                        <div class="image" style="background-image: url({{ asset('img/header.jpg') }}); background-size: cover; background-position: 50% 50%;">
                                            <img src="{{ asset('img/header.jpg') }}" alt="..." style="display: none;">
                                            <div class="filter"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="category">Course à pied
                                                <a href="#" class="pull-right">
                                                    <i class="fa fa-map-marker"></i> Dijon
                                                </a>
                                            </h5>
                                            <a href="#">
                                                <h4 class="title">Courir autour du Lac</h4>
                                            </a>
                                        </div>
                                        <div class="footer">
                                            <div class="author">
                                                <a href="#">
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Alexandre Petit </span>
                                                </a>
                                            </div>
                                            <div class="stats pull-right">
                                                <i class="fa fa-clock-o"></i> 12h - 13h30
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-background">
                                        <div class="image" style="background-image: url({{ asset('img/header.jpg') }}); background-size: cover; background-position: 50% 50%;">
                                            <img src="{{ asset('img/header.jpg') }}" alt="..." style="display: none;">
                                            <div class="filter"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="category">Course à pied
                                                <a href="#" class="pull-right">
                                                    <i class="fa fa-map-marker"></i> Dijon
                                                </a>
                                            </h5>
                                            <a href="#">
                                                <h4 class="title">Courir autour du Lac</h4>
                                            </a>
                                        </div>
                                        <div class="footer">
                                            <div class="author">
                                                <a href="#">
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Alexandre Petit </span>
                                                </a>
                                            </div>
                                            <div class="stats pull-right">
                                                <i class="fa fa-clock-o"></i> 12h - 13h30
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card card-background">
                                        <div class="image" style="background-image: url({{ asset('img/header.jpg') }}); background-size: cover; background-position: 50% 50%;">
                                            <img src="{{ asset('img/header.jpg') }}" alt="..." style="display: none;">
                                            <div class="filter"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="category">Course à pied
                                                <a href="#" class="pull-right">
                                                    <i class="fa fa-map-marker"></i> Dijon
                                                </a>
                                            </h5>
                                            <a href="#">
                                                <h4 class="title">Courir autour du Lac</h4>
                                            </a>
                                        </div>
                                        <div class="footer">
                                            <div class="author">
                                                <a href="#">
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Alexandre Petit </span>
                                                </a>
                                            </div>
                                            <div class="stats pull-right">
                                                <i class="fa fa-clock-o"></i> 12h - 13h30
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-background">
                                        <div class="image" style="background-image: url({{ asset('img/header.jpg') }}); background-size: cover; background-position: 50% 50%;">
                                            <img src="{{ asset('img/header.jpg') }}" alt="..." style="display: none;">
                                            <div class="filter"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="category">Course à pied
                                                <a href="#" class="pull-right">
                                                    <i class="fa fa-map-marker"></i> Dijon
                                                </a>
                                            </h5>
                                            <a href="#">
                                                <h4 class="title">Courir autour du Lac</h4>
                                            </a>
                                        </div>
                                        <div class="footer">
                                            <div class="author">
                                                <a href="#">
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Alexandre Petit </span>
                                                </a>
                                            </div>
                                            <div class="stats pull-right">
                                                <i class="fa fa-clock-o"></i> 12h - 13h30
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-background">
                                        <div class="image" style="background-image: url({{ asset('img/header.jpg') }}); background-size: cover; background-position: 50% 50%;">
                                            <img src="{{ asset('img/header.jpg') }}" alt="..." style="display: none;">
                                            <div class="filter"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="category">Course à pied
                                                <a href="#" class="pull-right">
                                                    <i class="fa fa-map-marker"></i> Dijon
                                                </a>
                                            </h5>
                                            <a href="#">
                                                <h4 class="title">Courir autour du Lac</h4>
                                            </a>
                                        </div>
                                        <div class="footer">
                                            <div class="author">
                                                <a href="#">
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Alexandre Petit </span>
                                                </a>
                                            </div>
                                            <div class="stats pull-right">
                                                <i class="fa fa-clock-o"></i> 12h - 13h30
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <a class="left carousel-control" href="#best-activities-carousel" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#best-activities-carousel" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-abous-us">
        <div class="container">
            <h2 class="tim-title text-center">Activitly c'est aussi...</h2>
            <div class="row">
                <div class="col-md-4">
                    <div class="info">
                        <div class="icon icon-green">
                            <i class="fa fa-child"></i>
                        </div>
                        <div class="description">
                            <h3> Du Sport </h3>
                            <p>Tous les jours des milliers de membres vous proposent des activités sportives.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info">
                        <div class="icon icon-azure">
                            <i class="fa fa-glass"></i>
                        </div>
                        <div class="description">
                            <h3> Des sorties </h3>
                            <p>Un Cinéma ? La visite d'un musée ? Découvrer les sorties proposées par nos membres.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info">
                        <div class="icon icon-orange">
                            <i class="fa fa-heart"></i>
                        </div>
                        <div class="description">
                            <h3> Des rencontres </h3>
                            <p>Apprenez à connaître les gens qui vous entourent dans une bonne ambiance.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="info">
                        <div class="icon icon-red">
                            <i class="fa fa-fire"></i>
                        </div>
                        <div class="description">
                            <h3> De nouvelles sensations </h3>
                            <p>Dépassez-vous, à plusieurs trouver la force de vous surpasser afin d'atteindre vos objectifs sans vous décourager.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="info">
                        <div class="icon icon-purple">
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <div class="description">
                            <h3> Une réputation à tenir </h3>
                            <p>Sur Activitly à la fin de chaque activité vous pouvez noter les membres et leur demander une demande d'ami.</p>
                        </div>
                    </div>
                </div>
                <div class="space-50"></div>
                <div class="col-md-4 col-md-offset-4">
                    <button type="button" data-toggle="modal" data-target="#registerBox" class="btn btn-info btn-round btn-fill btn-block">Pré-inscription</button>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-testimonials section-gray">
        <div class="container">
            <h2 class="tim-title text-center">Ce qu'ils en pensent...</h2>
            <div id="testimonials-carousel" class="carousel slide" data-ride="carousel">

                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="content">
                                        <a target="_blank" href="#">
                                            <h4 class="title">J'avais envie de bouger et faire du sport, Activitly est parfait pour moi.</h4>
                                        </a>
                                        <div class="footer">
                                            <div class="author">
                                                <a>
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Michelle Benoît </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="content">
                                        <h4 class="title">Je trouve le principe de ce site vraiment super, c'est vraiment le site qu'il me fallait.
                                        </h4>

                                        <div class="footer">
                                            <div class="author">
                                                <a target="_blank" href="https://twitter.com/tudorvintilescu">
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Alex Simon </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="content">
                                        <a target="_blank" href="">
                                            <h4 class="title">Un site super-simple et convivial, grâce à lui j'ai fait de super rencontre.</h4>
                                        </a>
                                        <div class="footer">
                                            <div class="author">
                                                <a target="_blank" href="http://www.awwwards.com/">
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Lucie Bouie </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="content">
                                        <a target="_blank" href="http://www.creative-tim.com/product/get-shit-done-pro">
                                            <h4 class="title">Déjà un an où j'utilise ce site et vraiment, je n'ai rien à dire. Continuer comme ceci.</h4>
                                        </a>
                                        <div class="footer">
                                            <div class="author">
                                                <a>
                                                    <img src="{{ asset('img/header.jpg') }}" alt="..." class="avatar">
                                                    <span> Mathilde De Lamarre </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <a class="left carousel-control" href="#testimonials-carousel" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#testimonials-carousel" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="container" id="subscription">
        <div class="tim-title">
            <h3>Suivez-nous</h3>
        </div>
    </div>
    <div class="subscribe-line subscribe-line-transparent" style="background-image: url('http://37.media.tumblr.com/d77e21ed167c2125627b210b48e23f81/tumblr_na0kw25OtD1st5lhmo1_1280.jpg')">
        <div class="container">
            <div class="row">
                    {!! BootForm::open()->action(url('/newsletter')) !!}
                    <div class="col-md-9">
                        <div class="form-group">
                            {!! BootForm::text(null, 'email')->placeholder('Entrer votre adresse mail') !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        {!! BootForm::submit('S\'inscrire à la newsletter !', null)->class('btn btn-info btn-fill btn-block') !!}
                    </div>
                    {!! BootForm::close() !!}
            </div>
        </div>
    </div>

    <div class="social-line">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <a href="http://facebook.com/Activitly" target="_blank" class="btn btn-round btn-fill btn-social btn-facebook">
                        <i class="fa fa-facebook-square"></i> Facebook
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="http://twitter.com/Activitly" target="_blank" class="btn btn-round btn-fill btn-social btn-twitter">
                        <i class="fa fa-twitter"></i> Twitter
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="http://google.com/+Activitly_com" target="_blank" class="btn btn-round btn-fill btn-social btn-google">
                        <i class="fa fa-google-plus-square"></i> Google+
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $( document ).ready(function() {
            $('input#date').datepicker({
                format: "dd/mm/yyyy",
                weekStart: 1,
                startDate: "today",
                endDate: "+2m",
                language: "fr",
                orientation: "top auto",
                autoclose: true,
                toggleActive: true,
                todayBtn: "linked",
                todayHighlight: true
            });
        });
    </script>
@endsection