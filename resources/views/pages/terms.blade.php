@extends('default')

@section('title')
    Conditions Générales d'utilisation
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="container">
        <div id="legend">
            <h2>Conditions Générales d'utilisation</h2>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <h6>CONDITIONS GÉNÉRALES D'UTILISATION DES L'APPLICATIONS ACTIVITLY ET DU SITE INTERNET WWW.ACTIVITLY.COM</h6>
            <hr>
            <p>L’utilisation de l’application mobile “ACTIVITLY ” vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation (ci-après les “CGU”).</p>
            <p>Le présent contrat (ci-après le «Contrat») constitue les conditions générales d'utilisation entre vous personne physique ou morale unique (ci-après « l'Utilisateur ») et l’éditeur de l’application ACTIVITLY, Mr PETIT Alexandre - contact@activitly.com</p>
            <p>L’application sera ci-après mentionnée en tant que « ACTIVITLY », son nom usuel.</p>
            <p>Elle est représentée par Mr Petit Alexandre, en sa qualité d’éditeur.</p>
            <p>(email : contact@activitly.com)</p>
            <p>Le Contrat est applicable à l'application ACTIVITLY et ce compris, le cas échéant, les mises à jour de celle-ci (ci-après « l'Application ») et exclusivement pour une utilisation dans les conditions et la destination décrites ci-après sur un téléphone mobile disposant d'un système d'exploitation dont l'Utilisateur est le propriétaire unique (ci-après « Smartphone ») et à des fins personnelles, sans but lucratif direct ou indirect et dans le cadre d'une utilisation à titre privé.</p>
            <br>
            <h6>ARTICLE 1 - DÉFINITIONS</h6>
            <hr>
            <p>Les termes qui seront définis ci-dessous auront la signification suivante :</p>
            <br>
            <p>« CGU » : désigne les Conditions Générales d'Utilisation qui régissent l'utilisation de l'application mobile ACTIVITLY et du site internet www.activitly.com.</p>
            <p>« Application » : désigne le site web disponible à l'adresse « www.activitly.com » ou l'application mobile conçue, développée et exploitée par ACTIVITLY qui est téléchargeable sur la plateforme de téléchargement d'applications pour Smartphone : « App Store » ou « Google Play ».</p>
            <p>« Site » : désigne le site Internet conçu, développé et exploité par ACTIVITLY et dont l'adresse url est la suivante : www.activitly.com.</p>
            <p>« Utilisateur » : désigne toute personne utilisant le ACTIVITLY via l'application mobile ou via le site internet.</p>
            <p>« ACTIVITLY » : désigne l'ensemble des fonctionnalités de l'Application mise à la disposition des utilisateurs.</p>
            <p>« Compte utilisateur » désigne le compte créé par l'utilisateur à la 1ère ouverture de l'application, et nécessaire pour pouvoir activer le service.</p>
            <p>Le « Contenu » désigne les données mises à disposition par ACTIVITLY via l’application.</p>
            <br>
            <h6>ARTICLE 2 - OBJET</h6>
            <hr>
            <p>Les présentes constituent, au 21/07/2015 les Conditions Générales d’Utilisation de l'Application ACTIVITLY.</p>
            <p>Elles s'appliquent à l'exclusion de toutes autres conditions.</p>
            <p>Elles peuvent toutefois faire l'objet de modifications.</p>
            <p>Les conditions applicables sont alors celles en vigueur sur l’Application à la date d’inscription puis lors de son Utilisation.</p>
            <p>En conséquence, l’Utilisateur est invité à venir consulter l’Application régulièrement afin de se tenir informé des évolutions les plus récentes.</p>
            <br>
            <h6>ARTICLE 3 - ACCÈS À ACTIVITLY</h6>
            <hr>
            <p>Pour utiliser ACTIVITLY dans son exhaustivité, l’Utilisateur doit procéder à son inscription complète via son compte Facebook, Twitter ou Google Plus mais peut toute fois être créer sans l'implication de l'un des réseaux sociaux cités. L’Utilisateur sera seul responsable des conséquences de l'utilisation du Contenu et ce jusqu'à la désactivation de celui-ci de l’Utilisateur.</p>
            <br>
            <h6>ARTICLE 4 - MODIFICATION DE L'APPLICATION</h6>
            <hr>
            <p>ACTIVITLY se réserve le droit, à tout moment de modifier ou d'interrompre temporairement ou définitivement l'Application sans préavis.</p>
            <br>
            <h6>ARTICLE 5 - OBLIGATIONS DE L'UTILISATEUR</h6>
            <hr>
            <p>L'utilisateur doit être âgé au minimum de 16 ans et remplir l'ensemble des champs obligatoires figurant dans le formulaire d'inscription.</p>
            <p>L'utilisateur garantit que les données qu'il communique sont exactes et conformes à la réglementation en vigueur.</p>
            <p>Toute saisie d’information fausse, inexacte, non actualisée ou incomplète ou d’un nom d’Utilisateur ou pseudo contenant des termes, injurieux, diffamants, violents, obscènes ou plus généralement des termes inappropriés, pourra donner lieu à la fermeture du compte Utilisateur.</p>
            <p>Plus généralement ACTIVITLY se réserve la possibilité de suspendre ou de supprimer le compte d’un utilisateur malveillant ou en cas de violation des présentes conditions générales, d’utilisation ou de violation de dispositions légales ou réglementaires.</p>
            <p>Seront supprimés, en particulier, les comptes utilisateurs usurpant l'identité d'un tiers ou visant à poster des commentaires sous l'identité d'un tiers.</p>
            <p>Le manquement aux obligations ainsi définies constitue un manquement grave.</p>
            <p>Sans préjudice des dispositions de l'article "Résiliation", en cas de manquement par un utilisateur à une ou plusieurs de ces obligations ACTIVITLY pourra résilier le contrat et supprimera définitivement la possibilité d'ouvrir un ouvreau compte.</p>
            <br>
            <h6>ARTICLE 6 - CONDITIONS D'UTILISATION</h6>
            <hr>
            <p>L'Utilisateur déclare, garantit et s'engage à :
            <p>- Ne pas utiliser l'Application à des fins professionnelles, commerciales, lucratives (publicité, prospection, etc.) ou non privées.</p>
            <p>- Respecter les droits de propriété intellectuelle afférents aux contenus fournis par ACTIVITLY et par les autres utilisateurs.</p>
            <p>- Ne pas utiliser l'Application à des fins de racolage ou de prostitution.</p>
            <p>- Ne poster, n'indiquer ou ne diffuser sous quelque forme que ce soit que des informations ou contenus conformes à la réalité.</p>
            <p>- Ne pas tenir ou proférer des propos ou diffuser sous quelque forme que ce soit des contenus contrevenant aux droits d'autrui ou à caractère diffamatoire, injurieux, obscène, offensant, violent ou incitant à la violence, politique, raciste ou xénophobe et de manière générale tout contenu contraire à l'objet des Services, aux lois et règlements en vigueur, aux droits personnes ou aux bonnes moeurs.</p>
            <p>- Ne pas poster, indiquer, ni diffuser sous quelque forme que ce soit des informations ou contenus ayant pour effet de diminuer, de désorganiser, d'empêcher l'utilisation normale de ACTIVITLY d'interrompre et/ou de ralentir la circulation normale des communications entre les utilisateurs par l'intermédiaire de l'application, tels que des logiciels, virus, bombes logiques, envoi massif de messages, etc. ACTIVITLY se réserve le droit de supprimer les messages qui sont envoyés massivement par un utilisateur afin de préserver une qualité d'utilisation normale de l'application auprès des autres utilisateurs.</p>
            <p>- Etre seul responsable de l'utilisation des données fournies par l'application.
            <p>- N'utiliser l'Application qu'à des fins licites dans les conditions et limites du présent Contrat et sans porter atteinte aux droits de tiers ou de ACTIVITLY.</p>
            <p>- N'utiliser l'Application qu'à des fins privées.
            <p>- L'utilisateur reconnaît que les messages qu'il va adresser via l'application sont sous sa seule responsabilité.</p>
            <p>- ACTIVITLY ne saurait être tenu pour responsables de l'indemnisation des dommages directs et indirects subis par l'Utilisateur du fait notamment directement ou indirectement d'une mauvaise utilisation et/ou d'une utilisation non-conforme de l'Application.</p>
            <p>- ACTIVITLY ne saurait être tenu pour responsables en cas d'incident lors d'une "activité" ou du comportement inaproprié de tiers personnes.
            <p>Par ailleurs, ACTIVITLY ne pourra voir sa responsabilité engagée si, pour une raison échappant à son contrôle, l'Application a été partiellement ou totalement inaccessible, indisponible, interrompue ou de mauvaise qualité, notamment ACTIVITLY ne saurait être tenu responsable de tout acte ou omission d'un tiers intervenant à quelque titre que ce soit dans la fourniture de l'Application (opérateur de téléphonie mobile par exemple).! L’Utilisateur reconnait que ACTIVITLY n’a aucune obligation à fournir des prestations de support et de mise à jour de l’Application.</p>
            <br>
            <h6>ARTICLE 7 - VIE PRIVÉE</h6>
            <hr>
            <p>ACTIVITLY s'engage à ce que la collecte et le traitement de données à caractère directement ou indirectement personnel, effectuées à partir de l'Application, soient effectués conformément à la loi française dite "Informatique et Libertés" n°78-17 du 6 janvier 1978.</p>
            <p>L'Utilisateur dispose d'un droit d'opposition au traitement par ACTIVITLY de ses données personnelles, d'un droit d'accès, de modification, de rectification et de suppression des données personnelles le concernant.</p>
            <p>L'utilisateur s'engage à respecter toutes les lois relatives à la protection des données personnelles et au respect de la vie privée lors de l' utilisation de l’application.</p>
            <p>L’utilisateur qui créer un compte via l’application ACTIVITLY, après avoir passer l'étape de validation du compte, autorise l’éditeur à regrouper les informations récupérées sur son compte Facebook, ainsi que les informations données dans le questionnaire au lancement de l’application, dans une base de donnée qui pourrait ensuite être utilisées à des fins commerciales.</p>
            <p>L’utilisateur pourra à tout moment demander à l’éditeur, d’effacer les informations le concernant, de lui transmettre ou de ne pas les utiliser à des fins commerciales, en en faisant la demande via la rubrique « contact » de l’application ou par tout autre moyen.</p>
            <br>
            <h6>ARTICLE 8 - RESPONSABILITÉS</h6>
            <hr>
            <p>L'utilisateur reconnaît avoir connaissance des contraintes et limites du réseau Internet et Internet mobile. Dans ces conditions :</p>
            <p>- Il incombe à l'utilisateur, comme à tout internaute ou possesseur de téléphone mobile, de protéger ses équipements techniques notamment contre toute forme de contamination par des virus et/ou de tentative d'intrusion, ACTIVITLY ne pouvant en aucun cas en être tenu pour responsable.</p>
            <p>- L'Utilisateur est seul responsable de l'installation, de l'exploitation et de la maintenance de ses équipements techniques nécessaires pour utiliser l'Application. En aucun cas ACTIVITLY ne saurait être tenu responsable si l'application s'avère incompatible ou présente des dysfonctionnements avec certains équipements de l'utilisateur.</p>
            <p>- L'Utilisateur est seul responsable de l'utilisation qu'il fait de l'application et ne saurait tenir responsable ACTIVITLY pour toute réclamation et/ou procédure faite à son encontre. Il s'engage à faire son affaire personnelle de toute réclamation et/ou procédure formée contre ACTIVITLY et qui se rattacherait à son utilisation personnelle.</p>
            <p>- Enfin, la responsabilité de ACTIVITLY ne pourra être engagée en cas de non respect de la législation d'un pays étranger.</p>
            <p>Des liens peuvent renvoyer vers d'autres applications, sites Internet ou autres réseaux sociaux. La responsabilité de ACTIVITLY ne saurait être engagée dans le cas où le contenu desdits autres applications, sites Internet ou réseaux sociaux contreviendrait aux droits de tiers et plus généralement aux dispositions légales ou réglementaires en vigueur.</p>
            <br>
            <h6>ARTICLE 9 - MODIFICATION DE L'APPLICATION</h6>
            <hr>
            <p>ACTIVITLY se réserve le droit d'apporter à son Application toutes les modifications et améliorations qu'elle jugera nécessaires ou utiles et ne sera pas responsable des dommages de toute nature pouvant survenir de ce fait.</p>
            <br>
            <h6>ARTICLE 10 - DISPONIBILITÉ DU SERVICE</h6>
            <hr>
            <p>L'application est accessible par l'Utilisateur 24 heures sur 24, 7 jours sur 7 et toute l'année. ACTIVITLY se réserve néanmoins le droit, sans préavis ni indemnité, d'en fermer temporairement ou définitivement l'accès et ne sera pas responsable des dommages de toute nature pouvant survenir de ce fait.</p>
            <br>
            <h6>ARTICLE 11 - RESTRICTIONS D'UTILISATION</h6>
            <hr>
            <p>L'application ACTIVITLY est réservée aux personnes majeures ou aux personnes mineures titulaires d'une autorisation parentale leur permettant d'utiliser l'application.</p>
            <p>L'application est réservée aux personnes ayant souscrit à toutes les conditions d'utilisation de leur téléphone mobile, de leur opérateur mobile et de la place de marché associé diffusant l'Application.</p>
            <br>
            <h6>ARTICLE 12 - ACCÈS AU SERVICE</h6>
            <hr>
            <p>L'Utilisateur sera seul responsable des conséquences de l'utilisation de l'application et ce jusqu'à la désactivation de celui-ci de l'Utilisateur.</p>
            <br>
            <h6>ARTICLE 13 - TARIF</h6>
            <hr>
            <p>L’application est gratuite au téléchargement et entièrement fonctionnelle mais ACTIVITLY se reserve le droit de changer ses tarifs à tout moment.</p>
            <p>Lorsqu'il est payant, le prix du service est affiché sur l'Apple Store.</p>
            <p>Les éventuels frais de communication liés à l'utilisation de l'application proposés par ACTIVITLY demeurent à la charge de l’utilisateur.</p>
            <br>
            <h6>ARTICLE 14 - DROIT DE RÉTRACTATION</h6>
            <hr>
            <p>Conformément à l'article L 121-20 du Code de la Consommation français, l’utilisateur est informé qu'il dispose d'un délai de sept jours francs à compter de la souscription aux Services pour exercer son droit de rétractation, sans pénalité et sans motif.</p>
            <p>Toutefois, conformément à l'article L 121-20-2, 1° du Code de la Consommation français, le droit de rétractation ne saurait être exercé dès lors que l’utilisateur a accédé à son initiative aux Services concernés ou a commencé à les utiliser avant l'expiration du délai de rétractation visé à l'article L 121-20 susvisé.</p>
            <br>
            <h6>ARTICLE 15 - MODALITÉS DE RENOUVELLEMENT D'UN ABONNEMENT</h6>
            <hr>
            <p>L’utilisateur peut à tout moment notifier à l’éditeur son souhait de résilier son Abonnement sans frais autres que ceux éventuellement liés à la transmission de sa demande. La résiliation prendra effet à la date d'échéance de l'Abonnement en cours, à la condition que la notification soit faite par l’utilisateur au plus tard 48 h avant la date d'échéance de l'abonnement en cours, conformément aux dispositions de l'ARTICLE 16 ci-dessous "Résiliation".</p>
            <p>A l'expiration d'un Abonnement, celui-ci sera, sauf résiliation notifiée par l’utilisateur à l’éditeur avant l'échéance de l'Abonnement en cours dans les conditions indiquées, renouvelé par périodes équivalentes à celle initialement choisie par l’utilisateur. L'abonnement prorogé sera facturé à l’utilisateur sur la base tarifaire et la périodicité de l'abonnement initialement souscrits par l’utilisateur. l’utilisateur pourra résilier à tout moment l'abonnement prorogé pour une durée indéterminée, qui prendra alors fin à l'issue de la période d'abonnement en cours à la condition que la notification soit faite par l’utilisateur à l’éditeur au plus tard 48 h avant la date d'échéance de l'Abonnement en cours.</p>
            <p>Conformément à l'article L 136-1 du Code de Consommation français, l’éditeur informera l’utilisateur concerné en lui indiquant, par courrier électronique, envoyé au plus tard trente (30) jours avant le terme extinctif de l'Abonnement initial, qu'il peut résilier gratuitement son Abonnement avant la date d'échéance de celui-ci en le notifiant à l’éditeur, faute de quoi, son Abonnement sera prorogé par périodes équivalentes à celle initialement choisie par l’utilisateur jusqu'à ce que l’utilisateur notifie à l’éditeur son souhait de résilier son Abonnement en cours dans les conditions décrites à l'article 16 "Résiliation" ci-dessous.</p>
            <br>
            <h6>ARTICLE 16 - RÉSILIATION</h6>
            <hr>
            <p>Chaque utilisateur peut mettre fin à son inscription aux Services en demandant la clôture de son compte à tout moment, auprès de l’éditeur, sans frais autres que ceux liés à la transmission de sa demande et sans motif, notamment via la rubrique "Contact" de l’application. Cette demande sera réputée effectuée le jour ouvré suivant la réception par l’éditeur de la demande de clôture du compte concerné. Cette demande n'emporte pas le remboursement à l’utilisateur de la période restant à courir jusqu'à l'échéance de l'Abonnement l’utilisateur.</p>
            <p>La résiliation d'un Abonnement décidée par l’utilisateur prend effet à la date d'expiration de l'Abonnement en cours à la condition que la notification soit faite par l’utilisateur à l’éditeur au plus tard 48 h avant la date d'échéance de l'abonnement en cours.</p>
            <p>Sans préjudice des autres dispositions des Conditions d'Utilisation, en cas de manquement grave l’utilisateur, l’éditeur résilie le compte l’utilisateur sans préavis ni mise en demeure. Cette résiliation produit les mêmes effets que celle décidée par l’utilisateur.</p>
            <p>Sans préjudice des autres dispositions des Conditions d'Utilisation, en cas de manquement de l’utilisateur, l’éditeur résilie le compte de l’utilisateur 7 (sept) jours après l'envoi à l’utilisateur d'un courrier électronique lui demandant de se conformer aux Conditions d'Utilisation resté infructueux. Cette résiliation interviendra sans préjudice de tous les dommages et intérêts qui pourraient être réclamés par l’éditeur à l’utilisateur ou ses ayants droit et représentants légaux en réparation des préjudices subis par l’éditeur ou ces derniers du fait de tels manquements.</p>
            <p>L’utilisateur sera informé par courrier électronique de la résiliation ou de la confirmation de la résiliation de son compte. Les données relatives à l’utilisateur seront détruites à sa demande ou à l'expiration des délais légaux courant à compter de la résiliation du compte de l’utilisateur.</p>
            <br>
            <h6>ARTICLE 17 - PROPRIÉTÉ INTELLECTUELLE</h6>
            <hr>
            <p>17.1 Contenus diffusés par l’éditeur,
            <p>Les marques (notamment Activitly), les logos, les graphismes, les photographies, les animations, les vidéos et les textes contenus sur les Sites et dans les Services sont la propriété de l’éditeur, et ne peuvent être reproduits, utilisés ou représentés sans l'autorisation expresse de l’éditeur ou de ses partenaires, sous peine de poursuites judiciaires.</p>
            <p>Les droits d'utilisation concédés par l’éditeur à l’utilisateur sont réservés à un usage privé et personnel dans le cadre et pour la durée de l'inscription aux Services. Toute autre utilisation par l’utilisateur est interdite sans l'autorisation de l’éditeur.</p>
            <p>L’utilisateur s'interdit notamment de modifier, copier, reproduire, télécharger, diffuser, transmettre, exploiter commercialement et/ou distribuer de quelque façon que ce soit les Services, les pages des Sites, ou les codes informatiques des éléments composant les Services et les Sites.</p>
            <br>
            <p>17.2 Contenus diffusés par les utilisateurs,</p>
            <p>L’utilisateur concède à l’éditeur une licence d'utilisation des droits de propriété intellectuelle attachés aux contenus fournis par l’utilisateur dans le cadre de leur utilisation des Services. Cette licence comprend notamment le droit pour l’éditeur de reproduire, représenter, adapter, traduire, numériser, utiliser aux fins des Services ou de sous- licencier les contenus concernant l’utilisateur (informations, images, description, critères de recherche, etc.), sur tout ou partie des Services (sur les Sites, par e-mail) et/ou dans les mailings de l’éditeur et de manière générale sur tous supports de communication électronique (e-mail, SMS, MMS, WAP, Internet) dans le cadre des Services.</p>
            <p>L’utilisateur autorise expressément l’éditeur à modifier lesdits contenus afin de respecter la charte graphique des Services ou des autres supports de communication visés ci-dessus et/ou de les rendre compatibles avec ses performances techniques ou les formats des supports concernés. Ces droits sont concédés pour le monde entier et pour la durée d'exécution des Conditions d'Utilisation entre l’utilisateur et l’éditeur. L’utilisateur s'interdit de copier, reproduire, ou autrement utiliser les contenus relatifs aux autres utilisateurs autrement que pour les stricts besoins d'utilisation des Services à des fins personnelles et privées.</p>
            <br>
            <h6>ARTICLE 18 - DROIT APPLICABLE - ATTRIBUTION DE JURIDICTION</h6>
            <hr>
            <p>Ces Conditions d'Utilisation sont régies, interprétées et appliquées conformément au droit français, la langue d'interprétation étant la langue française en cas de contestation sur la signification d'un terme ou d'une disposition des Conditions d'Utilisation.</p>
            <br>
            <h6>ARTICLE 19 - LOCALISATION</h6>
            <hr>
            <p>L’utilisateur affirme qu’il n’est pas localisé dans un pays :</p>
            <p>- sujet à un embargo des Etats-Unis ;</p>
            <p>- désigné comme un pays soutenant le terrorisme.</p>
            <br>
            <p>Le présent Contrat est soumis à la loi française.</p>
        </div>
        <div class="space-50"></div>

    </div>
@endsection
