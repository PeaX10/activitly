@extends('default')

@section('title')
    Contactez-nous
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="container">
        <div id="legend">
            <h2>Contactez-nous</h2>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if(Auth::check())
                        {!! BootForm::open() !!}
                        <div class="media media-post">
                            <a class="pull-left author" href="#">
                                <div class="avatar">
                                    <img class="media-object" alt="64x64" src="@if(!empty(Auth::user()->avatar)){{ asset('img/avatar/'.Auth::user()->avatar.'.png') }} @else {{ asset('img/register/unknown.png') }} @endif">
                                </div>
                            </a>
                            <div class="media-body">
                                {!! BootForm::select(null, 'subject')->options(array(0 => 'Sujet',
                                     1 => 'Proposition d\'une nouvelle activité',
                                     2 => 'Problème technique',
                                     3 => 'Réclamation',
                                     4 => 'Partenariat',
                                     5 => 'Publicité',
                                     6 => 'Autres'
                                     ))->disabled(0)->class('selectpicker')->data_style('btn-info btn-fill btn-block')->data_menu_style('dropdown-blue') !!}
                                {!! BootForm::textarea(null, 'message')->placeholder('Votre message...')->rows(6)->style('margin-top:10px') !!}
                                <div class="media-footer">
                                    {!! BootForm::submit('Envoyer')->class('btn btn-info btn-fill pull-right') !!}
                                </div>
                            </div>
                        </div>
                        {!! BootForm::close() !!}
                    @else
                        <div class="media media-post">
                            {!! BootForm::open() !!}
                                <a class="pull-left author" href="#">
                                    <div class="avatar">
                                        <img class="media-object" alt="64x64" src="{{ asset('img/register/unknown.png') }}">
                                    </div>
                                </a>
                                <div class="media-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! BootForm::text(null, 'name')->placeholder('Nom et Prénom') !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! BootForm::email(null, 'email')->placeholder('Votre E-mail') !!}
                                        </div>
                                    </div>
                                    {!! BootForm::select(null, 'subject')->options(array(0 => 'Sujet',
                                     1 => 'Proposition d\'une nouvelle activité',
                                     2 => 'Problème technique',
                                     3 => 'Réclamation',
                                     4 => 'Partenariat',
                                     5 => 'Publicité',
                                     6 => 'Autres'
                                     ))->disabled(0)->class('selectpicker')->data_style('btn-info btn-fill btn-block')->data_menu_style('dropdown-blue') !!}
                                    {!! BootForm::textarea(null, 'message')->placeholder('Votre message...')->rows(6)->style('margin-top:10px') !!}
                                    <div class="media-footer">
                                        <h6>Se connecter avec</h6>
                                        <a href="{{ url('twitter') }}" class="btn btn-round btn-fill btn-social btn-twitter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a href="{{ url('facebook') }}" class="btn btn-round btn-fill btn-social btn-facebook">
                                            <i class="fa fa-facebook-square"></i>
                                        </a>
                                        <a href="{{ url('google') }}" class="btn btn-round btn-fill btn-social btn-google">
                                            <i class="fa fa-google-plus-square"></i>
                                        </a>
                                        {!! BootForm::submit('Envoyer')->class('btn btn-info btn-fill pull-right') !!}
                                    </div>
                                </div><!-- end media-body -->
                            {!! BootForm::close() !!}
                        </div> <!-- end media-post -->
                    </div>
                    @endif
            </div>
        </div>
    </div>
@endsection
