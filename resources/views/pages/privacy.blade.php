@extends('default')

@section('title')
    Mentions Légales
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="container">
        <div id="legend">
            <h2>Mentions légales</h2>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <h6>INFORMATIONS :</h6>
            <hr>
            <p>Le site internet activitly.com est réalisé et édité par Petit Alexandre.</p>
            <p>Email : contact@activitly.com</p>
            <br>
            <h6>CRÉATION :</h6>
            <hr>
            <p>Le site internet www.activitly.com et les applications iPhone et Android ont été créés par Petit Alexandre.</p>
            <p>Email : contact@activitly.com</p>
            <p>Téléphone : +33 6 11 81 58 11</p>
            <br>
            <h6>HÉBERGEMENT :</h6>
            <hr>
            <p>Le site internet www.activitly.com est hébergé chez la société LWS (Ligne Web Services).<br>S.A.R.L au capital de 1 000 000 Euros </p><br>
            <p>Adresse : S.A.R.L LWS 4, RUE GALVANI 75838 PARIS CEDEX 17 FRANCE </p>
            <p>Siret : 45045388100010</p>
            <p>TVA intra : FR44 450 453 881</p>
            <p>RCS Paris B 450 453 881 - APE 723Z</p>
            <p>Site internet : www.lws.fr</p>
            <br>

            <h6>PROPRIÉTÉ INTELLECTUELLE :</h6>
            <hr>
            <p>Activitly est une protégé par un copyright. Toute reproduction ou représentation totale ou partielle de cette marque, seule ou intégrée à d'autres éléments, sans l'autorisation expresse et préalable de Activitly est prohibée, et engagerait la responsabilité de l'utilisateur au sens des articles L 713-2 et L 713-3 du Code de la Propriété Intellectuelle.
            Les textes, photographies, graphisme, images et tous les éléments composant le site Web de Activitly, sont protégés. Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de : Activitly. Cela constituerait une contrefaçon sanctionnée par les articles L. 335-2 et suivants du Code de la propriété intellectuelle qui définit la contrefaçon comme un délit, et prévoit jusqu'à trois ans d'emprisonnement et 300 000 euros d'amende.</p>
            <br>
            <h6>DONNÉES PERSONNELLES :</h6>
            <br>
            <p>Conformément à la loi Informatique et Libertés en date du 6 janvier 1978, Les utilisateur du site activitly.com dispose d'un droit d'accès, de rectification, de modification et de suppression pour les données qui vous concernent. Vous pouvez exercer ce droit en envoyant un courrier à Activitly.
            Les informations du site activitly.com peuvent contenir des inexactitudes techniques ou des erreurs typographiques. Elles sont non contractuelles et sujettes à modification sans préavis.</p>
            <div class="space-50"></div>
        </div>

    </div>
@endsection
