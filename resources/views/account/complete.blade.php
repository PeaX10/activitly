@extends('default')

@section('title')
    Compléter mon profil
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/board.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/cropper.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="container">

        <div id="legend">
            <legend class="">Parlez-nous un peu de vous !</legend>
        </div>
        <form name="complete_profile" method="post" enctype="multipart/form-data" action>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="row">
            <div class="progress">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="5" style="width: 20%;">
                </div>

            </div>

            <div class="navbar" style="display: none;">
                <div class="navbar-inner">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#step1" data-toggle="tab" data-step="1">Étape 1</a></li>
                        <li><a href="#step2" data-toggle="tab" data-step="2">Étape 2</a></li>
                        <li><a href="#step3" data-toggle="tab" data-step="3">Étape 3</a></li>
                        <li><a href="#step4" data-toggle="tab" data-step="4">Étape 4</a></li>
                        <li><a href="#step5" data-toggle="tab" data-step="5">Étape 5</a></li>
                    </ul>
                </div>
            </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        @if (count($errors) == 1)
                            Une erreur est survenue : @foreach ($errors->all() as $error){{ $error }}@endforeach
                        @else
                            Des erreurs sont survenues :<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                @endif
            <div class="tab-content">
                <div class="tab-pane fade in active" id="step1">
                    <div class="well">
                        <h3>Dans quelle ville êtes-vous ?<small><b>   (obligatoire)</b></small><br><small>Sera utilisé quand nous ne pourrons pas vous localiser</small></h3>
                        <hr>
                        <br>
                        <div class="input-group">
                            <input id="cities" type="text" placeholder="Ex : Paris" class="form-control location" name="city" value="@if(!empty(old('city'))){{ old('city') }}@elseif(!empty($user->location)){{ $user->location }}@endif" />
                            <span class="input-group-addon"><i class="fa fa-location-arrow"></i></span>
                        </div>
                        {!! $errors->first('city', '<div class="has-error"><p class="help-block">:message</p></div>') !!}

                    </div>

                    <a class="btn btn-info btn-round btn-fill next" href="#">Suivant</a>
                </div>
                <div class="tab-pane fade" id="step2">
                    <div class="well">
                        <h3>Quelles sont vos activités favorites ?<small><b>   (minimum: 1)</b></small><br><small>Permets de déterminer quelles activités vous conviendront.</small></h3>
                        <hr>
                        <h5><small>Appuyer sur la touche Entrer pour valider une activité.</small></h5>
                        <div class="input-group" style="background: #FFF; border-radius:3px; border:1px solid #e3e3e3; padding-left:6px">
                            <input name="activities" class="tagsinput tag-azure tag-fill tag-round" value="@if(old('activities')){{ old('activities') }}@else Course à pied @endif" id="activities" style="min-width: 100px">
                            <span class="input-group-addon" style="border:none"><i class="fa fa-child"></i></span>
                        </div>
                        {!! $errors->first('activities', '<div class="has-error"><p class="help-block">:message</p></div>') !!}
                    </div>
                    <a class="btn btn-warning btn-round btn-fill prev" href="#">Précèdent</a>
                    <a class="btn btn-info btn-round btn-fill next" href="#">Suivant</a>
                </div>
                <div class="tab-pane fade" id="step3">
                    <div class="well">
                        <h3>Réseaux Sociaux<small><b>   (optionnel)</b></small><br><small>Permettra aux autres membres de vous retrouver sur les réseaux sociaux.</small></h3>
                        <hr>
                        <br>
                        <label for="facebook">Facebook</label>
                        <div class="input-group">
                            <input name="facebook" id="facebook" type="text" placeholder="Entrer le lien de votre page Facebook" class="form-control" value="@if(!empty( old('facebook') )){{ old('facebook') }}@elseif(!empty($user->facebook)){{ $user->facebook }}@endif">
                            <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                        </div>
                        {!! $errors->first('facebook', '<div class="has-error"><p class="help-block">:message</p></div>') !!}
                        <br>
                        <label for="twitter">Twitter</label>
                        <div class="input-group">
                            <input name="twitter" id="twitter" type="text" placeholder="Entrer le lien de votre page Twitter" class="form-control" value="@if(!empty( old('twitter') )){{ old('twitter') }}@elseif(!empty($user->twitter)){{ $user->twitter }}@endif">
                            <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                        </div>
                        {!! $errors->first('twitter', '<div class="has-error"><p class="help-block">:message</p></div>') !!}
                        <br>
                        <label for="google-plus">Google+</label>
                        <div class="input-group">
                            <input name="google" id="google-plus" type="text" placeholder="Entrer le lien de votre page Google+" class="form-control" value="@if(!empty( old('google') )){{ old('google') }}@elseif(!empty($user->google)){{ $user->google }}@endif">
                            <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                        </div>
                        {!! $errors->first('google', '<div class="has-error"><p class="help-block">:message</p></div>') !!}
                    </div>
                    <a class="btn btn-warning btn-round btn-fill prev" href="#">Précèdent</a>
                    <a class="btn btn-info btn-round btn-fill next" href="#">Suivant</a>
                </div>
                <div class="tab-pane fade" id="step4">
                    <div class="well">
                        <h3>Personnaliser votre profil<small><b>   (optionnel)</b></small><br><small>Changer votre photo de profil et/ou votre bannière</small></h3>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="card card-user" id="card-profile">
                                    <div class="image preview-lg">
                                        <img src="@if(!empty( $user->banner_online )){{ $user->banner_online }}@else {{ asset('img/register/ban.png') }} @endif" alt="..."/>
                                    </div>
                                    <div class="content">
                                        <div class="author" align="center">
                                            <a href="#">
                                                <div class="img-preview preview-lg avatar"><img src="@if(!empty( $user->avatar_online )){{ $user->avatar_online }}@else {{ asset('img/register/unknown.png') }} @endif" alt="..."/></div>
                                                <h4 class="title"><span id="lastname">{{ $user->lastname }}</span> <span id="firstname">{{ $user->firstname }}</span><br />
                                                    <small><span id="username">{{ $user->username }}</span></small>
                                                </h4>
                                            </a>
                                        </div>
                                        <p class="description text-center">
                                            <i class="fa fa-map-marker text-muted"></i> <span id="location"></span><br>
                                            <i class="fa fa-envelope-o text-muted"></i> <span id="email">{{ $user->email }}</span><br>
                                            <i class="fa fa-birthday-cake text-muted"></i> Anniversaire : <span id="birthday">{{ $user->birthday }}</span><br>
                                            <i class="fa fa-clock-o text-muted"></i> Inscription : {{ $user->created_date }}
                                        </p>
                                    </div>
                                    <hr>
                                    <div class="text-center">
                                        <button href="#" class="btn btn-social btn-simple"><i class="fa fa-facebook-square"></i></button>
                                        <button href="#" class="btn btn-social btn-simple"><i class="fa fa-twitter"></i></button>
                                        <button href="#" class="btn btn-social btn-simple"><i class="fa fa-google-plus-square"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="card card-user" style="padding:10px;" id="form-profile">
                                    <h4>Photos</h4>
                                    <hr>
                                    <br>
                                    <div class="form-group {!! $errors->first('profile_img', ' has-error') !!}" style="min-height: 30px">
                                        <label class="col-sm-6 col-lg-4 control-label" for="profile_img">
                                            Photo de Profil
                                        </label>
                                        <div class="col-sm-6 col-lg-8">
                                            <input type="hidden" name="profile_params" id="profile_params" />
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-fill btn-info btn-file">
                                                        Choissir&hellip; <input type="file" name="profile_img" id="profile_img" @if(!empty( $user->avatar_online )) value="{{ $user->avatar_online }}" @else {{ old('profile_img') }} @endif>
                                                    </span>
                                                </span>
                                                <input type="text" class="form-control" readonly>
                                            </div>
                                            {!! $errors->first('profile_img', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group {!! $errors->first('ban_img', ' has-error') !!}" style="min-height: 30px">
                                        <label class="col-sm-6 col-lg-4 control-label" for="ban_img">
                                            Bannière
                                        </label>
                                        <div class="col-sm-6 col-lg-8">
                                            <input type="hidden" name="ban_params" id="ban_params" />
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-fill btn-info btn-file">
                                                        Choissir&hellip; <input type="file" name="ban_img" id="ban_img" value="{{ old('ban_img') }}">
                                                    </span>
                                                </span>
                                                <input type="text" class="form-control" readonly>
                                            </div>
                                            {!! $errors->first('ban_img', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="btn btn-warning btn-round btn-fill prev" href="#">Précèdent</a>
                    <a class="btn btn-info btn-round btn-fill next" href="#">Suivant</a>
                </div>
                <div class="tab-pane fade" id="step5">
                    <div class="well">
                        <h3>Vérification</h3>
                        <hr>
                        <p>En cliquant sur le bouton "C'est parti !", vous confirmez que toutes les informations que vous nous avez fournies sont corrects.</p>
                        <p>Vous acceptez également nos <a href="{{ url('/terms') }}" target="_blank">Conditions Générales d'utilisation</a>.</p>
                    </div>
                    <a class="btn btn-warning btn-round btn-fill prev" href="#">Précèdent</a>
                    <button class="btn btn-success btn-round btn-fill first" href="#">C'est parti !</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="space-50"></div>
    <div class="modal fade" id="modalProfilePhoto" tabindex="-1" role="dialog" aria-labelledby="modalProfilePhotoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modalProfilePhotoLabel">Recadrer ma photo de profil</h4>
                </div>
                <div class="modal-body">
                    <div id="cropper-profile">
                        <img src="" alt="" style="max-height: 400px; max-width: 400px">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-info btn-simple">Valider</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalBanPhoto" tabindex="-1" role="dialog" aria-labelledby="modalBanPhotoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modalBanPhotoLabel">Recadrer ma bannière</h4>
                </div>
                <div class="modal-body">
                    <div id="cropper-ban">
                        <img src="" alt="" style="max-height: 400px; max-width: 400px">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-info btn-simple">Valider</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/complete_profile.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
            if(input.attr('id') == 'profile_img'){
                if(input.val() != ''){
                    $('#modalProfilePhoto').modal('show');
                }else{
                    $('#cropper-profile img').attr('src', '{{ asset('img/register/unknown.png') }}');
                    $image.cropper('destroy');
                }
            }else{
                if(input.val() != ''){
                    $('#modalBanPhoto').modal('show');
                }else{
                    $('#cropper-ban img').attr('src', '{{ asset('img/register/ban.png') }}');
                    $image2.cropper('destroy');
                }
            }
        });
    </script>
@endsection