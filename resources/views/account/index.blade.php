@extends('default')

@section('title')
    Mon Compte
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/board.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/cropper.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="container">

        <div id="legend">
            <h2>Mon Compte</h2>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="nav nav-stacked nav-icons" role="tablist">
                            <li class="active">
                                <a href="#stacked-description-logo" role="tab" data-toggle="tab" aria-expanded="true">
                                    <i class="fa fa-info-circle"></i><br>
                                    Informations
                                </a>
                            </li>
                            <li class="">
                                <a href="#stacked-legal-logo" role="tab" data-toggle="tab" aria-expanded="false">
                                    <i class="fa fa-user"></i><br>
                                    Profil
                                </a>
                            </li>
                            <li class="">
                                <a href="#stacked-help-logo" role="tab" data-toggle="tab" aria-expanded="false">
                                    <i class="fa fa-cog"></i><br>
                                    Paramètres
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="stacked-description-logo">
                                <?php  $columnSizes = [
                                        'sm' => [5, 7],
                                        'lg' => [3, 9],
                                        'files' => true

                                ]; ?>
                                {!! BootForm::openHorizontal($columnSizes)->action(url('account/edit/infos')) !!}
                                {!! BootForm::text('Pseudo', 'username')->value(old('username') ?: $user->username) !!}
                                {!! BootForm::text('Prénom', 'firstname')->value(old('firstname') ?: $user->firstname) !!}
                                {!! BootForm::text('Nom', 'lastname')->value(old('lastname') ?: $user->lastname) !!}
                                <?php if(is_numeric(old('gender'))) $user->gender = old('gender'); ?>
                                {!! BootForm::select('Sexe', 'gender')->options(['0' => 'Femme', '1' => 'Homme'])->class('selectpicker')->data_style('form-control')->data_menu_style('')->select($user->gender) !!}
                                {!! BootForm::text('Date de naissance', 'birthday')->value(old('birthday') ?: $user->birthday) !!}
                                {!! BootForm::email('E-mail', 'email')->value(old('email') ?: $user->email) !!}
                                {!! BootForm::text('Ville', 'city')->placeholder('Votre ville')->value(old('city') ?: $user->location)->id("cities") !!}
                                <hr>
                                {!! BootForm::password('Nouveau Mot de passe', 'new_password') !!}
                                {!! BootForm::password('Confirmer nouveau Mot de passe', 'new_password_confirm') !!}
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-6 col-lg-4"></div><div class="col-sm-6 col-lg-8"><label>Merci d'entrer votre mot de passe pour valider les modifications</label></div>
                                </div>

                                {!! BootForm::password('Mot de passe', 'password_infos') !!}
                                {!! BootForm::submit('Modifier')->class('btn-round btn-fill btn-primary') !!}
                                {!! BootForm::close() !!}

                            </div>
                            <div class="tab-pane" id="stacked-legal-logo">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="card card-user" id="card-profile">
                                            <div class="image preview-lg">
                                                <img src="@if(!empty($user->banner)){{ asset('img/banner/'.$user->banner.'.png') }}@else{{ asset('img/register/ban.png') }}@endif" alt="..."/>
                                            </div>
                                            <div class="content">
                                                <div class="author">
                                                    <a href="#">
                                                        <div class="img-preview preview-lg avatar"><img src="@if(!empty($user->avatar)){{ asset('img/avatar/'.$user->avatar.'.png') }}@else{{ asset('img/register/unknown.png') }}@endif" alt="..."/></div>
                                                        <h4 class="title"><span id="lastname">{{ $user->lastname }}</span> <span id="firstname">{{ $user->firstname }}</span><br />
                                                            <small><span id="username">{{ $user->username }}</span></small>
                                                        </h4>
                                                    </a>
                                                </div>
                                                <p class="description text-center">
                                                    <i class="fa fa-map-marker text-muted"></i> <span id="location">{{ $user->location }}</span><br>
                                                    <i class="fa fa-envelope-o text-muted"></i> <span id="email">{{ $user->email }}</span><br>
                                                    <i class="fa fa-birthday-cake text-muted"></i> Anniversaire : <span id="birthday">{{ $user->birthday }}</span><br>
                                                    <i class="fa fa-clock-o text-muted"></i> Inscription : {{ $user->created_date }}
                                                </p>
                                            </div>
                                            <hr>
                                            <div class="text-center">
                                                <button href="#" class="btn btn-social btn-simple"><i class="fa fa-facebook-square"></i></button>
                                                <button href="#" class="btn btn-social btn-simple"><i class="fa fa-twitter"></i></button>
                                                <button href="#" class="btn btn-social btn-simple"><i class="fa fa-google-plus-square"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card card-user" style="padding:10px; overflow: auto" id="form-profile">
                                            {!! BootForm::openHorizontal($columnSizes)->action(url('account/edit/profile'))->enctype('multipart/form-data') !!}
                                            <h4>Activités</h4>
                                            <hr>
                                            <div class="input-group" style="background: #FFF; border-radius:3px; border:1px solid #e3e3e3; padding-left:6px">
                                                <input name="activities" class="tagsinput tag-azure tag-fill tag-round" value="{{ old('activities') ?: $user->activities }}" id="activities" style="min-width: 100px">
                                                <span class="input-group-addon" style="border:none"><i class="fa fa-child"></i></span>
                                            </div>
                                            {!! $errors->first('activities', '<div class="has-error"><p class="help-block">:message</p></div>') !!}
                                            <h4>Photos</h4>
                                            <hr>
                                            <div class="form-group {!! $errors->first('profile_img', ' has-error') !!}">
                                                <label class="col-sm-6 col-lg-4 control-label" for="profile_img">
                                                    Photo de Profil
                                                </label>
                                                <div class="col-sm-6 col-lg-8">
                                                    <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <span class="btn btn-fill btn-info btn-file">
                                                            <input type="hidden" name="profile_params" id="profile_params" />
                                                            Choissir&hellip; <input type="file" name="profile_img" id="profile_img">
                                                        </span>
                                                    </span>
                                                    <input type="text" class="form-control" readonly>
                                                    </div>
                                                    {!! $errors->first('profile_img', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group {!! $errors->first('ban_img', ' has-error') !!}">
                                                <label class="col-sm-6 col-lg-4 control-label" for="ban_img">
                                                    Bannière
                                                </label>
                                                <div class="col-sm-6 col-lg-8">
                                                    <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <span class="btn btn-fill btn-info btn-file">
                                                            <input type="hidden" name="ban_params" id="ban_params" />
                                                            Choissir&hellip; <input type="file" name="ban_img" id="ban_img">
                                                        </span>
                                                    </span>
                                                        <input type="text" class="form-control" readonly>
                                                    </div>
                                                    {!! $errors->first('ban_img', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <h4>Réseaux Sociaux</h4>
                                            <hr>
                                            {!! BootForm::text('Facebook', 'facebook')->placeholder('Url page Facebook')->value(old('facebook') ?: $user->facebook) !!}
                                            {!! BootForm::text('Twitter', 'twitter')->placeholder('Url page Twitter')->value(old('twitter') ?: $user->twitter) !!}
                                            {!! BootForm::text('Google+', 'googleplus')->placeholder('Url page Google+')->value(old('google') ?: $user->google) !!}
                                            <hr>
                                            <div class="form-group">
                                                <div class="col-sm-2 col-lg-1"></div><div class="col-sm-10 col-lg-11"><label>Merci d'entrer votre mot de passe pour valider les modifications</label></div>
                                            </div>
                                            {!! BootForm::password('Mot de passe', 'password_profile') !!}
                                            {!! BootForm::submit('Modifier')->class('btn-round btn-fill btn-primary') !!}
                                            {!! BootForm::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="stacked-help-logo">
                                <p>Paramètres non disponibles pour le moment.</p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </div>
        <div class="space-50"></div>
    </div>
    <div class="modal fade" id="modalProfilePhoto" tabindex="-1" role="dialog" aria-labelledby="modalProfilePhotoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modalProfilePhotoLabel">Recadrer ma photo de profil</h4>
                </div>
                <div class="modal-body">
                    <div id="cropper-profile">
                        <img src="" alt="" style="max-height: 400px; max-width: 400px">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-info btn-simple">Valider</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalBanPhoto" tabindex="-1" role="dialog" aria-labelledby="modalBanPhotoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modalBanPhotoLabel">Recadrer ma bannière</h4>
                </div>
                <div class="modal-body">
                    <div id="cropper-ban">
                        <img src="" alt="" style="max-height: 400px; max-width: 400px">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-info btn-simple">Valider</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/account.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
            if(input.attr('id') == 'profile_img'){
                if(input.val() != ''){
                    $('#modalProfilePhoto').modal('show');
                }else{
                    $('#cropper-profile img').attr('src', '{{ asset('img/register/unknown.png') }}');
                    $image.cropper('destroy');
                }
            }else{
                if(input.val() != ''){
                    $('#modalBanPhoto').modal('show');
                }else{
                    $('#cropper-ban img').attr('src', '{{ asset('img/register/ban.png') }}');
                    $image2.cropper('destroy');
                }
            }
        });
        $('.typeahead').css('display', 'none');

        $(document).ready(function() {
            $('.typeahead').css('display', 'none');
            @if(!empty(Session::get('open_profile')))
                $('.nav.nav-stacked.nav-icons li:eq(0)').removeClass('active').attr('aria-expanded', false);
                $("#stacked-description-logo").removeClass('active');
                $('.nav.nav-stacked.nav-icons li:eq(1)').addClass('active').attr('aria-expanded', true);
                $("#stacked-legal-logo").addClass('active');
            @endif
        });

    </script>
@endsection