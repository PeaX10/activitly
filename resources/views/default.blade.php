<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Activitly - @yield('title')</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport' />
    <link href="{{ asset("css/bootstrap.css") }}" rel="stylesheet" />
    <link href="{{ asset("css/gsdk.css") }}" rel="stylesheet" />
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/home.css") }}" rel="stylesheet"/>
    @if(Auth::check())<link href="{{ asset("css/board.css") }}" rel="stylesheet"/>@endif
    @yield('css')

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset("css/pe-icons.css") }}" rel="stylesheet" />

</head>

<body class="home">

@if(Auth::check())
    @include('menu/connected')
@else
    @include('menu/default')
@endif

@yield('content')


@include('home.footer')

</div>

@include('home.modal')

</body>
<!-- Javascript -->
<script src="{{ asset('js/jquery.js') }}" type="text/javascript"></script>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&language=fr&region=FR&key=AIzaSyBJGuNrzWw_uVSu6MIjbw9lEfz2bdAdiqc"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/gsdk.js') }}"></script>
<script src="{{ asset('js/chartist.min.js') }}"></script>
<script src="{{ asset('js/home.js') }}" type="text/javascript"></script>
@yield('js')
</html>