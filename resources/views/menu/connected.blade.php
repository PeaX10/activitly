<nav class="navbar navbar-inverse navbar-icons navbar-fixed-top" role="navigation">
    @include('home.alerts')
    <div class="container">
        <div class="navbar-header">
            <button id="menu-toggle" type="button" class="navbar-toggle">
                <span class="sr-only">Ouvrir / Fermer</span>
                <span class="icon-bar bar1"></span>
                <span class="icon-bar bar2"></span>
                <span class="icon-bar bar3"></span>
            </button>
            <a class="navbar-brand logo" href="{{ url('board') }}"><i class="logo"></i></a>
            <a class="navbar-brand" href="{{ url('board') }}">Activitly</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ url('board') }}">
                        <i class="fa fa-home"></i>
                        <p>Accueil</p>
                    </a>
                </li>
                <li class="dropdown">
                    <?php
                        $nbr_invitations = 0;
                        $nbr_next_activities = DB::table('participations')
                                ->join('events', 'participations.event', '=', 'events.id')
                                ->where('participations.status', 1)
                                ->where('participations.user', $user->id)
                                ->count();
                        $nbr_total_activities = $nbr_invitations + $nbr_next_activities;
                    ?>
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-location-arrow"></i>
                        <p>Activités @if($menu['activities_next'] + $menu['activities_invites'] > 0)<span class="label label-warning label-fill">{{ $menu['activities_next'] + $menu['activities_invites'] }}</span>@endif
                            <span class="caret"></span>
                        </p>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('activity/create') }}"><i class="fa fa-plus-square-o"></i> Créer une activité</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('activity') }}"><i class="fa fa-hand-o-up"></i> Mes Activités</a></li>
                        <li><a href="{{ url('activity/next') }}"><i class="fa fa-arrow-circle-o-up"></i> Activités à venir @if($menu['activities_next'] > 0)<span class="label label-warning label-fill">{{ $menu['activities_next'] }}</span>@endif</a></li>
                        <li><a href="{{ url('activity/prev') }}"><i class="fa fa-arrow-circle-o-down"></i> Activités passés</a></li>
                        <li><a href="{{ url('activity/invites') }}"><i class="fa fa-envelope-o"></i> Invitations @if($menu['activities_invites'] > 0)<span class="label label-warning label-fill">{{ $menu['activities_invites'] }}</span>@endif</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ url('messages') }}">
                        <i class="fa fa-comments"></i>
                        <p>Messages @if($menu['messages'] > 0)<span class="label label-danger label-fill">{{ $menu['messages'] }}</span>@endif</p>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell-o"></i>
                        <p>Notifications @if($menu['notifications'] > 0)<span class="label label-danger label-fill">{{ $menu['notifications'] }}</span>@endif
                            <span class="caret"></span>
                        </p>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a>Aucune Notification</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('board/notifications') }}">Voir toutes les notifications</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <div class="user-photo">
                            <img class="avatar" src="@if(!empty($user->avatar)){{ url('img/avatar/'.$user->avatar.'.png') }}@else {{ url('img/register/unknown.png') }}@endif" />
                        </div>

                        <p>{{ $user->username }} @if($menu['profile_friends'] > 0)<span class="label label-danger label-fill">{{ $menu['profile_friends'] }}</span>@endif
                            <span class="caret"></span>
                        </p>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('profile/'.$user->username) }}"><i class="fa fa-user"></i> Mon Profil</a></li>
                        <li><a href="{{ url('friends') }}"><i class="fa fa-users"></i> Mes Amis @if($menu['profile_friends'] > 0)<span class="label label-danger label-fill">{{ $menu['profile_friends'] }}</span>@endif</a></li>
                        <li><a href="{{ url('account') }}"><i class="fa fa-cog"></i> Paramètres</a></li>
                        <li class="divider"></li>
                        <li><a class="text-danger" href="{{ url('auth/logout') }}"><i class="fa fa-sign-out"></i> Déconnexion</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
</nav>