<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    @include('home.alerts')
    <div class="container">
        <div class="navbar-header">
            <button id="menu-toggle" type="button" class="navbar-toggle">
                <span class="sr-only">Ouvrir / Fermer</span>
                <span class="icon-bar bar1"></span>
                <span class="icon-bar bar2"></span>
                <span class="icon-bar bar3"></span>
            </button>
            <a class="navbar-brand logo" href="{{ URL::route('home') }}"><i class="logo"></i></a>
            <a class="navbar-brand" href="{{ URL::route('home') }}">Activitly</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if(!Request::is('auth/login'))<li><a href="#" data-toggle="modal" data-target="#loginBox">Connexion</a></li>@endif
                @if(!Request::is('auth/register'))<li><a href="#" data-toggle="modal" data-target="#registerBox" class="btn btn-round btn-default">Préinscription</a></li>@endif
            </ul>

        </div>
    </div>
</nav>