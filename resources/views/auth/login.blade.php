@extends('default')

@section('title')
    Connexion
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="container">

        <div id="legend">
            <h2>Connexion<br>
                <small>Ou connexion avec
                    <a href="{!!URL::to('twitter')!!}" class="btn btn-round btn-fill btn-social btn-twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="{!!URL::to('facebook')!!}" class="btn btn-round btn-fill btn-social btn-facebook">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                    <a href="{!!URL::to('google')!!}" class="btn btn-round btn-fill btn-social btn-google">
                        <i class="fa fa-google-plus-square"></i>
                    </a>
                </small>
            </h2>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    {!! BootForm::open() !!}
                    {!! BootForm::text('Identifiant', 'username')->placeholder('Pseudo ou E-mail...') !!}
                    {!! BootForm::password('Mot de passe', 'password') !!}
                    {!! BootForm::checkbox('Se souvenir de moi', 'remember')->data_toggle('checkbox') !!}
                    <a class="pull-right" href="{{ url('password/reset') }}">Mot de passe oublié</a>
                    {!! BootForm::submit('Connexion')->class('btn-fill btn-round btn-info') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
        <div class="space-50"></div>
    </div>
@endsection


