@extends('default')

@section('title')
    Valider mon adresse mail
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="container">
        <?php  $columnSizes = [
                'sm' => [6, 6],
                'lg' => [4, 8]
        ]; ?>
        <div id="legend">
            <h2>Valider son adresse mail</h2>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {!! BootForm::openHorizontal($columnSizes) !!}
                    {!! BootForm::text('Pseudo', 'username')->placeholder('Pseudo et non E-mail') !!}
                    {!! BootForm::password('Mot de passe', 'password_infos') !!}
                    <div class="form-group">
                        <label class="col-sm-6 col-lg-4 control-label"><i class="fa fa-info"></i></label>
                        <div class="col-sm-6 col-lg-8">Dans le cas où vous auriez fait une erreur sur votre adresse mail, merci d'entrer l'adresse mail avec laquelle associé votre compte</div>
                    </div>
                    {!! BootForm::email('E-mail', 'email') !!}
                    {!! BootForm::submit('Valider')->class('btn-fill btn-round btn-info') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection