@extends('default')

@section('title')
    Mot de passe oublié
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="space-50"></div>
    <div class="container">
        <?php  $columnSizes = [
                'sm' => [6, 6],
                'lg' => [4, 8]
        ]; ?>
        <div id="legend">
            <h2>Mot de passe oublié</h2>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {!! BootForm::openHorizontal($columnSizes) !!}
                    {!! BootForm::text('Identifiant', 'username')->placeholder('Pseudo ou E-mail...') !!}
                    {!! BootForm::password('Nouveau mot de passe', 'new_password') !!}
                    {!! BootForm::password('Confirmer nouveau mot de passe', 'new_password_confirm') !!}
                    {!! BootForm::submit('Réinitialiser')->class('btn-fill btn-round btn-info') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection