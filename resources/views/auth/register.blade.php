@extends('default')

@section('title')
    Inscription
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
<div class="space-50"></div>
<div class="space-50"></div>
<div class="space-50"></div>
<div class="container">
    <?php  $columnSizes = [
    'sm' => [6, 6],
    'lg' => [4, 8]
    ]; ?>

    <div id="legend">
        <h2>Création du Profil<br>
            <small>Ou inscription avec
                <a href="{!!URL::to('twitter')!!}" class="btn btn-round btn-fill btn-social btn-twitter">
                    <i class="fa fa-twitter"></i>
                </a>
                <a href="{!!URL::to('facebook')!!}" class="btn btn-round btn-fill btn-social btn-facebook">
                    <i class="fa fa-facebook-square"></i>
                </a>
                <a href="{!!URL::to('google')!!}" class="btn btn-round btn-fill btn-social btn-google">
                    <i class="fa fa-google-plus-square"></i>
                </a>
            </small>
        </h2>
    </div>
</div>
<div class="section section-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="image">
                        <img src="{{ asset('img/register/ban.png') }}" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                            <a href="#">
                                <img class="avatar" src="{{ asset('img/register/unknown.png') }}" alt="..."/>
                                <h4 class="title"><span id="lastname">Nom</span> <span id="firstname">Prénom</span><br />
                                    <small><span id="username">Pseudo</span></small>
                                </h4>
                            </a>
                        </div>
                        <p class="description text-center">
                            <i class="fa fa-map-marker text-muted"></i> <span id="location">Localisation</span><br>
                            <i class="fa fa-envelope-o text-muted"></i> <span id="email">Adresse Mail</span><br>
                            <i class="fa fa-birthday-cake text-muted"></i> Anniversaire : le <span id="birthday">???</span><br>
                            <i class="fa fa-clock-o text-muted"></i> Inscription : Aujourd'hui
                        </p>
                    </div>
                    <hr>
                    <div class="text-center">
                        <button href="#" class="btn btn-social btn-simple"><i class="fa fa-facebook-square"></i></button>
                        <button href="#" class="btn btn-social btn-simple"><i class="fa fa-twitter"></i></button>
                        <button href="#" class="btn btn-social btn-simple"><i class="fa fa-google-plus-square"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                {!! BootForm::openHorizontal($columnSizes) !!}
                <input type="hidden" name="key_social" value="{{ old('key_social') }}" />
                {!! BootForm::text('Pseudo', 'username') !!}
                {!! BootForm::text('Prénom', 'firstname') !!}
                {!! BootForm::text('Nom', 'lastname') !!}
                {!! BootForm::select('Sexe', 'gender')->options(['0' => 'Femme', '1' => 'Homme'])->class('selectpicker')->data_style('form-control')->data_menu_style('') !!}
                {!! BootForm::text('Date de naissance', 'birthday') !!}
                {!! BootForm::email('E-mail', 'email') !!}
                {!! BootForm::password('Mot de passe', 'password') !!}
                {!! BootForm::password('Confirmation du mot de passe', 'password_confirmation') !!}
                {!! BootForm::submit('Envoyer')->class('btn-fill btn-round btn-info') !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $( document ).ready(function() {
            $('input#birthday').datepicker({
                format: "dd/mm/yyyy",
                weekStart: 1,
                endDate: "-16y",
                startView: 2,
                language: "fr",
                orientation: "bottom auto",
                autoclose: true,
                toggleActive: true
            });
        });
        $(function() {
            $('input').each(function(){
                if($(this).val() != ''){
                    $("span#" + $(this).attr('name') + "").html($(this).val());
                }
            });
            $('input').on('keypress change input hover mouseover', function() {
                if($(this).val() != '' || $('input#birthday').val() != '') {
                    if($(this).attr('name') == 'birthday' || $('input#birthday').attr('name') == 'birthday'){
                        var date = $('input#birthday').val().split("/");

                        var months = [];
                        months[1] = 'Janvier';
                        months[2] = 'Février';
                        months[3] = 'Mars';
                        months[4] = 'Avril';
                        months[5] = 'Mai';
                        months[6] = 'Juin';
                        months[7] = 'Juillet';
                        months[8] = 'Août';
                        months[9] = 'Septembre';
                        months[10] = 'Octobre';
                        months[11] = 'Novembre';
                        months[12] = 'Décembre';

                        var day = date[0];
                        var month = parseInt(date[1])

                        $("span#birthday").html(date[0]+" "+months[month]);
                    }
                    if($(this).val() != '' && $(this).attr('name') != 'birthday'){
                        $("span#" + $(this).attr('name') + "").html($(this).val());
                    }
                }
            });
        });
    </script>
@endsection
