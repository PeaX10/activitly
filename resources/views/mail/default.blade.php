<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Activitly</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport' />
    <link href="{{ asset("css/bootstrap.css") }}" rel="stylesheet" />
    <link href="{{ asset("css/gsdk.css") }}" rel="stylesheet" />
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/home.css") }}" rel="stylesheet"/>
    @yield('css')

    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset("css/pe-icons.css") }}" rel="stylesheet" />

</head>

<body>
    <div class="container">
        @yield('content')
    </div>
</body>