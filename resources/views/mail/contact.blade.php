@extends('mail.default')

@section('content')
    <h2>{{ $name }} a laissé un message sur Activitly</h2>
    <hr>
    <br>
    <p>Voici le contenu du message :</p><br>
    <blockquote>
        <p>{!! nl2br(e($msg)) !!}</p>
        <small>
            {{ $name }}
        </small>
    </blockquote>

    <br>
    <p>E-mail de contact : {{ $email }}</p>
    <br />

    <p>A bientôt sur Activitly.com</p>
    <br>
@endsection