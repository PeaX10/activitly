@extends('mail.default')

@section('content')
    <h2>Bonjour {{ $username }} !</h2>

    <p>Votre demande de modification du mot de passe a bien été enregistrée.<br>Cependant afin de l'activer, nous vous prions de bien vouloir le confirmer en suivant le lien suivant : <a href="{{ url('password/confirm/'.$token) }}">{{ url('password/confirm/'.$token) }}</a></p>

    <br />
    <h3>Rappel de vos identifiants</h3>
    <p>Pseudo : {{ $username }}</p>
    <p>E-mail : {{ $email }}</p>
    <p>Nouveau mot de passe : {{ $password }}</p>

    <div align="center">
        <a href="{{ url('password/confirm/'.$token) }}" class="btn btn-round btn-fill">
            <i class="fa fa-user"></i> Confirmer
        </a>
    </div>
    <br><br>

    <p>A bientôt sur Activitly.com</p><br>

@endsection