@extends('mail.default')

@section('content')
    <h2>Bienvenue sur Activitly {{ $username }} !</h2>

    <p>Nous vous confirmons que votre compte a bien été créer.<br>Cependant afin que celui-ci soit actif nous vous prions de bien vouloir le confirmer en cliquant sur le lien suivant : <a href="{{ url('account/confirm/'.$token) }}">{{ url('account/confirm/'.$token) }}</a></p>

    <br />
    <h3>Rappel de vos identifiants</h3>
    <p>Pseudo : {{ $username }}</p>
    <p>E-mail : {{ $email }}</p>
    <p>Mot de passe : {{ $password }}</p>

    <div align="center">
        <a href="{{ url('account/confirm/'.$token) }}" class="btn btn-round btn-fill">
            <i class="fa fa-user"></i> Confirmer
        </a>
    </div>
    <br><br>

    <p>A bientôt sur Activitly.com</p><br>

@endsection