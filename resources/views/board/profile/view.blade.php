@extends('default')

@section('title')
    Profil de {{ $profile->username }}
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/profile.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    @include('board.search-box')
    <div class="container">
        <div class="row app">
            <div class="row-height">
                <div class="col-xs-12 col-sm-4 col-sm-height col-sm-top no-padding">
                    <div class="inside inside-full-height">
                        <div class="content">
                            <div class="card card-user" id="card-profile">
                                <div class="image preview-lg">
                                    <img src="@if(!empty($profile->banner)){{ asset('img/banner/'.$profile->banner.'.png') }}@else{{ asset('img/register/ban.png') }}@endif" alt="..."/>
                                </div>
                                <div class="content">
                                    <div class="author">
                                        <a href="#">
                                            <div class="img-preview preview-lg avatar" style="margin:auto; margin-bottom:5px"><img src="@if(!empty($profile->avatar)){{ asset('img/avatar/'.$profile->avatar.'.png') }}@else{{ asset('img/register/unknown.png') }}@endif" style="max-width: 126px; max-height: 126px" alt="..."/></div>
                                            <h4 class="title">
                                                @if(Auth::check() && Auth::user()->id != $profile->id)
                                                    <?php $friendship = App\Friendship::where('user_id', Auth::user()->id)->where('friend_id', $profile->id) ?>
                                                    @if($friendship->count() == 0)
                                                    <a href="{{ url('profile/'.$profile->username.'/friend/add') }}" class="btn btn-round btn-fill btn-info"><i class="fa fa-user-plus" aria-hidden="true"></i> Suivre</a>
                                                    @else

                                                        <p>@if(App\Friendship::where('friend_id', Auth::user()->id)->where('user_id', $profile->id)->count() == 0)Suivi @else Ami @endif depuis : {{ \Carbon\Carbon::parse($friendship->first()->created_at)->diffForHumans() }}<br><a href="{{ url('profile/'.$profile->username.'/friend/delete') }}"><small class="text-muted"><i class="fa fa-user-times" aria-hidden="true"></i> Ne plus suivre</a></small></p>
                                                    @endif
                                                @endif
                                                <br><br>
                                                <span id="status">
                                                @if(Carbon\Carbon::parse($profile->last_activity)->diffInMinutes() <= 3)
                                                    <i class="fa fa-circle-o" aria-hidden="true" style="color:#46be8a"></i> En ligne</>
                                                @else
                                                    <i class="fa fa-circle-o" aria-hidden="true" style="color:#fb434a"></i> Hors Ligne</>
                                                @endif
                                                </span><br />
                                                <small><span id="last_activity">Dernière activité: {{ Carbon\Carbon::parse($profile->last_activity)->diffForHumans() }}</span></small>
                                            </h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $activities = explode(',', $profile->activities);
                            ?>
                            @if(count($activities) > 0)
                            <div class="content box-content">
                                <h6>Activités favorites</h6>
                                <table class="table">
                                    @foreach($activities as $activity)
                                    <tr>
                                        <td><i class="fa fa-check-circle-o"></i></td>
                                        <td>{{ $activity }}</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                            @endif
                            <div class="content box-content">
                                <h6>Information</h6>
                                <table class="table">
                                    <tr>
                                        <td><i class="fa fa-envelope-o"></i></td>
                                        <td>{{ $profile->email }}</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-map-marker"></i></td>
                                        <td>{{ $profile->location }}</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-birthday-cake"></i></td>
                                        <td>{{ Carbon\Carbon::parse($profile->birthday)->age }} ans</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-clock-o"></i></td>
                                        <td>{{ Carbon\Carbon::parse($profile->created_at)->diffForHumans() }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-height col-sm-top no-padding">
                    <div class="inside inside-full-height">
                        <div class="box-content row">
                            <div class="col-md-5 col-sm-12">
                                <h5>{{ $profile->firstname }} {{ $profile->lastname }} <small class="text-info">&#64;{{ $profile->username }}</small></h5>
                                <h6>{{ $profile->location }}</h6>
                            </div>
                            <div class="col-md-7 col-sm-12 stats" align="center">
                                <div class="col-xs-4">
                                    <h4>{{ $nb_folowers = count($folowers) }}<br><small>Abonnements</small></h4>
                                </div>
                                <div class="col-xs-4">
                                    <h4>{{ $nb_subscriptions = count($subscriptions) }}<br><small>Abonnés</small></h4>
                                </div>
                                <div class="col-xs-4">
                                    <h4>0<br><small>Activ'Coins</small></h4>
                                </div>
                            </div>
                        </div>
                        <div class="row box-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="nav nav-text" role="tablist">
                                        <li class="active">
                                            <a href="#wall" role="tab" data-toggle="tab">
                                                <i class="fa fa-bars"></i> Mur
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#activity" role="tab" data-toggle="tab">
                                                <i class="fa fa-child"></i> Activités
                                            </a>
                                        </li>
                                    </ul>
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="wall">
                                            @if(Auth::check())
                                            <div class="user-wall-posting">
                                                <div class="media media-post">
                                                    <a class="pull-left author" href="#">
                                                        <div class="avatar">
                                                            <img class="media-object" alt="64x64" src="@if(!empty($user->avatar)){{ asset('img/avatar/'.$user->avatar.'.png') }}@else{{ asset('img/register/unknown.png') }}@endif">
                                                        </div>
                                                    </a>
                                                    <div class="media-body">
                                                        {!! BootForm::open() !!}
                                                        <textarea class="form-control" name="content" placeholder="@if($user->id == $profile->id) Exprimez-vous @else Laissez un message sur le mur de &#64;{{ $profile->username }}@endif" rows="3"></textarea>
                                                        <div class="media-footer">
                                                            <input type="submit" href="#" class="btn btn-info btn-fill pull-right" value="Publier">
                                                        </div>
                                                        {!! BootForm::close() !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            @endif
                                            <div class="user-posts">
                                                @include('board.profile.posts')
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="activity">
                                            <div class="col-md-12">
                                                <div class="legend text-left">
                                                    <h4>Activités crées par {{ $profile->username }} <span class="label label-danger label-fill">{{ $events = count($my_activities) }}</span></h4>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                            @if($events == 0)
                                                <h5 class="text-center">Aucune activité</h5>
                                            @else
                                                @foreach($my_activities as $event)
                                                    <div class="col-md-6">
                                                        <div class="card card-background">
                                                            <div class="image" style="background-image: url(@if(!empty($event->cover)){{ asset('img/cover/'.$event->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif); background-size: cover; background-position: 50% 50%;">
                                                                <img src="@if(!empty($event->cover)){{ asset('img/cover/'.$event->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif" alt="..." style="display: none;">
                                                                <div class="filter filter-black"></div>
                                                            </div>
                                                            <div class="content">
                                                                <h5 class="category"><i class="fa fa-child"></i> <span id="card-activity">{{ App\Activity::where('id', $event->activity_id)->first()->name }}</span>
                                                                    <a href="{{ url('activity/'.$event->id.'/view') }}" rel="tooltip" data-placement="left" title="Voir l'activité" class="pull-right">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>
                                                                </h5>
                                                                <a href="{{ url('activity/'.$event->id.'/view') }}">
                                                                    <h4 class="title">
                                                                        <span id="card-title">{{ $event->title }}</span><br>
                                                                    </h4>
                                                                    <h5>
                                                                        <small>
                                                                            <i class="fa fa-map-marker"></i> <b><span id="card-location">{{ $event->location }}</span></b><br><i class="fa fa-clock-o"></i> le <b><span id="card-date">{{ strftime('%e %B', time($event->date)) }}</span></b> @if(empty($event->end_time))à <b>{{ $event->start_time }}</b>@else()de <b>{{ $event->start_time }}</b> à <b>{{ $event->end_time }}</b>@endif
                                                                        </small>
                                                                    </h5>
                                                                </a>
                                                                @if($event->place > 0)
                                                                    <a class="">@if(App\Participation::where('event', $event->id)->count() <= $event->place)<?php $participation = App\Participation::where('event', $event->id)->count(); $attente = 0; ?>@else <?php $participation = $event->place; $attente = App\Participation::where('event', $event->id)->count() - $event->place; ?>@endif {{ $participation }} participant(s) et {{ $attente }} en attente</a>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $participation }}" aria-valuemin="0" aria-valuemax="{{ $event->place }}" style="width:{{ number_format($participation/$event->place*100, 0) }}%">
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    <a class="">{{ App\Participation::where('event', $event->id)->count() }} participants</a>
                                                                @endif
                                                            </div>
                                                            <div class="footer">
                                                                <div class="author pull-right">
                                                                    <a href="{{ url('profile/'.$user->username) }}">
                                                                        <img src="@if(!empty($user->avatar)){{ url('img/avatar/'.$user->avatar.'.png') }}@else {{ url('img/register/unknown.png') }}@endif" class="avatar">
                                                                        <span>{{ $user->username }}</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                            <div class="col-md-12">
                                                <div class="legend text-left">
                                                    <h4>Activités auxquelles {{ $profile->username }} va parciper <span class="label label-danger label-fill">{{ $events = count($activities_next) }}</span></h4>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                @if($events == 0)
                                                    <h5 class="text-center">Aucune activité</h5>
                                                @else
                                                    @foreach($activities_next as $event)
                                                        <div class="col-md-6">
                                                            <div class="card card-background">
                                                                <div class="image" style="background-image: url(@if(!empty($event->cover)){{ asset('img/cover/'.$event->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif); background-size: cover; background-position: 50% 50%;">
                                                                    <img src="@if(!empty($event->cover)){{ asset('img/cover/'.$event->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif" alt="..." style="display: none;">
                                                                    <div class="filter filter-black"></div>
                                                                </div>
                                                                <div class="content">
                                                                    <h5 class="category"><i class="fa fa-child"></i> <span id="card-activity">{{ App\Activity::where('id', $event->activity_id)->first()->name }}</span>
                                                                        <a href="{{ url('activity/'.$event->id.'/view') }}" rel="tooltip" data-placement="left" title="Voir l'activité" class="pull-right">
                                                                            <i class="fa fa-eye"></i>
                                                                        </a>
                                                                    </h5>
                                                                    <a href="{{ url('activity/'.$event->id.'/view') }}">
                                                                        <h4 class="title">
                                                                            <span id="card-title">{{ $event->title }}</span><br>
                                                                        </h4>
                                                                        <h5>
                                                                            <small>
                                                                                <i class="fa fa-map-marker"></i> <b><span id="card-location">{{ $event->location }}</span></b><br><i class="fa fa-clock-o"></i> le <b><span id="card-date">{{ strftime('%e %B', time($event->date)) }}</span></b> @if(empty($event->end_time))à <b>{{ $event->start_time }}</b>@else()de <b>{{ $event->start_time }}</b> à <b>{{ $event->end_time }}</b>@endif
                                                                            </small>
                                                                        </h5>
                                                                    </a>
                                                                    @if($event->place > 0)
                                                                        <a class="">@if(App\Participation::where('event', $event->id)->count() <= $event->place)<?php $participation = App\Participation::where('event', $event->id)->count(); $attente = 0; ?>@else <?php $participation = $event->place; $attente = App\Participation::where('event', $event->id)->count() - $event->place; ?>@endif {{ $participation }} participant(s) et {{ $attente }} en attente</a>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $participation }}" aria-valuemin="0" aria-valuemax="{{ $event->place }}" style="width:{{ number_format($participation/$event->place*100, 0) }}%">
                                                                            </div>
                                                                        </div>
                                                                    @else
                                                                        <a class="">{{ App\Participation::where('event', $event->id)->count() }} participants</a>
                                                                    @endif
                                                                </div>
                                                                <div class="footer">
                                                                    <div class="author pull-right">
                                                                        <a href="{{ url('profile/'.$user->username) }}">
                                                                            <img src="@if(!empty($user->avatar)){{ url('img/avatar/'.$user->avatar.'.png') }}@else {{ url('img/register/unknown.png') }}@endif" class="avatar">
                                                                            <span>{{ $user->username }}</span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                                    <div class="col-md-12">
                                                        <div class="legend text-left">
                                                            <h4>Activités auxquelles {{ $profile->username }} a participé <span class="label label-danger label-fill">{{ $events = count($activities_prev) }}</span></h4>
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        @if($events == 0)
                                                            <h5 class="text-center">Aucune activité</h5>
                                                        @else
                                                            @foreach($activities_prev as $event)
                                                                <div class="col-md-6">
                                                                    <div class="card card-background">
                                                                        <div class="image" style="background-image: url(@if(!empty($event->cover)){{ asset('img/cover/'.$event->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif); background-size: cover; background-position: 50% 50%;">
                                                                            <img src="@if(!empty($event->cover)){{ asset('img/cover/'.$event->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif" alt="..." style="display: none;">
                                                                            <div class="filter filter-black"></div>
                                                                        </div>
                                                                        <div class="content">
                                                                            <h5 class="category"><i class="fa fa-child"></i> <span id="card-activity">{{ App\Activity::where('id', $event->activity_id)->first()->name }}</span>
                                                                                <a href="{{ url('activity/'.$event->id.'/view') }}" rel="tooltip" data-placement="left" title="Voir l'activité" class="pull-right">
                                                                                    <i class="fa fa-eye"></i>
                                                                                </a>
                                                                            </h5>
                                                                            <a href="{{ url('activity/'.$event->id.'/view') }}">
                                                                                <h4 class="title">
                                                                                    <span id="card-title">{{ $event->title }}</span><br>
                                                                                </h4>
                                                                                <h5>
                                                                                    <small>
                                                                                        <i class="fa fa-map-marker"></i> <b><span id="card-location">{{ $event->location }}</span></b><br><i class="fa fa-clock-o"></i> le <b><span id="card-date">{{ strftime('%e %B', time($event->date)) }}</span></b> @if(empty($event->end_time))à <b>{{ $event->start_time }}</b>@else()de <b>{{ $event->start_time }}</b> à <b>{{ $event->end_time }}</b>@endif
                                                                                    </small>
                                                                                </h5>
                                                                            </a>
                                                                            @if($event->place > 0)
                                                                                <a class="">@if(App\Participation::where('event', $event->id)->count() <= $event->place)<?php $participation = App\Participation::where('event', $event->id)->count(); $attente = 0; ?>@else <?php $participation = $event->place; $attente = App\Participation::where('event', $event->id)->count() - $event->place; ?>@endif {{ $participation }} participant(s) et {{ $attente }} en attente</a>
                                                                                <div class="progress">
                                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $participation }}" aria-valuemin="0" aria-valuemax="{{ $event->place }}" style="width:{{ number_format($participation/$event->place*100, 0) }}%">
                                                                                    </div>
                                                                                </div>
                                                                            @else
                                                                                <a class="">{{ App\Participation::where('event', $event->id)->count() }} participants</a>
                                                                            @endif
                                                                        </div>
                                                                        <div class="footer">
                                                                            <div class="author pull-right">
                                                                                <a href="{{ url('profile/'.$user->username) }}">
                                                                                    <img src="@if(!empty($user->avatar)){{ url('img/avatar/'.$user->avatar.'.png') }}@else {{ url('img/register/unknown.png') }}@endif" class="avatar">
                                                                                    <span>{{ $user->username }}</span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/search.js') }}"/>
    <script type="text/javascript">
        $(document).ready(function(){
            var bottom_nav = 74; // MARGIN
            var top_search = $('.search').offset();
            var height_search = $('.search').height() + 60; // PADDING
            var height_alert = $('.alert').height();
            $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
            $('.search').css('z-index', 9999);
            $('.search').css('z-index', 9999);
            $('.app .nav-links').css('padding-top', height_search);
            $('.app .right-side').css('padding-top', height_search);
            $('.alert').css('top', bottom_nav + height_search);
            $('.app').css('padding-top', height_search + height_alert);
            window.onresize = function() {
                var top_search = $('.search').offset();
                var height_search = $('.search').height() + 60; // PADDING
                var height_alert = $('.alert').height();
                $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
                $('.search').css('z-index', 9999);
                $('.search').css('z-index', 9999);
                $('.app').css('padding-top', height_search + height_alert);
                $('.alert').css('top', bottom_nav + height_search);
            }
        });

        $( "#slider-km" ).slider({
            value: 30,
            orientation: "horizontal",
            max:150,
            range: "min",
            animate: true,
            change: function( event, ui ) {
                $('span#rayon-km').html(ui.value);
            }
        });

        $('.icon-tooltip').tooltip();

    </script>
    <script>
        $(window).on('hashchange', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPosts(page);
                }
            }
        });
        $(document).ready(function() {
            $(document).on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href').split('page=')[1]);
                e.preventDefault();
            });
        });
        function getPosts(page) {
            $.ajax({
                url : '?page=' + page

            }).done(function (data) {
                $('.user-posts').html(data);
                location.hash = page;
            }).fail(function (error) {
                alert('Posts could not be loaded.');
            });
        }
    </script>
@endsection
