@extends('default')

@section('title')
    Mes Amis
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    @include('board.search-box')
    <div class="container">
        <div class="row app">
            <div class="col-md-12">
                <div class="legend text-left">
                    <h4>Mes amis <span class="label label-danger label-fill">{{ $nb_friends = count($friends) }}</span></h4>
                    <hr>
                    <br>
                </div>
            </div>
            @foreach($friends as $friend)
                <div class="col-md-4">
                    <div class="card card-user" id="card-profile">
                        <div class="image preview-lg">
                            <img src="@if(!empty($friend->banner)){{ asset('img/banner/'.$friend->banner.'.png') }}@else{{ asset('img/register/ban.png') }}@endif" alt="..."/>
                        </div>
                        <div class="content">
                            <div class="author">
                                <a href="#">
                                    <div class="img-preview preview-lg avatar" style="margin:auto; margin-bottom:5px"><img src="@if(!empty($friend->avatar)){{ asset('img/avatar/'.$friend->avatar.'.png') }}@else{{ asset('img/register/unknown.png') }}@endif" style="max-width: 126px; max-height: 126px" alt="..."/></div>
                                    <h4 class="title"><span id="lastname">{{ $friend->lastname }}</span> <span id="firstname">{{ $friend->firstname }}</span><br />
                                        <small><span id="username">{{ $friend->username }}</span></small>
                                    </h4>
                                </a>
                            </div>
                            <p class="description text-center">
                                <i class="fa fa-map-marker text-muted"></i> <span id="location">{{ $friend->location }}</span><br>
                                <i class="fa fa-envelope-o text-muted"></i> <span id="email">{{ $friend->email }}</span><br>
                                <i class="fa fa-birthday-cake text-muted"></i> Age : <span id="birthday">{{ Carbon\Carbon::parse($friend->birthday)->age }} ans</span><br>
                                <i class="fa fa-clock-o text-muted"></i> Inscription : {{ Carbon\Carbon::parse($friend->created_at)->diffForHumans() }}
                            </p>
                        </div>
                        <hr>
                        <div class="text-center">
                            <button href="#" class="btn btn-social btn-simple"><i class="fa fa-facebook-square"></i></button>
                            <button href="#" class="btn btn-social btn-simple"><i class="fa fa-twitter"></i></button>
                            <button href="#" class="btn btn-social btn-simple"><i class="fa fa-google-plus-square"></i></button>
                        </div>
                    </div>
                </div>
            @endforeach
            @if($nb_friends === 0)
                <div class="col-md-12"><h5 class="text-center">Aucun ami pour le moment !</h5></div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/search.js') }}"/>
    <script type="text/javascript">
        $(document).ready(function(){
            var bottom_nav = 74; // MARGIN
            var top_search = $('.search').offset();
            var height_search = $('.search').height() + 60; // PADDING
            var height_alert = $('.alert').height();
            $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
            $('.search').css('z-index', 9999);
            $('.search').css('z-index', 9999);
            $('.app .nav-links').css('padding-top', height_search);
            $('.app .right-side').css('padding-top', height_search);
            $('.alert').css('top', bottom_nav + height_search);
            $('.app').css('padding-top', height_search + height_alert);
            window.onresize = function() {
                var top_search = $('.search').offset();
                var height_search = $('.search').height() + 60; // PADDING
                var height_alert = $('.alert').height();
                $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
                $('.search').css('z-index', 9999);
                $('.search').css('z-index', 9999);
                $('.app').css('padding-top', height_search + height_alert);
                $('.alert').css('top', bottom_nav + height_search);
            }
        });

        $( "#slider-km" ).slider({
            value: 30,
            orientation: "horizontal",
            max:150,
            range: "min",
            animate: true,
            change: function( event, ui ) {
                $('span#rayon-km').html(ui.value);
            }
        });

        $('.icon-tooltip').tooltip();

    </script>
@endsection
