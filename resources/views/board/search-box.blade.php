<div class="section section-gray search">
    <div class="container">
        <div class="row">
            <form name="search" method="GET" action="{{ url('search') }}">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="input-group">
                        <input id="activities" type="text" class="col-md-12 form-control" name="activity" placeholder="Quelle activité ?" value="{{ Input::get('activity') }}"/>
                        <span class="input-group-addon"><i class="fa fa-child"></i></span>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="input-group">
                        <input id="cities" class="form-control" type="text" name="city" placeholder="Quelle ville ?" value="{{ Input::get('city') }}" />
                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="input-group">
                        <input class="form-control" type="text" id="date" name="date" placeholder="Quel jour ?" value="{{ Input::get('date') }}" />
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-md-3 col-sm-3 col-xs-6" align="center">
                    <input type="submit" class="btn btn-info btn-round btn-fill btn-block" value="Rechercher">
                </div>
            </form>
        </div>
    </div>
</div>