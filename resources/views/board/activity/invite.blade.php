@extends('default')

@section('title')
   Invitations
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    @include('board.search-box')
    <div class="container">
        <div class="row app">
            <div class="col-md-9 col-xs-12">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="{{ url('activity') }}">Mes activités</a></li>
                    <li><a href="{{ url('activity/next') }}">Activités à venir @if($menu['activities_next'] > 0)<span class="label label-warning label-fill" style="font-size:9px; border:none">{{ $menu['activities_next'] }}</span>@endif</a></li>
                    <li><a href="{{ url('activity/prev') }}">Activité passés</a></li>
                    <li class="active"><a href="{{ url('activity/invites') }}">Mes invitations @if($menu['activities_invites'] > 0)<span class="label label-warning label-fill" style="font-size:9px; border:none">{{ $menu['activities_invites'] }}</span>@endif</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="text-right">
                    <a class="btn btn-round btn-fill btn-block btn-danger" href="{{ url('activity/create') }}">Créer une activité</a></li>
                </div>
            </div>
            <div class="col-md-12">
                <div class="legend text-left">
                    <h4>Invitations <span class="label label-danger label-fill">{{ $events = count($invites) }}</span></h4>
                    <hr>
                    <br>
                </div>
            </div>
            <div class="col-md-12">
                @if($events == 0)
                    <h5 class="text-center">Aucune invitation</h5>
                    <div class="space-50"></div>
                @else
                    @foreach($invites as $invite)
                        <div class="media">
                            <?php
                                $sender = App\User::where('id', $invite->sender)->select('username', 'avatar')->first();
                            ?>
                            <a class="pull-left" href="{{ url('profile/'.$sender->username) }}">
                                <div class="avatar">
                                    <img class="media-object" src="@if(!empty($sender->avatar)){{ url('img/avatar/'.$sender->avatar.'.png') }}@else {{ url('img/register/unknown.png') }}@endif" alt="...">
                                </div>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">{{ $sender->username }} vous a invité à <a href="{{ url('activity/'.$invite->event.'/view') }}">{{ App\Event::where('id', $invite->event)->first()->title }}</a></h4>
                                <h6 class="pull-right text-muted">{{ Jenssegers\Date\Date::createFromFormat('Y-m-d H:i:s', $invite->created_at)->diffForHumans() }}</h6>
                                <p>{!! nl2br(e($invite->message)) !!}</p>
                                <div class="media-footer">
                                    <div class="pull-right">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a class="btn btn-success btn-fill btn-round" href="{{ url('event/'.$invite->id) }}"><i class="fa fa-check"></i> Accepter</a>
                                            </div>
                                            <div class="col-md-6">
                                                <form action="" method="post">
                                                    <input type="hidden" value="{{ $invite->id }}" name="invite">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button class="btn btn-danger btn-fill btn-round"><i class="fa fa-times"></i> Décliner</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/search.js') }}"/>
    <script type="text/javascript">
        $(document).ready(function(){
            var bottom_nav = 74; // MARGIN
            var top_search = $('.search').offset();
            var height_search = $('.search').height() + 60; // PADDING
            var height_alert = $('.alert').height();
            $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
            $('.search').css('z-index', 9999);
            $('.search').css('z-index', 9999);
            $('.app .nav-links').css('padding-top', height_search);
            $('.app .right-side').css('padding-top', height_search);
            $('.alert').css('top', bottom_nav + height_search);
            $('.app').css('padding-top', height_search + height_alert);
            window.onresize = function() {
                var top_search = $('.search').offset();
                var height_search = $('.search').height() + 60; // PADDING
                var height_alert = $('.alert').height();
                $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
                $('.search').css('z-index', 9999);
                $('.search').css('z-index', 9999);
                $('.app').css('padding-top', height_search + height_alert);
                $('.alert').css('top', bottom_nav + height_search);
            }
        });

        $( "#slider-km" ).slider({
            value: 30,
            orientation: "horizontal",
            max:150,
            range: "min",
            animate: true,
            change: function( event, ui ) {
                $('span#rayon-km').html(ui.value);
            }
        });

        $('.icon-tooltip').tooltip();

    </script>
@endsection
