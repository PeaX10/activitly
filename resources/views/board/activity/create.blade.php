@extends('default')

@section('title')
    Créer une activité
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/cropper.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    @include('board.search-box')
    <div class="app" style="padding-bottom:0px">
        <div class="container">
            <div class="legend">
                <h3>Créer une activité</h3>
            </div>
        </div>
        <?php
            $columnSizes = [
            'sm' => [0, 12],
            'lg' => [0, 8]
            ];
        ?>
        <div class="section section-gray">
            <div class="container">
                <div class="col-md-4">
                    <div class="card card-background">
                        <div class="image" style="background-image: url({{ asset('img/extra/cover.png') }}); background-size: cover; background-position: 50% 50%;">
                            <img src="{{ asset('img/extra/cover.png') }}" alt="..." style="display: none;">
                            <div class="filter filter-black"></div>
                        </div>
                        <div class="content">
                            <h5 class="category"><i class="fa fa-child"></i> <span id="card-activity">?????</span></h5>
                            <a href="#">
                                <h4 class="title">
                                    <span id="card-title">??????</span><br>
                                </h4>
                                <h5>
                                    <small>
                                        <i class="fa fa-map-marker"></i> <b><span id="card-location">{{ $user->location }}</span></b><br><i class="fa fa-clock-o"></i> le <b><span id="card-date">??</span></b> <span id="trans-time">à</span> <b><span id="card-start_time">??</span></b><span id="card-end-time"> à <b><span id="card-end_time">??</span></b></span>
                                    </small>
                                </h5>
                            </a>
                        </div>
                        <div class="footer">
                            <div class="author pull-right">
                                <a href="#">
                                    <img src="@if(!empty($user->avatar)){{ url('img/avatar/'.$user->avatar.'.png') }}@else {{ url('img/register/unknown.png') }}@endif" class="avatar">
                                    <span>{{ $user->username }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                {!! BootForm::open()->enctype("multipart/form-data") !!}
                <div class="form-group {!! $errors->first('title', ' has-error') !!}">
                    <div class="input-group">
                        <input type="text" name="title" placeholder="Titre de votre activité" class="form-control" id="title" value="{{ old('title') }}">
                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                    </div>
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->first('cover_img', ' has-error') !!}">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-fill btn-info btn-file">
                                Changer la photo de couverture&hellip; <input type="file" name="cover_img" id="cover_img"  value="{{ old('cover_img') }}">
                            </span>
                        </span>
                        <input type="text" class="form-control" readonly>
                    </div>
                    {!! $errors->first('cover_img', '<p class="help-block">:message</p>') !!}
                </div>
                <input type="hidden" value="" name="Latlng" id="Latlng">
                <div class="form-group {!! $errors->first('location', ' has-error') !!}">
                    <div class="input-group">
                        <input type="text" name="location" placeholder="Lieu de votre activité" class="form-control" value="{{ old('location') ?: $user->location }}" id="location">
                        <span class="input-group-addon"><i class="fa fa-location-arrow"></i></span>
                    </div>
                    {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="card card-background">
                    <div class="map" id="map"></div>
                </div>

                <div class="form-group {!! $errors->first('activity', ' has-error') !!}">
                    <div class="input-group">
                        <input name="activity" id="activity" type="text" class="col-md-12 form-control" placeholder="Quelle activité ?"  value="{{ old('activity') }}"/>
                        <span class="input-group-addon"><i class="fa fa-child"></i></span>
                    </div>
                    {!! $errors->first('activity', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->first('date-activity', ' has-error') !!}">
                    <div class="input-group">
                        <input class="form-control" type="text" name="date-activity" id="date-activity" placeholder="Quel jour ?" value="{{ old('date-activity') }}" />
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('date-activity', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->first('start_time', ' has-error') !!}">
                    <div class="input-group clockpicker">
                        <input class="form-control" type="text" id="start_time" name="start_time" placeholder="Heure de début" value="{{ old('start_time') }}" />
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    </div>
                    {!! $errors->first('start_time', '<p class="help-block">:message</p>') !!}
                </div>
                {!! BootForm::checkbox('Heure de fin ?', 'endtime')->data_toggle('checkbox')->id('endtime') !!}
                <div class="form-group {!! $errors->first('end_time', ' has-error') !!}" id="end-time">
                    <div class="input-group clockpicker">
                        <input class="form-control" type="text" id="end_time" name="end_time" placeholder="Heure de fin" value="{{ old('end_time') }}"/>
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    </div>
                    {!! $errors->first('end_time', '<p class="help-block">:message</p>') !!}
                </div>
                {!! BootForm::textarea(null, 'description')->placeholder('Description de l\'activité... ') !!}
                <?php
                    $options[0] = 'Illimité';
                    for ($i = 1; $i <= 100; $i++) {
                        $options[$i] = $i;
                    }
                ?>
                {!! BootForm::select('Nombre de place', 'place')->options($options)->class('selectpicker')->data_style('form-control')->data_menu_style('') !!}
                {!! BootForm::submit('Créer mon activité')->class('btn-round btn-fill btn-danger') !!}
                {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/add_activity.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/search.js') }}"/>
@endsection
