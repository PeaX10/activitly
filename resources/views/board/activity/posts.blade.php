@if(count($posts) == 0) <p align="center" class="text-info">Il n'y a aucune publication</p>@endif
@foreach ($posts as $post)
    <?php
    $user_post = App\User::where('id', $post->user)->first();
    ?>
    <div class="media">
        <a class="pull-left" href="{{ url('profile/'.$user_post->username) }}">
            <div class="avatar">
                <img class="media-object" src="@if(!empty($user_post->avatar)){{ asset('img/avatar/'.$user_post->avatar.'.png') }}@else{{ asset('img/register/unknown.png') }}@endif" alt="...">
            </div>
        </a>
        <div class="media-body">
            <p class="media-heading">
                <a href="{{ url('profile/'.$user_post->username) }}" class="text-info">&#64;{{ $user_post->username }}</a>
                <br>
                <small class="text-default">{{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</small>
            </p>
            @if(Auth::check() && ($post->user == $user->id || App\Event::where('id', $post->event)->first()->owner == $user->id))<a href="{{ url('post/activity/'.$post->id.'/delete') }}" class="pull-right"><i class="fa fa-times text-danger"></i></a>@endif
            <p>
                {{ $post->content }}
            </p>
        </div>
    </div>
@endforeach
<div align="center">{!! $posts->render() !!}</div>