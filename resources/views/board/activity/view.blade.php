@extends('default')

@section('title')
    {{ $activity->title }} par &#64;{{ $owner->username }}
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/profile.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    @include('board.search-box')
    <div class="container">
        <div class="row app">
            <div class="row-height">
                <div class="col-xs-12 col-sm-4 col-sm-height col-sm-top no-padding">
                    <div class="inside inside-full-height">
                        <div class="content">
                            <div class="card card-background">
                                <div class="image" style="background-image: url(@if(!empty($activity->cover)){{ asset('img/cover/'.$activity->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif); background-size: cover; background-position: 50% 50%;">
                                    <img src="@if(!empty($activity->cover)){{ asset('img/cover/'.$activity->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif" alt="..." style="display: none;">
                                    <div class="filter filter-black"></div>
                                </div>
                                <div class="content">
                                    <h5 class="category"><i class="fa fa-child"></i> <span id="card-activity">{{ App\Activity::where('id', $activity->activity_id)->first()->name }}</span>
                                    </h5>
                                    <a href="{{ url('activity/'.$activity->id.'/view') }}">
                                        <h4 class="title">
                                            <span id="card-title">{{ $activity->title }}</span><br>
                                        </h4>
                                        <h5>
                                            <small>
                                                <i class="fa fa-map-marker"></i> <b><span id="card-location">{{ $activity->location }}</span></b><br><i class="fa fa-clock-o"></i> le <b><span id="card-date">{{ strftime('%e %B', time($activity->date)) }}</span></b> @if(empty($activity->end_time))à <b>{{ $activity->start_time }}</b>@else()de <b>{{ $activity->start_time }}</b> à <b>{{ $activity->end_time }}</b>@endif
                                            </small>
                                        </h5>
                                    </a>
                                </div>
                                <div class="footer">
                                    <a href="{{ url('activity/'.$activity->id.'/delete') }}" rel="tooltip" data-placement="right" title="Supprimer l'activité" class="text-danger">
                                        <i class="fa fa-times"></i>
                                    </a>
                                    <div class="author pull-right">
                                        <a href="{{ url('profile/'.$owner->username) }}">
                                            <img src="@if(!empty($owner->avatar)){{ url('img/avatar/'.$owner->avatar.'.png') }}@else {{ url('img/register/unknown.png') }}@endif" class="avatar">
                                            <span>{{ $owner->username }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content box-content">
                                <h6>Description</h6>
                                <p>{{ $activity->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-height col-sm-top no-padding">
                    <div class="inside inside-full-height">
                        <div class="box-content row">
                            <div class="col-md-6 col-sm-12">
                                <h5>{{ $activity->title }} <small class="text-info">par &#64;{{ $owner->username }}</small></h5>
                                <h6>{{ $activity->location }}</h6>
                            </div>
                            <div class="col-md-6 col-sm-12 stats" align="center">

                                @if($activity->place != 0)
                                    <div class="col-xs-4">
                                        <h4>@if(count($participants) <= $activity->place) {{ count($participants) }} @else {{ $activity->place }} @endif<br><small>Participants</small></h4>
                                    </div>
                                    <div class="col-xs-4">
                                        <h4>@if(count($participants) <= $activity->place) 0 @else {{ count($participants) - $activity->place }} @endif<br><small>Attendent</small></h4>
                                    </div>
                                    <div class="col-xs-4">
                                        <h4>{{ $activity->place }}<br><small>Places</small></h4>
                                    </div>
                                @else
                                    <div class="col-xs-6">
                                        <h4>{{ count($participants) }}<br><small>Participants</small></h4>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4>&infin;<br><small>Places</small></h4>
                                    </div>
                                @endif

                            </div>
                        </div>
                        <div class="row box-content transparent">
                            <div class="card card-background">
                                <div class="map" id="map"></div>
                            </div>
                        </div>
                        <div class="row box-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="nav nav-text" role="tablist">
                                        <li class="active">
                                            <a href="#wall" role="tab" data-toggle="tab">
                                                <i class="fa fa-bars"></i> Mur
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#participant" role="tab" data-toggle="tab">
                                                <i class="fa fa-child"></i> Participants
                                            </a>
                                        </li>
                                    </ul>
                                    <hr>
                                </div>

                                <div class="col-md-12">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="wall">
                                            @if(Auth::check())
                                            <div class="user-wall-posting">
                                                <div class="media media-post">
                                                    <a class="pull-left author" href="#">
                                                        <div class="avatar">
                                                            <img class="media-object" alt="64x64" src="@if(!empty($user->avatar)){{ asset('img/avatar/'.$user->avatar.'.png') }}@else{{ asset('img/register/unknown.png') }}@endif">
                                                        </div>
                                                    </a>
                                                    <div class="media-body">
                                                        {!! BootForm::open() !!}
                                                        <textarea class="form-control" name="content" placeholder="Exprimez-vous" rows="3"></textarea>
                                                        <div class="media-footer">
                                                            <input type="submit" href="#" class="btn btn-info btn-fill pull-right" value="Publier">
                                                        </div>
                                                        {!! BootForm::close() !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            @endif
                                            <div class="user-posts">
                                                @include('board.activity.posts')
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="participant">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/search.js') }}"/>
    <script type="text/javascript">
        $(document).ready(function(){
            var bottom_nav = 74; // MARGIN
            var top_search = $('.search').offset();
            var height_search = $('.search').height() + 60; // PADDING
            var height_alert = $('.alert').height();
            $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
            $('.search').css('z-index', 9999);
            $('.search').css('z-index', 9999);
            $('.app .nav-links').css('padding-top', height_search);
            $('.app .right-side').css('padding-top', height_search);
            $('.alert').css('top', bottom_nav + height_search);
            $('.app').css('padding-top', height_search + height_alert);
            window.onresize = function() {
                var top_search = $('.search').offset();
                var height_search = $('.search').height() + 60; // PADDING
                var height_alert = $('.alert').height();
                $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
                $('.search').css('z-index', 9999);
                $('.search').css('z-index', 9999);
                $('.app').css('padding-top', height_search + height_alert);
                $('.alert').css('top', bottom_nav + height_search);
            }
        });
    </script>
    <script>
        $(window).on('hashchange', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPosts(page);
                }
            }
        });
        $(document).ready(function() {
            $(document).on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href').split('page=')[1]);
                e.preventDefault();
            });
        });
        function getPosts(page) {
            $.ajax({
                url : '?page=' + page

            }).done(function (data) {
                $('.user-posts').html(data);
                location.hash = page;
            }).fail(function (error) {
                alert('Posts could not be loaded.');
            });
        }
    </script>
    <script>
        $(document).ready(function(){
            initGoogleMaps();

            function initGoogleMaps(){
                var myLatlng = new google.maps.LatLng({{ $activity->lat }}, {{ $activity->lng }});
                var mapOptions = {
                    zoom:13,
                    center: myLatlng,
                    draggable: false,
                    scrollwheel: false
                }

                var map = new google.maps.Map(document.getElementById("map"), mapOptions);

                var marker = new google.maps.Marker({
                    position: myLatlng
                });

                marker.setMap(map);
            }
        });
    </script>
@endsection
