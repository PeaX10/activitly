@extends('default')

@section('title')
   Activités passés
@endsection

@section('css')
    <link href="{{ asset("css/global.css") }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="space-50"></div>
    <div class="space-50"></div>
    @include('board.search-box')
    <div class="container">
        <div class="row app">
            <div class="col-md-9 col-xs-12">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="{{ url('activity') }}">Mes activités</a></li>
                    <li><a href="{{ url('activity/next') }}">Activités à venir @if($menu['activities_next'] > 0)<span class="label label-warning label-fill" style="font-size:9px; border:none">{{ $menu['activities_next'] }}</span>@endif</a></li>
                    <li class="active"><a href="{{ url('activity/prev') }}">Activité passés</a></li>
                    <li><a href="{{ url('activity/invites') }}">Mes invitations @if($menu['activities_invites'] > 0)<span class="label label-warning label-fill" style="font-size:9px; border:none">{{ $menu['activities_invites'] }}</span>@endif</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="text-right">
                    <a class="btn btn-round btn-fill btn-block btn-danger" href="{{ url('activity/create') }}">Créer une activité</a></li>
                </div>
            </div>
            <div class="col-md-12">
                <div class="legend text-left">
                    <h4>Activités passés <span class="label label-danger label-fill">{{ $events = count($activities_prev) }}</span></h4>
                    <hr>
                    <br>
                </div>
            </div>
            <div class="col-md-12">
                @if($events == 0)
                    <h5 class="text-center">Aucune activité passés</h5>
                    <div class="space-50"></div>
                @else
                    @foreach($activities_prev as $event)
                        <div class="col-md-4">
                            <div class="card card-background">
                                <div class="image" style="background-image: url(@if(!empty($event->cover)){{ asset('img/cover/'.$event->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif); background-size: cover; background-position: 50% 50%;">
                                    <img src="@if(!empty($event->cover)){{ asset('img/cover/'.$event->cover.'.png') }} @else {{ asset('img/extra/cover.png') }} @endif" alt="..." style="display: none;">
                                    <div class="filter filter-black"></div>
                                </div>
                                <div class="content">
                                    <h5 class="category"><i class="fa fa-child"></i> <span id="card-activity">{{ App\Activity::where('id', $event->activity_id)->first()->name }}</span>
                                        <a href="{{ url('activity/'.$event->id.'/view') }}" rel="tooltip" data-placement="left" title="Voir l'activité" class="pull-right">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </h5>
                                    <a href="{{ url('activity/'.$event->id.'/view') }}">
                                        <h4 class="title">
                                            <span id="card-title">{{ $event->title }}</span><br>
                                        </h4>
                                        <h5>
                                            <small>
                                                <i class="fa fa-map-marker"></i> <b><span id="card-location">{{ $event->location }}</span></b><br><i class="fa fa-clock-o"></i> le <b><span id="card-date">{{ strftime('%e %B', time($event->date)) }}</span></b> @if(empty($event->end_time))à <b>{{ $event->start_time }}</b>@else()de <b>{{ $event->start_time }}</b> à <b>{{ $event->end_time }}</b>@endif
                                            </small>
                                        </h5>
                                    </a>
                                    @if($event->place > 0)
                                        <a class="">@if(App\Participation::where('event', $event->id)->count() <= $event->place)<?php $participation = App\Participation::where('event', $event->id)->count(); $attente = 0; ?>@else <?php $participation = $event->place; $attente = App\Participation::where('event', $event->id)->count() - $event->place; ?>@endif {{ $participation }} participant(s)</a>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $participation }}" aria-valuemin="0" aria-valuemax="{{ $event->place }}" style="width:{{ number_format($participation/$event->place*100, 0) }}%">
                                            </div>
                                        </div>
                                    @else
                                        <a class="">{{ App\Participation::where('event', $event->id)->count() }} participants</a>
                                    @endif
                                </div>
                                <div class="footer">
                                    <div class="author pull-right">
                                        <?php
                                          $owner = App\User::where('id', $event->owner)->select('username', 'avatar')->first();
                                        ?>
                                        <a href="{{ url('profile/'.$owner->username) }}">
                                            <img src="@if(!empty($owner->avatar)){{ url('img/avatar/'.$owner->avatar.'.png') }}@else {{ url('img/register/unknown.png') }}@endif" class="avatar">
                                            <span>{{ $owner->username }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/search.js') }}"/>
    <script type="text/javascript">
        $(document).ready(function(){
            var bottom_nav = 74; // MARGIN
            var top_search = $('.search').offset();
            var height_search = $('.search').height() + 60; // PADDING
            var height_alert = $('.alert').height();
            $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
            $('.search').css('z-index', 9999);
            $('.search').css('z-index', 9999);
            $('.app .nav-links').css('padding-top', height_search);
            $('.app .right-side').css('padding-top', height_search);
            $('.alert').css('top', bottom_nav + height_search);
            $('.app').css('padding-top', height_search + height_alert);
            window.onresize = function() {
                var top_search = $('.search').offset();
                var height_search = $('.search').height() + 60; // PADDING
                var height_alert = $('.alert').height();
                $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
                $('.search').css('z-index', 9999);
                $('.search').css('z-index', 9999);
                $('.app').css('padding-top', height_search + height_alert);
                $('.alert').css('top', bottom_nav + height_search);
            }
        });

        $( "#slider-km" ).slider({
            value: 30,
            orientation: "horizontal",
            max:150,
            range: "min",
            animate: true,
            change: function( event, ui ) {
                $('span#rayon-km').html(ui.value);
            }
        });

        $('.icon-tooltip').tooltip();

    </script>
@endsection
