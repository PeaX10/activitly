$().ready(function(){
    $.getJSON( "http://activitly.dev/json/activities", function( data ) {
        $('input#activities').typeahead({
            source: data,
        });
    });

    $("input#cities").geocomplete({
        types: ['(cities)'],
        componentRestrictions: { country: 'fr' }
    });

    $('input#date').datepicker({
        format: "dd/mm/yyyy",
        weekStart: 1,
        startDate: "today",
        endDate: "+2m",
        language: "fr",
        orientation: "top auto",
        autoclose: true,
        toggleActive: true,
        todayBtn: "linked",
        todayHighlight: true
    });
});