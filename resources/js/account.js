$(document).ready(function() {
    $('input#birthday').datepicker({
        format: "dd/mm/yyyy",
        weekStart: 1,
        endDate: "-13y",
        startView: 2,
        language: "fr",
        orientation: "bottom auto",
        autoclose: true,
        toggleActive: true
    });

    $("#stacked-legal-logo").addClass('active');
    var h_card = $('#card-profile').height() - 20;
    $("#stacked-legal-logo").removeClass('active');
    $('#form-profile').height(h_card);
});

$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});
$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
            input.val(log);
        }
    });

    $('li').on('click', function(event, numFiles, label) {
        $('.typeahead').css('display', 'none');
    });
});

$( document ).ready(function() {
    $.getJSON( "http://activitly.dev/json/activities", function( source ) {
        $('#activities_tag').typeahead({
            source: source,
        });
    });
});
$('input').keypress(function() {
    $('.tagsinput-add-container .typeahead').css('display', 'block');
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
            input.val(log);
        }
    });
});
function readURL(input, photo) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            photo.attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#profile_img").change(function(){
    $image.cropper('destroy');
    readURL(this, $('#cropper-profile img'));
});

$("#ban_img").change(function(){
    $image2.cropper('destroy');
    readURL(this, $('#cropper-ban img'));
});

var $image = $('#cropper-profile img'),
    cropBoxData,
    canvasData;

var $image2 = $('#cropper-ban img'),
    cropBoxData,
    canvasData;

$('#modalProfilePhoto').on('shown.bs.modal', function () {
    $image.cropper({
        autoCropArea: 0.5,
        aspectRatio: 1,
        preview: ".img-preview",
        built: function () {
            // Strict mode: set crop box data first
            $image.cropper('setCropBoxData', cropBoxData);
            $image.cropper('setCanvasData', canvasData);
        }
    });
}).on('hidden.bs.modal', function () {
    cropBoxData = $image.cropper('getCropBoxData');
    canvasData = $image.cropper('getCanvasData');
    $('input#profile_params').val(JSON.stringify($image.cropper('getData'), null, 2));
});

$('#modalBanPhoto').on('shown.bs.modal', function () {
    $image2.cropper({
        autoCropArea: 0.5,
        aspectRatio: 370/110,
        preview: ".image",
        built: function () {
            // Strict mode: set crop box data first
            $image2.cropper('setCropBoxData', cropBoxData);
            $image2.cropper('setCanvasData', canvasData);
        }
    });
}).on('hidden.bs.modal', function () {
    cropBoxData = $image2.cropper('getCropBoxData');
    canvasData = $image2.cropper('getCanvasData');
    $('input#ban_params').val(JSON.stringify($image2.cropper('getData'), null, 2));
});