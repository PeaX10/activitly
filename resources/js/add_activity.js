$(document).ready(function(){
    var bottom_nav = 74; // MARGIN
    var top_search = $('.search').offset();
    var height_search = $('.search').height() + 60; // PADDING
    var height_alert = $('.alert').height();
    $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
    $('.search').css('z-index', 9999);
    $('.search').css('z-index', 9999);
    $('.app .nav-links').css('padding-top', height_search);
    $('.app .right-side').css('padding-top', height_search);
    $('.alert').css('top', bottom_nav + height_search);
    $('.app').css('padding-top', height_search + height_alert);
    window.onresize = function() {
        var top_search = $('.search').offset();
        var height_search = $('.search').height() + 60; // PADDING
        var height_alert = $('.alert').height();
        $('.search').css({position: 'fixed', top: bottom_nav, right: '0px', left: '0px'});
        $('.search').css('z-index', 9999);
        $('.search').css('z-index', 9999);
        $('.app').css('padding-top', height_search + height_alert);
        $('.alert').css('top', bottom_nav + height_search);
    }

    $( "#slider-km" ).slider({
        value: 30,
        orientation: "horizontal",
        max:150,
        range: "min",
        animate: true,
        change: function( event, ui ) {
            $('span#rayon-km').html(ui.value);
        }
    });

    $('.icon-tooltip').tooltip();

    initGoogleMaps();



    function initGoogleMaps(){
        var geocoder = new google.maps.Geocoder();
        var myLatlng = new google.maps.LatLng(5, 5);

        var mapOptions = {
            zoom: 12,
            center: myLatlng,
            scrollwheel: true, //we disable de scroll over the map, it is a really annoing when you scroll through page
        }

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function(responses) {
                if (responses && responses.length > 0) {
                    $('#location').val(responses[0].formatted_address);
                    var result = responses[0];

                    var city = "";
                    var country = "";
                    for (var i = 0, len = result.address_components.length; i < len; i++) {
                        var ac = result.address_components[i];
                        if (ac.types.indexOf("country") >= 0) country = ac.long_name;
                        if (ac.types.indexOf("locality") >= 0) city = ac.long_name;
                    }
                    $('#card-location').html(city+', '+country);
                } else {
                    $('#location').val('Impossible de déterminer l\'adresse.');
                    $('#card-location').val('????');
                }
            });
        }

        var map = new google.maps.Map(document.getElementById("map"), mapOptions);



        var marker = new google.maps.Marker({
            position: myLatlng,
            draggable:true
        });

        google.maps.event.addListener(marker, 'dragend', function() {
            geocodePosition(marker.getPosition());
        });

        // To add the marker to the map, call setMap();

        marker.setMap(map);

        geocoder.geocode({'address': $('#location').val()}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var result = results[0];
                var city = "";
                var country = "";
                for (var i = 0, len = result.address_components.length; i < len; i++) {
                    var ac = result.address_components[i];
                    if (ac.types.indexOf("country") >= 0) country = ac.long_name;
                    if (ac.types.indexOf("locality") >= 0) city = ac.long_name;

                }
                $('#card-location').html(city+', '+country);
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                map.setOptions({center: new google.maps.LatLng(latitude,longitude)});
                marker.setPosition(new google.maps.LatLng(latitude,longitude));
            }
        });

        $('#location').on('keyup keypress blur change', function() {
            geocoder.geocode({'address': $('#location').val()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    map.setOptions({center: new google.maps.LatLng(latitude,longitude)});
                    marker.setPosition(new google.maps.LatLng(latitude,longitude));
                    var result = results[0];

                    var city = "";
                    var country = "";
                    for (var i = 0, len = result.address_components.length; i < len; i++) {
                        var ac = result.address_components[i];
                        if (ac.types.indexOf("country") >= 0) country = ac.long_name;
                        if (ac.types.indexOf("locality") >= 0) city = ac.long_name;
                        $('span#card-location').html(city+', '+country);
                    }

                    $('#card-city').val(city+', '+country);
                }
            });
        });

        if($('#endtime').is(':checked')){
            $('#end-time').show();
            $('#card-end-time').show();
            $('#trans-time').html('de');
        }else{
            $('#end-time').hide();
            $('#card-end-time').hide();
            $('#trans-time').html('à');
        }

        if($('input#title').val() != ''){
            $('#card-title').html($('input#title').val());
        }else{
            $('#card-title').html('??????');
        }

        if($('input#start_time').val() != ''){
            $('#card-start_time').html($('input#start_time').val());
        }else{
            $('#card-start_time').html('??');
        }

        if($('input#end_time').val() != ''){
            $('#card-end_time').html($('input#end_time').val());
        }else{
            $('#card-end_time').html('??');
        }

        if($('input#activity').val() != ''){
            $('#card-activity').html($('input#activity').val());
        }else{
            $('#card-activity').html('?????');
        }

    }

    $('#endtime').on('change', function(){
        if($('#endtime').is(':checked')){
            $('#end-time').show();
            $('#card-end-time').show();
            $('#trans-time').html('de');
        }else{
            $('#end-time').hide();
            $('#card-end-time').hide();
            $('#trans-time').html('à');
        }
    });

    $('input#title').on('keyup keypress change blur', function(){
        if($('input#title').val() != ''){
            $('#card-title').html($('input#title').val());
        }else{
            $('#card-title').html('??????');
        }
    });

    $('input#start_time').on('keyup keypress change blur', function(){
        if($('input#start_time').val() != ''){
            $('#card-start_time').html($('input#start_time').val());
        }else{
            $('#card-start_time').html('??');
        }
    });

    $('input#end_time').on('keyup keypress change blur', function(){
        if($('input#end_time').val() != ''){
            $('#card-end_time').html($('input#end_time').val());
        }else{
            $('#card-end_time').html('??');
        }
    });

    $('input#activity').on('keyup keypress change blur', function(){
        if($('input#activity').val() != ''){
            $('#card-activity').html($('input#activity').val());
        }else{
            $('#card-activity').html('?????');
        }
    });

    $('input').on('keypress change input hover mouseover', function() {
        if($('#date-activity').val() != '') {
            var date = $('input#date-activity').val().split("/");
            var months = [];
            months[1] = 'Janvier';
            months[2] = 'Février';
            months[3] = 'Mars';
            months[4] = 'Avril';
            months[5] = 'Mai';
            months[6] = 'Juin';
            months[7] = 'Juillet';
            months[8] = 'Août';
            months[9] = 'Septembre';
            months[10] = 'Octobre';
            months[11] = 'Novembre';
            months[12] = 'Décembre';

            var day = date[0];
            var month = parseInt(date[1])

            $("#card-date").html(date[0]+" "+months[month]);
        }
    });

});

$.getJSON( "http://activitly.dev/json/activities", function( data ) {
    $('#activity').typeahead({
        source: data,
    });
});

$("#location").geocomplete({
});

$('.clockpicker').clockpicker({
    placement: 'bottom',
    align: 'left',
    donetext: 'Valider',
    autoclose: true,
});

$('input#date-activity').datepicker({
    format: "dd/mm/yyyy",
    weekStart: 1,
    startDate: "today",
    endDate: "+2m",
    language: "fr",
    orientation: "top auto",
    autoclose: true,
    toggleActive: true,
    todayBtn: "linked",
    todayHighlight: true
});
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    if(input.val() == '') $('.card .image').css('background-image', 'url(http://www.activitly.com/img/extra/cover.png)');
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
            input.val(log);
        }
    });
});

function readURL(input, photo) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            photo.css('background-image', 'url('+e.target.result+')');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#cover_img").change(function(){
    readURL(this, $('.card .image'));
});