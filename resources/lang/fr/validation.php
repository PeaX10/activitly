<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    "accepted"             => "Votre :attribute doit être accepté.",
    "active_url"           => "Votre :attribute n'est pas une URL valide.",
    "after"                => "Votre :attribute doit être une date postérieure au :date.",
    "alpha"                => "Votre :attribute doit seulement contenir des lettres.",
    "alpha_dash"           => "Votre :attribute doit seulement contenir des lettres, des chiffres et des tirets.",
    "alpha_num"            => "Votre :attribute doit seulement contenir des chiffres et des lettres.",
    "array"                => "Votre :attribute doit être un tableau.",
    "before"               => "Votre :attribute doit être une date antérieure au :date.",
    "between"              => [
        "numeric" => "La valeur de :attribute doit être comprise entre :min et :max.",
        "file"    => "Le fichier :attribute doit avoir une taille entre :min et :max kilo-octets.",
        "string"  => "Votre :attribute doit avoir entre :min et :max caractères.",
        "array"   => "Le tableau :attribute doit avoir entre :min et :max éléments.",
    ],
    "boolean"              => "Votre :attribute doit être vrai ou faux.",
    "confirmed"            => "Le champ de confirmation :attribute ne correspond pas.",
    "date"                 => "Votre :attribute n'est pas une date valide.",
    "date_format"          => "Votre :attribute ne correspond pas au format :format.",
    "different"            => "Les champs :attribute et :other doivent être différents.",
    "digits"               => "Votre :attribute doit avoir :digits chiffres.",
    "digits_between"       => "Votre :attribute doit avoir entre :min and :max chiffres.",
    "email"                => "Votre :attribute doit être une adresse email valide.",
    "exists"               => "Votre :attribute sélectionné est invalide.",
    "filled"               => "Votre :attribute est obligatoire.",
    "image"                => "Votre :attribute doit être une image.",
    "in"                   => "Votre :attribute est invalide.",
    "integer"              => "Votre :attribute doit être un entier.",
    "ip"                   => "Votre :attribute doit être une adresse IP valide.",
    "max"                  => [
        "numeric" => "La valeur de :attribute ne peut être supérieure à :max.",
        "file"    => "Le fichier :attribute ne peut être plus gros que :max kilo-octets.",
        "string"  => "Votre :attribute ne peut contenir plus de :max caractères.",
        "array"   => "Le tableau :attribute ne peut avoir plus de :max éléments.",
    ],
    "mimes"                => "Votre :attribute doit être un fichier de type : :values.",
    "min"                  => [
        "numeric" => "Votre :attribute doit être supérieure à :min.",
        "file"    => "Le fichier :attribute doit être plus gros que :min kilo-octets.",
        "string"  => "Votre :attribute doit contenir au moins :min caractères.",
        "array"   => "Le tableau :attribute doit avoir au moins :min éléments.",
    ],
    "not_in"               => "Votre :attribute sélectionné n'est pas valide.",
    "numeric"              => "Votre :attribute doit contenir un nombre.",
    "regex"                => "Le format du champ :attribute est invalide.",
    "required"             => "Votre :attribute est obligatoire.",
    "required_if"          => "Votre :attribute est obligatoire quand la valeur de :other est :value.",
    "required_with"        => "Votre :attribute est obligatoire quand :values est présent.",
    "required_with_all"    => "Votre :attribute est obligatoire quand :values est présent.",
    "required_without"     => "Votre :attribute est obligatoire quand :values n'est pas présent.",
    "required_without_all" => "Votre :attribute est requis quand aucun de :values n'est présent.",
    "same"                 => "Les champs :attribute et :other doivent être identiques.",
    "size"                 => [
        "numeric" => "La valeur de :attribute doit être :size.",
        "file"    => "La taille du fichier de :attribute doit être de :size kilo-octets.",
        "string"  => "Votre :attribute doit contenir :size caractères.",
        "array"   => "Le tableau :attribute doit contenir :size éléments.",
    ],
    "timezone"             => "Votre :attribute doit être un fuseau horaire valide.",
    "unique"               => "Votre :attribute est déjà utilisée.",
    "url"                  => "Le format de l'URL du champ :attribute n'est pas valide.",
    "captcha"               => "Le captcha est incorrect",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'password_infos' => [
            'passcheck' => 'Mot de passe actuel incorrect',
        ],
        'password_profile' => [
            'passcheck' => 'Mot de passe actuel incorrect',
        ],
        'username' => [
            'unique' => 'Cet identifiant n\'est pas disponible',
        ],
        'birthday' => [
            'date_format' => 'Votre date de naissance doit être au format jj/mm/aaaa',
            'before' => 'Vous devez avoir au moins 16 ans',
        ],
        'email' => [
            'unique' => 'Cette adresse mail est déjà utilisé',
        ],
        'password_confirmation' => [
            'same' => 'Les mots de passe doivent être identiques',
        ],
        'activities' => [
            'required' => 'Les activités favorites sont obligatoires'
        ],
        'facebook' => [
            'regex' => 'Le format de l\'URL vers votre page Facebook n\'est pas correct',
        ],
        'twitter' => [
            'regex' => 'Le format de l\'URL vers votre page Twitter n\'est pas correct',
        ],
        'google' => [
            'regex' => 'Le format de l\'URL vers votre page Google Plus n\'est pas correct',
        ],
        'profile_params' => [
            'required_with' => 'Erreur lors du découpage de votre avatar'
        ],
        'ban_params' => [
            'required_with' => 'Erreur lors du découpage de votre bannière'
        ],
        'new_password_confirm' => [
            'required_with' => 'Une confirmation du nouveau mot de passe est nécessaire',
            'same' => 'Les mots de passe doivent être identiques'
        ],
        'username' => [
            'exists' => 'Cet identifiant n\'est associé à aucun utilisateur',
        ],
        'email' => [
            'emailcheck' => 'Cet adresse mail est associé à un autre compte qui ne correspond pas avec le pseudo',
        ],
        'end_time' => [
            'required_with' => 'Si vous cochez la case précèdente, merci de fournir une heure de fin',
            'date_format' => 'Merci de rentrer une heure valide au format hh:mm',
        ],
        'start_time' => [
            'date_format' => 'Merci de rentrer une heure valide au format hh:mm',
        ],
        'date'  => [
            'date_format' => 'Merci de rentrer une date valide au format jj/mm/aaaa',
        ],
        'date-activity' => [
            'date_format' => 'Merci de rentrer une date valide au format jj/mm/aaaa',
            'before' => 'Votre activité ne peux pas être créer plus de 2 mois à l\'avance',
            'after' => 'Votre activité ne peux pas commencer avant aujourd\'hui'
        ],
        'activity' => [
            'exists' => 'Cette activité n\'existe pas sur Activitly.'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        "name" => "nom",
        "username" => "identifiant",
        "email" => "e-mail",
        "first_name" => "prénom",
        "last_name" => "nom",
        "password" => "mot de passe",
        "password_confirmation" => "confirmation du mot de passe",
        "city" => "ville",
        "country" => "pays",
        "address" => "adresse",
        "phone" => "téléphone",
        "mobile" => "portable",
        "age" => "âge",
        "sex" => "sexe",
        "gender" => "sexe",
        "day" => "jour",
        "month" => "mois",
        "year" => "année",
        "hour" => "heure",
        "minute" => "minute",
        "second" => "seconde",
        "title" => "titre",
        "content" => "contenu",
        "description" => "description",
        "excerpt" => "extrait",
        "date" => "date",
        "time" => "heure",
        "available" => "disponible",
        "size" => "taille",
        "firstname" => "prénom",
        "lastname" => "nom",
        "birthday" => "date de naissance",
        "password_infos" => "mot de passe",
        "password_profile" => "mot de passe",
        "new_password" => "nouveau mot de passe",
        "new_password_confirm" => "confirmation du nouveau mot de passe",
        "activities" => "activités",
        "profile_img" => "avatar",
        "ban_img" => "bannière",
        "subject" => "sujet",
        "location" => "adresse",
        "activity" => "activité",
        "start_time" => "heure de début",
        "end_time" => "heure de fin",
        "date-activity" => "date",
        "cover_img" => "image de couverture",
        "place" => "nombre de place"
    ],
];
