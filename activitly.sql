/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50542
 Source Host           : localhost
 Source Database       : activitly

 Target Server Type    : MySQL
 Target Server Version : 50542
 File Encoding         : utf-8

 Date: 08/09/2016 23:31:24 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `activities`
-- ----------------------------
DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `icon` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `activities`
-- ----------------------------
BEGIN;
INSERT INTO `activities` VALUES ('1', 'Accrobranche', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('2', 'Aerobic sportive', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('3', 'Aéromodélisme', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('4', 'Aérostation', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('5', 'Agility', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('6', 'Aikido', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('7', 'Alpinisme', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('8', 'Apnée', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('9', 'Aqua gym', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('10', 'Arts martiaux artistiques', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('11', 'Athlétisme', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('12', 'Aviation', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('13', 'Aviron', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('14', 'Baby foot', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('15', 'Badminton', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('16', 'Ball trap', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('17', 'Ballet sur glace', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('18', 'Baseball', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('19', 'Basket ball', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('20', 'Baton défense', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('21', 'Beach soccer', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('22', 'Beach volley', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('23', 'Bébé nageur', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('24', 'Biathlon', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('25', 'Billard', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('26', 'BMX', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('27', 'Bodyboard', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('28', 'Boogie Woogie', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('29', 'Boomerang', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('30', 'Boule lyonnaise', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('31', 'Bowling', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('32', 'Boxe américaine', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('33', 'Boxe anglaise', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('34', 'Boxe chinoise', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('35', 'Boxe française', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('36', 'Boxe thaïlandaise', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('37', 'Bridge', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('38', 'Canne de combat', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('39', 'Canne défense', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('40', 'Canoë kayak', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('41', 'Canyonisme', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('42', 'Capoeira', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('43', 'Carrom', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('44', 'Cerf volant', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('45', 'Chanbara', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('46', 'Char à voile', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('47', 'Cheerleading', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('48', 'Cirque', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('49', 'Claquettes', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('50', 'Combat libre', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('51', 'Combat médiéval', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('52', 'Course à pied', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('53', 'Course d\'orientation', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('54', 'Cyclisme sur piste', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('55', 'Cyclisme sur route', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('56', 'Cyclo-cross', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('57', 'Cyclotourisme', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('58', 'Danse africaine', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('59', 'Danse classique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('60', 'Danse contemporaine', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('61', 'Danse country', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('62', 'Danse espagnole', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('63', 'Danse indienne', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('64', 'Danse jazz', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('65', 'Danse modern jazz', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('66', 'Danse orientale', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('67', 'Danse sur glace', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('68', 'Danses caraïbes', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('69', 'Danses de salon', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('70', 'Danses latines', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('71', 'Danses standards', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('72', 'Danses swing', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('73', 'Deltaplane', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('74', 'Disc Golf', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('75', 'Echecs', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('76', 'Equitation', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('77', 'Escalade', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('78', 'Escrime', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('79', 'Eveil corporel', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('80', 'Fitness', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('81', 'Flag', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('82', 'Fléchettes', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('83', 'Football', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('84', 'Football US', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('85', 'Force athlétique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('86', 'Futsal', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('87', 'Giraviation', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('88', 'Golf', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('89', 'Gouren', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('90', 'Grappling', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('91', 'Gymnastique artistique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('92', 'Gymnastique douce', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('93', 'Gymnastique rythmique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('94', 'Haltérophilie', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('95', 'Handball', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('96', 'Handisport', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('97', 'Hapkido', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('98', 'Hip hop', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('99', 'Hockey subaquatique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('100', 'Hockey sur gazon', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('101', 'Hockey sur glace', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('102', 'Horse ball', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('103', 'Iaïdo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('104', 'Jeet kune do', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('105', 'Jetski', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('106', 'Jiu-Jitsu brésilien', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('107', 'Jodo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('108', 'Jorkyball', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('109', 'Joutes nautiques', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('110', 'Ju-Jitsu traditionnel', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('111', 'Judo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('112', 'Kali Escrima', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('113', 'Karaté', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('114', 'Karting', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('115', 'Kempo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('116', 'Kendo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('117', 'Kenjutsu', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('118', 'Kick boxing', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('119', 'Kin ball', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('120', 'Kite surf', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('121', 'Kobudo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('122', 'Krav maga', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('123', 'Kung fu', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('124', 'Kyudo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('125', 'Luge', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('126', 'Luta livre', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('127', 'Lutte contact', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('128', 'Lutte gréco-romaine', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('129', 'Lutte libre', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('130', 'Marche athlétique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('131', 'Modélisme', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('132', 'Moto cross', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('133', 'Moto vitesse', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('134', 'Motoneige', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('135', 'Mountainboard', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('136', 'Musculation', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('137', 'Nage avec palmes', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('138', 'Nage en eau vive', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('139', 'Naginata', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('140', 'Natation', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('141', 'Natation synchronisée', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('142', 'Ninjitsu', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('143', 'Nunchaku', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('144', 'Padel', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('145', 'Paintball', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('146', 'Pancrace', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('147', 'Parachutisme', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('148', 'Paramoteur', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('149', 'Parapente', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('150', 'Patinage artistique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('151', 'Pêche', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('152', 'Pêche sous-marine', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('153', 'Pelote basque', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('154', 'Penchak Silat', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('155', 'Pentathlon', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('156', 'Pétanque', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('157', 'Peteca', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('158', 'Planche à voile', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('159', 'Plongée', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('160', 'Plongeon', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('161', 'Qi gong', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('162', 'Quad', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('163', 'Quilles', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('164', 'Qwan ki do', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('165', 'Rafting', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('166', 'Ragga', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('167', 'Raid nature', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('168', 'Rallye', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('169', 'Randonnée équestre', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('170', 'Randonnée pédestre', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('171', 'Raquette à neige', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('172', 'Rink hockey', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('173', 'Rock', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('174', 'Rock acrobatique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('175', 'Roller', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('176', 'Roller in line hockey', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('177', 'ROS', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('178', 'Rugby à XIII', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('179', 'Rugby à XV', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('180', 'Salsa', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('181', 'Samba', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('182', 'Sambo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('183', 'Sarbacana', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('184', 'Sarbacane', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('185', 'Sauvetage', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('186', 'Self défense', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('187', 'Self Pro Krav', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('188', 'Short track', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('189', 'Skateboard', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('190', 'Ski alpin', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('191', 'Ski de fond', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('192', 'Ski de randonnée', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('193', 'Ski de vitesse', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('194', 'Ski nautique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('195', 'Ski sur herbe', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('196', 'Snowboard', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('197', 'Softball', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('198', 'Spéléologie', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('199', 'Squash', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('200', 'Sumo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('201', 'Surf', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('202', 'Taekwondo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('203', 'Taï chi chuan', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('204', 'Taï jitsu', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('205', 'Tambourin', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('206', 'Tango argentin', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('207', 'Tennis', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('208', 'Tennis de table', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('209', 'Thaing Bando', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('210', 'Tir à l\'arc', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('211', 'Tir sportif', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('212', 'Tir subaquatique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('213', 'Traîneaux', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('214', 'Trampoline', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('215', 'Triathlon', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('216', 'Tumbling', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('217', 'Twirling baton', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('218', 'ULM', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('219', 'Ultimate Frisbee', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('220', 'Viet vo dao', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('221', 'Voile', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('222', 'Vol à voile', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('223', 'Volley ball', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('224', 'VTT', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('225', 'Water polo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('226', 'Wing chun', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('227', 'Yoga', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('228', 'Yoseikan budo', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('229', 'Cinéma', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('230', 'Boire un coup', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('231', 'Laser Game', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('232', 'Soirée / Fête', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('233', 'Théâtre', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('234', 'Spectacle', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('235', 'Concert / Musique', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('236', 'Boire un coup', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('237', 'Shopping', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('238', 'Boîte de nuit', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('239', 'Parc d\'attraction', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('240', 'Musée', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('241', 'Danser', '1', '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
COMMIT;

-- ----------------------------
--  Table structure for `auth_socials`
-- ----------------------------
DROP TABLE IF EXISTS `auth_socials`;
CREATE TABLE `auth_socials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `network` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `auth_socials`
-- ----------------------------
BEGIN;
INSERT INTO `auth_socials` VALUES ('5', '13', 'twitter', '726827448', '{\"twitter\":\"https:\\/\\/twitter.com\\/Alexandre_Ptit\",\"avatar_online\":\"http:\\/\\/pbs.twimg.com\\/profile_images\\/612567139510288384\\/obho0hgO.jpg\",\"banner_online\":\"https:\\/\\/pbs.twimg.com\\/profile_banners\\/726827448\\/1401006161\"}', '2015-07-20 19:44:51', '2015-07-20 19:45:25'), ('6', '0', 'google', '113607391109928388971', '{\"google\":\"https:\\/\\/plus.google.com\\/113607391109928388971\",\"avatar_online\":\"https:\\/\\/lh3.googleusercontent.com\\/-XdUIqdMkCWA\\/AAAAAAAAAAI\\/AAAAAAAAAAA\\/4252rscbv5M\\/photo.jpg?sz=500\"}', '2015-07-20 19:46:37', '2015-07-20 19:46:37'), ('9', '18', 'facebook', '10207754971220609', '{\"facebook\":\"https:\\/\\/www.facebook.com\\/10207754971220609\\/\",\"avatar_online\":\"https:\\/\\/graph.facebook.com\\/v2.3\\/10207754971220609\\/picture?width=1920\",\"location\":\"Dijon, France\"}', '2015-08-13 09:52:53', '2015-08-13 09:56:13');
COMMIT;

-- ----------------------------
--  Table structure for `event_posts`
-- ----------------------------
DROP TABLE IF EXISTS `event_posts`;
CREATE TABLE `event_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `event_posts`
-- ----------------------------
BEGIN;
INSERT INTO `event_posts` VALUES ('1', '18', '12', '1', 'Salut ton activité est géniale !', '2016-07-11 11:20:18', '2016-07-11 11:20:18'), ('3', '18', '6', '1', 'Salut', '2016-07-11 17:14:10', '2016-07-11 17:14:10');
COMMIT;

-- ----------------------------
--  Table structure for `events`
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner` int(11) NOT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activity_id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `place` int(11) NOT NULL,
  `start_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `options` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `events_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `events`
-- ----------------------------
BEGIN;
INSERT INTO `events` VALUES ('2', 'Courir autour du Lac', '12', 'peax_EiGnaUhBkMo99qREkWmoCPQ45WjXaC9F', '48.8566921', '2.407165', 'Paris, France', '89 Rue des Orteaux, 75020 Paris, France', '52', 'Toute une petite journée de run rien que pour vous les enfants.\r\nAlors profiter en', '2015-08-13', '6789098', '12:05', '12:00', '', '2015-08-11 01:12:03', '2015-08-11 01:12:03'), ('3', 'Courir autour de l\'étang de merde', '16', 'peax10_JBFVEI1YJrCnvezYC96dA56ZtzUNyOtF', '47.328918', '5.0020467', 'Dijon, France', '7 Avenue du 1er Consul, 21000 Dijon, France', '52', 'Et ouai on va courir comme des petits fous autour d\'un petit lac que l\'on kiff tous, alors gros bisous à tous et à bientôt les Loulous.', '2015-08-13', '5', '12:00', '01:05', '', '2015-08-11 17:36:19', '2015-08-11 17:36:19'), ('4', 'Marathon de Paris', '16', '', '48.8656728', '2.3409346', 'Paris, France', '3 Rue Catinat, 75001 Paris, France', '52', 'Un petit marathon pour tous venez nombreux surtout que l\'on puisse tous s\'amuser ensemble.\r\nGros bisous à tous les amis.\r\n\r\nCordialement,\r\nVotre petit nounours', '2015-08-12', '4', '12:00', '', '', '2015-08-12 13:38:54', '2015-08-12 13:38:54'), ('5', 'foot à 5', '17', 'adrien_kpZewol3GGmVXS1rJnclKTYt7xTjRzFq', '47.322047', '5.04148', 'Dijon, France', 'Dijon, France', '83', 'Un petit football chez Adrien histoire de bien s\'amuser.', '2015-08-15', '4', '21:00', '', '', '2015-08-12 18:51:15', '2015-08-12 18:51:15'), ('7', 'Courir durant l\'été', '19', '', '48.258027', '3.2643524', 'Cuy, France', '27 Rue Georges Cerneau, 89140 Cuy, France', '52', 'itsyuioyrfgmlgfsdfgohgfdsuilrertyuiortesfryk', '2015-08-14', '0', '17:00', '', '', '2015-08-13 17:41:54', '2015-08-13 17:41:54'), ('8', 'Football', '20', 'amine89_naDSrkcN1fsKvxKvRJIsXIxpDgy9fGjc', '48.8840421', '2.5257443', 'Gagny, France', 'Rue Clemenceau, 93220 Gagny, France', '83', 'dlijjokjghhsdhfj  hsqdihjqsd qs fkjqhsd fkjhqsdfk j \r\n$\r\n sqfsqdf qsd f', '2015-09-03', '4', '12:05', '14:00', '', '2015-09-02 21:41:02', '2015-09-02 21:41:02'), ('9', 'cyclisme', '21', 'guigui_AY8pBmkGtbywCM1MIAFWS6MOYEsSuObu', '47.3406352', '5.0644086', 'Dijon, France', '1 Rue de Cracovie, 21000 Dijon, France', '55', 'edfyguhijohgfddrtyuikjuhygtfredyukoijhgtfrdesdrtyguhijok,p;lkgyf\r\ntcfyvgubhinjo,kpluytre', '2015-09-14', '3', '00:00', '', '', '2015-09-14 19:34:11', '2015-09-14 19:34:11'), ('10', 'Athlétisme sur Dijon', '16', '', '47.3263515', '5.0447809', 'Dijon, France', 'Place de la République, 21000 Dijon, France', '11', 'Ce serait super cool que des gens comme moi et Elise participent à des activités sur Dijon !', '2016-07-18', '3', '13:00', '15:00', '', '2016-03-13 18:44:59', '2016-03-13 18:44:59');
COMMIT;

-- ----------------------------
--  Table structure for `friendships`
-- ----------------------------
DROP TABLE IF EXISTS `friendships`;
CREATE TABLE `friendships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `friendships`
-- ----------------------------
BEGIN;
INSERT INTO `friendships` VALUES ('2', '18', '20', '2016-07-10 18:29:39', '0000-00-00 00:00:00'), ('3', '18', '21', '2016-07-10 18:29:32', '0000-00-00 00:00:00'), ('4', '19', '18', '2016-07-09 19:05:41', '0000-00-00 00:00:00'), ('6', '18', '19', '2016-07-10 17:25:35', '2016-07-10 17:25:35');
COMMIT;

-- ----------------------------
--  Table structure for `invites`
-- ----------------------------
DROP TABLE IF EXISTS `invites`;
CREATE TABLE `invites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL,
  `recipient` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `visible` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `invites`
-- ----------------------------
BEGIN;
INSERT INTO `invites` VALUES ('1', '12', '16', '2', '1', 'Un petit marathon pour tous venez nombreux surtout que l\'on puisse tous s\'amuser ensemble.\r\nGros bisous à tous les amis.\r\n\r\nCordialement,\r\nVotre petit nounours', '2015-08-12 13:38:54', '0000-00-00 00:00:00'), ('2', '12', '21', '9', '0', 'Salut Adrien ça te dis de faire un foot ?', '2015-08-12 20:53:38', '2015-09-14 19:36:26');
COMMIT;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table', '1'), ('2015_07_13_184531_create_activities_table', '1'), ('2015_07_18_110114_create_auth_socials_table', '1'), ('2015_07_20_115351_create_newsletter_users_table', '2'), ('2014_10_12_100000_create_password_resets_table', '3'), ('2015_07_26_050654_create_events_table', '3'), ('2015_08_11_130317_create_participations_table', '4'), ('2015_08_12_124356_create_invites_table', '5'), ('2016_07_08_162329_create_friend_ships_table', '6'), ('2016_07_09_125616_User_last_activities', '7'), ('2016_07_10_125445_create_profile_posts_table', '8'), ('2016_07_11_091547_create_event_posts_table', '9');
COMMIT;

-- ----------------------------
--  Table structure for `newsletter_users`
-- ----------------------------
DROP TABLE IF EXISTS `newsletter_users`;
CREATE TABLE `newsletter_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `newsletter_users`
-- ----------------------------
BEGIN;
INSERT INTO `newsletter_users` VALUES ('1', 'sdqfsdqfsdf@sqfsqd.fr', '2015-07-21 09:42:02', '2015-07-21 09:42:02'), ('2', 'official-alex@live.fr', '2015-07-21 09:44:02', '2015-07-21 09:44:02');
COMMIT;

-- ----------------------------
--  Table structure for `participations`
-- ----------------------------
DROP TABLE IF EXISTS `participations`;
CREATE TABLE `participations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `participations`
-- ----------------------------
BEGIN;
INSERT INTO `participations` VALUES ('1', '12', '2', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'), ('2', '16', '2', '1', '2015-08-11 21:26:57', '2015-08-11 21:26:59'), ('3', '17', '5', '1', '2015-08-12 20:56:52', '2015-08-12 20:56:53'), ('4', '18', '5', '1', '2015-08-13 12:06:30', '2015-08-13 12:06:32'), ('5', '21', '9', '1', '2015-09-14 21:35:11', '2015-09-14 21:35:13');
COMMIT;

-- ----------------------------
--  Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `password_resets_id_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `profile_posts`
-- ----------------------------
DROP TABLE IF EXISTS `profile_posts`;
CREATE TABLE `profile_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `profile_user` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `profile_posts`
-- ----------------------------
BEGIN;
INSERT INTO `profile_posts` VALUES ('4', '18', '12', '1', 'Pleins de petit messages un peu cochon pour toi :)', '2016-07-10 14:11:13', '2016-07-10 14:11:13'), ('5', '18', '12', '1', 'sqdfsqdf', '2016-07-10 14:12:05', '2016-07-10 14:12:05'), ('6', '18', '12', '1', 'sdf', '2016-07-10 14:12:08', '2016-07-10 14:12:08'), ('7', '18', '12', '1', 'qsdfsdqfsf', '2016-07-10 14:12:11', '2016-07-10 14:12:11'), ('8', '18', '12', '1', 'sqdfsdqfqsdf', '2016-07-10 14:12:14', '2016-07-10 14:12:14'), ('9', '18', '12', '1', 'sqdfsqdfqsdf', '2016-07-10 14:12:20', '2016-07-10 14:12:20'), ('10', '18', '12', '1', 'sdqfsqdf', '2016-07-10 14:12:23', '2016-07-10 14:12:23'), ('11', '18', '12', '1', 'qdsfqsfqsdfff', '2016-07-10 14:12:30', '2016-07-10 14:12:30'), ('12', '18', '12', '1', 'sqdfsqdfqsdf', '2016-07-10 14:12:36', '2016-07-10 14:12:36'), ('14', '18', '12', '1', 'dsgf', '2016-07-10 16:05:30', '2016-07-10 16:05:30');
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activities` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `validate` int(11) NOT NULL,
  `step` int(11) NOT NULL,
  `ban` int(11) NOT NULL,
  `token` text COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_activity` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('12', 'PeaX', 'Alexandre', 'Petit', '1', 'peax10@me.com', '$2y$10$pQT3dxxNzmi9cNoZTz4tE.ad5m4OkisOJ8wI0AUMLmQ6biVBwSP7O', 'Paris, France', 'Course à pied', 'peax_TPSjdJmNPc8SciH8pLrRaSP0pRsZ6hYt', 'peax_OgVtDcthT0ir1ILLMwTDzJNpFVO717Fq', 'https://www.facebook.com/PetitAlexandre', 'https://twitter.com/Alexandre_Ptit', '', '1995-07-05', '1', '1', '0', '', 'N3eZl7sFWb8S5vkROnEzIaQp2UvTTilkN6ypBRpSYJLeJQRfovDuX6VPUjVo', '2015-07-20 19:45:25', '2015-08-12 15:06:57', '0000-00-00 00:00:00'), ('14', 'PeaX10400', 'Alexandre', 'Petit', '1', 'contact@activitly.com', '$2y$10$fg5sT.q6/LbV6Y4F4LLHyOZbZb6hL3zlfi5N8xffdmS/4F441PF2u', '', '', '', '', '', '', '', '1995-07-05', '1', '0', '0', '1oHiVGwDjYbVIbbCRhrMaKkMVDmgW1IAajaGhw2cKcmR5ZOr', 'x1943oC1NXScrhnFfQELICrwLUWo2sx3ya0RtMIlibrRUYbnTGgSfwabtzpU', '2015-07-23 05:50:10', '2015-07-23 05:52:02', '0000-00-00 00:00:00'), ('15', 'Motsor', 'Rostom', 'Petit', '1', 'rostom.ptt@gmail.com', '$2y$10$4MhlzDTUlF.bcl9h/J/UDOszQK1nkLghH.K/SMDk49Iow86jtPyqq', 'La Louptière-Thénard, France', 'Course à pied,Natation', 'motsor_s9fewTSHSlLe3156DPNHeaJuFEHFc24y', '', 'https://www.facebook.com/rostom.petit', '', '', '1988-02-26', '1', '1', '0', '', null, '2015-08-04 18:19:01', '2015-08-04 16:24:13', '0000-00-00 00:00:00'), ('16', 'PeaX10', 'Alexandre', 'Petit', '1', 'peax10@icloud.com', '$2y$10$ERQi4Dhx6/7f6DedB13rUuJckQfQhnri5d6r5soaHZoKn5RCnm776', 'Dijon, France', 'Course à pied', 'peax10_SAy9TUgtdOypPcgjTHz7p9AJA8tWshoX', '', '', '', '', '1995-07-05', '1', '1', '0', 'Q5pmOHRMlRq0AcFspHooy18kWNc9pnm5s0EyFo2UeokUc9nQ', 'sTxmALdSuwn9yqCUYlKI70TZXevEsq0ZB3KTaY5LUlhzsyXYpJQI3UgnpItv', '2015-08-11 17:27:45', '2016-03-13 18:47:23', '0000-00-00 00:00:00'), ('17', 'adrien', 'Adrien', 'Chereau', '1', 'chereau.adrien89140@live.fr', '$2y$10$.jwU8VF9OJW3YzKPK/WGf.MO5JUQep0CgTZSSQZEXl2rlBQGkb4aq', 'Dijon, France', 'Course à pied,football', 'adrien_BqzZV4IkT0OkWSta7zdpJSgxXYddtQKL', 'adrien_WVOcjS2iI4qdbrny5ZvpeuxzScdXf4zq', '', '', '', '1996-11-18', '1', '1', '0', 'xyRTWi89D8wVP3bp4FAgXHLrSPDaa7EGPw3wOd7WtXM7FyVI', null, '2015-08-12 18:40:52', '2015-08-12 18:44:21', '0000-00-00 00:00:00'), ('18', 'Alex', 'Alexandre', 'Petit', '1', 'dev42web@gmail.com', '$2y$10$fVQWgaseVHpegbguBwPFKOr0hahpl4DuCcRzsAj.7YIGXGPcxIAMy', 'Dijon, France', 'Course à pied,Natation,Boire un coup', 'alex_lrCyEBu8XGXdry3fdRt7UV3pr8hJnz8T', 'alex_WSOxv3Dr7bcm4DJfBXt6kOEeYaLoKkqz', 'https://www.facebook.com/PetitAlexandre', '', '', '1995-07-05', '1', '1', '0', '', 'Ro4SAbwbTwb7nINHxbyJlYidkbeUQ3kg3CvH3sH1Cswj7VDrpEVGZQzVBZzV', '2015-08-13 09:56:13', '2016-08-09 21:30:18', '2016-08-09 21:30:14'), ('19', 'theo23', 'Theo', 'Metro', '1', 'theo.metro45@gmail.com', '$2y$10$uESi2o8PtAtPs2oeAwhZCOEsJJxUuXtnQDWMnt2MWvSibiigHL60S', '89100 Sens, France', 'Course à pied,football', 'theo23_g0xwQj8jv8lz4hWX01K8JQg0CaLczqbg', '', '', '', '', '1996-01-10', '1', '1', '0', 'Vd2sB5IaFrqkPR80yvkigKmxUHeHCnsX5hVmZRzPxWiCfxtD', null, '2015-08-13 17:37:15', '2015-08-13 17:39:50', '0000-00-00 00:00:00'), ('20', 'Amine89', 'Amine', 'Belkacemi', '1', 'amine.93600@hotmail.com', '$2y$10$ie9qV7.HC5kQxPRpkSQi7eaNWfikeX6ztOuYJzBSWrtd1j./AkE8.', '89100 Sens, France', 'Course à pied,football', 'amine89_IHmAN9mfEAaAWXtZ6PtDQoZm7yLlRoGK', 'amine89_eWj1VrHRMJzihfHilLhupHVDgXaGtDxF', '', '', '', '1997-06-14', '1', '1', '0', 'YkWc6hLUV8zOPPyCGtd69Q80kdrrD7EcV5eKiRv04bo8WaRg', null, '2015-09-02 21:37:25', '2015-09-02 21:39:15', '0000-00-00 00:00:00'), ('21', 'guigui', 'Guillaume', 'Pfundstein', '1', 'guillaume.pfundstein@laposte.net', '$2y$10$zXmMBB2GI5QMowz5ytiZ4.UXCPDFh8sQa3cEgVT6X1iTm/HhA8zRe', 'Dijon, France', 'Course à pied', 'guigui_P7GddQCZqbeKtkfSBftPKO7XIhq7CDGq', 'guigui_6x98pYTu8AmFzrfrJ5Ov0skox8mdOTIu', '', '', '', '1996-11-13', '1', '1', '0', 'F5Omr9JgwriOeeeFfrxWIoUmVoq02y2WEvpOg0nEuhzu8MKX', 'XFZcDAHYNFTG13OG8pRSmGS7GSOipL36CuMFb3AHzeukHxyWJrlCD4yFxqKL', '2015-09-14 19:29:12', '2015-09-14 19:37:13', '0000-00-00 00:00:00');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
